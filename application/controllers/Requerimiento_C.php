<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Requerimiento_C extends CI_Controller {

        private $estado;
        private $calificacion;
        private $sla_ta_;
        private $sla_TS_;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Sis_Requerimiento_M','requerimiento');
	}

	public function panel_view()
	{
        if( !empty($this->session->userdata('des_usuario')))
            $this->load->view('panel');
        else
            redirect('/');
	}

	public function login_view()
	{   
		if( !empty($this->session->userdata('des_usuario')))
			redirect('/panel');
		else
			$this->load->view('login');
	    
	}

	public function login_json()
	{
		$this->requerimiento->usuario = $this->input->post('usuario');
		$this->requerimiento->password = $this->input->post('password');
        if ( $this->requerimiento->info_usuario() ) { 
             $est_pass = $this->requerimiento->info_usuario();
             if ( $est_pass['est_pass'] == '1' ){
                $this->session->set_userdata($this->requerimiento->info_usuario());$res = 2;
             } else {
                $this->session->set_userdata($this->requerimiento->info_usuario());$res = 1;
             }
        } else { 
            $res = 0;
        }
        echo $res;
	}

	public function login_cerrar()
	{
		$this->session->sess_destroy();
	}
        
        public function devolucion_equipo()
    { 
        $arr_where = array( 'id_reser_equipo' => $this->input->post('id_reser_equipo'));
        $arr_update = array( 'est_devolucion' => '0');
        $this->requerimiento->name_table = "reserva_equipo";
        $this->requerimiento->update($arr_update,$arr_where);         
    }

        public function cerrar_session_pausar()
    {   
        
        $estados_tickets = array('0' => 'Terminado', '1' => 'En proceso', '2' => 'Pendiente', '3' => 'Pausa' );
        $sql = "SELECT * FROM detalle_requerimiento WHERE id_usuario_asig = '".$this->session->userdata('id_usuario')."' AND est = '1'";

        $detalle_requerimiento = $this->db->query($sql)->result();
        foreach ( $detalle_requerimiento as $_detalle_requerimiento ) {

            $this->requerimiento->name_table = "detalle_requerimiento";
            $where = array( 'id_detalle_requerimiento' => $_detalle_requerimiento->id_detalle_requerimiento);
            $this->requerimiento->update(array('est' => '3'),$where);

            $this->requerimiento->name_table = "hist_req_ticket";
            $arr = array(
                    'id_usuario' => $this->session->userdata('id_usuario'),
                    'est_requerimiento' => '3',
                    'id_detalle_requerimiento' => $_detalle_requerimiento->id_detalle_requerimiento,
                    'id_detalle_requerimiento_remp' => 0
            );

            $this->requerimiento->insert_requerimiento($arr);

            $this->requerimiento->name_table = "hist_requerimiento";
            $arr_insert = array(
                               'id_detalle_requerimiento' => $_detalle_requerimiento->id_detalle_requerimiento,
                               'comet_hist_req' => 'Cambio de estado a ' . $estados_tickets['3'],
                               'id_usuario' => $this->session->userdata('id_usuario')  
                          );

            $this->requerimiento->insert_requerimiento($arr_insert);
            
        }
        
        $this->session->sess_destroy();

    }

	public function modules()
	{
        $this->load->view('modulos/'.$this->input->get('op'));
	}

	public function datalle_requerimiento()
	{
		$this->requerimiento->id_detalle_requerimiento = $this->input->post('id_detalle_requerimiento');
		$this->json_format($this->requerimiento->detalle_requerimiento());
	}

	public function tip_requerimiento()
	{
		$this->json_format( $this->requerimiento->tip_requerimiento() );
	}

	public function insert_requerimiento()
	{
        $this->requerimiento->name_table = "detalle_requerimiento";
        $this->id_detalle_requerimiento =  $this->requerimiento->insert_requerimiento((array) $this->input->post());
        $this->requerimiento->name_table = "hist_req_ticket";
        $arr_isert = array( 
                            'id_usuario' => $this->input->post('id_usuario'),
        	                'id_detalle_requerimiento' => $this->id_detalle_requerimiento
        	              );

                 $this->requerimiento->insert_requerimiento($arr_isert);

                 require FCPATH . 'vendor/autoload.php';
		$options = array(
                    'cluster' => 'mt1',
                    'useTLS' => true
                  );
        $pusher = new Pusher\Pusher(
            'c61cd220d9d9bf274c8b',
            '88c2cfb5fe85e755af7e',
            '817308',
            $options
        );
          
        $data['tip_req'] = 'nuevo';
        $data['usuarios'] = 'todo';
        $data['tipo_usuario'] = 'sistemas';
        $data['titulo'] = $this->input->post('titulo_requerimiento');
		$data['descripcion'] =  strip_tags($this->input->post('desc_requerimiento'));
		$pusher->trigger('my-channel', 'my-event', $data);        
	}

	public function insert_usuario()
	{
		$this->requerimiento->name_table = "usuario";
        $this->requerimiento->insert_requerimiento((array) $this->input->post());
	}

	public function sub_tip_requerimiento()
	{
		$this->requerimiento->id_tip_requerimiento = $this->input->post('id');
		$this->json_format($this->requerimiento->sub_tip_requerimiento());
	}

	public function load_requerimientos()
	{   
		$this->requerimiento->op = $this->input->get('opt');
		$this->requerimiento->est = $this->input->get('est');
		$this->requerimiento->req_prioridad = $this->input->get('req_prioridad');
		$this->requerimiento->id_usuario = $this->input->get('id_usuario');
		$this->requerimiento->id_tip_requerimiento = $this->input->get('id_tip_requerimiento');
		$this->requerimiento->muestra_tikets = $this->input->get('muestra_tikets');
        $this->requerimiento->fec_ini_cierre = $this->input->get('start');
        $this->requerimiento->fec_hasta_cierre = $this->input->get('end');
        $this->requerimiento->fil_fec = $this->input->get('fec');
		$this->json_format($this->requerimiento->load_requerimientos_m());
	}

	public function load_historial_requerimiento()
	{
        $this->requerimiento->id_detalle_requerimiento = $this->input->post('id_detalle_requerimiento');
        $this->json_format($this->requerimiento->load_historial_requerimiento());		
	}

	public function insert_historial_requerimiento()
	{
	    $this->requerimiento->name_table = "hist_requerimiento";
        $this->requerimiento->insert_requerimiento((array) $this->input->post());
	}

	public function load_area()
	{
		$this->json_format($this->requerimiento->load_area());
	}

	public function load_tipo_usuario()
	{
		$this->json_format($this->requerimiento->load_tipo_usuario());
	}

	public function load_user()
	{
		$this->json_format($this->requerimiento->load_user());
	}

    public function delete_usuario()
    {
    	$this->requerimiento->id_usuario  = $this->input->post('id_usuario');
    	$this->requerimiento->delete_usuario();
    }

    public function detalle_usuario()
    {
    	$this->requerimiento->id_usuario  = $this->input->post('id_usuario');
    	$this->json_format($this->requerimiento->detalle_usuario());
    }

    public function update_usuario()
    {
       $this->requerimiento->name_table = "usuario";
       $where = array( 'id_usuario' => $this->input->get('id_usuario'));
       
       if( $this->input->post('est_pass') == '0' )  {       
           $this->requerimiento->update((array) $this->input->post(),$where);        
       } else if ( $this->input->post('est_pass') == '1' ){
           $this->requerimiento->update((array) $this->input->post(),$where);
           $this->requerimiento->update( array('pass_inc'=>'e10adc3949ba59abbe56e057f20f883e','pass_vis'=>'123456') ,$where);  
       }
  
       
    }

    public function detalle_area()
    {
       $this->requerimiento->id_area = $this->input->post('id_area');
       $this->json_format($this->requerimiento->detalle_area());
    }

    public function list_reservas()
    {
    	$this->json_format($this->requerimiento->list_reservas());
    }

    public function load_tikets_com()
    {
        $this->requerimiento->id_usuario = $this->input->post('id_usuario');
        $this->json_format($this->requerimiento->load_tikets_com());
    }

    public function delete_area()
    {   
    	$this->requerimiento->id_area = $this->input->post('id_area');
    	$this->requerimiento->delete_area();
    }

    public function delete_sub_tipo()
    {
        $arr_where = array( 'id_sub_tip_requerimiento' => $this->input->post('id_sub_tip_requerimiento'));
        $arr_update = array( 'est' => '0');
        $this->requerimiento->name_table = "sub_tipo_requerimiento";
        $this->requerimiento->update($arr_update,$arr_where);
    }
    
    public function insert_area()
	{
	    $this->requerimiento->name_table = "area_of";
        $this->requerimiento->insert_requerimiento((array) $this->input->post());
	}

	public function update_area()
	{
		$this->requerimiento->name_table = "area_of";
        $where = array( 'id_area' => $this->input->get('id_area'));
        $this->requerimiento->update((array) $this->input->post(),$where);
	}

	public function abrir_requerimiento()
	{
		$this->requerimiento->name_table = "detalle_requerimiento";
		$where = array( 'id_detalle_requerimiento' => $this->input->post('id_detalle_requerimiento'));
        $this->requerimiento->update(array('est' => '2','date_requerimiento_cierre'=>'0000-00-00 00:00:00'),$where);
        $this->requerimiento->name_table = "hist_requerimiento";
        $arr_insert = array(
                           'id_detalle_requerimiento' => $this->input->post('id_detalle_requerimiento'),
                           'comet_hist_req' => 'Cambio de estado a Pendiente',
                           'id_usuario' => $this->input->post('id_usuario')  
                      );
        $this->requerimiento->insert_requerimiento($arr_insert);
	}

	public function insert_historia()
	{  

		$estados_tickets = array('0' => 'Terminado', '1' => 'En proceso', '2' => 'Pendiente', '3' => 'Pausa' );

        $sql = "SELECT est FROM detalle_requerimiento WHERE id_detalle_requerimiento = '".$this->input->post('input_id_detalle_requerimiento')."' AND est = '".$this->input->post('__estado_req')."'";

        $resp = $this->db->query($sql)->row();

        if( empty($resp) )  {

        	$this->requerimiento->name_table = "hist_req_ticket";
        	$arr = array(
	        	    'id_usuario' => $this->input->post('id_usuario'),
	                'est_requerimiento' => $this->input->post('__estado_req'),
	                'id_detalle_requerimiento' => $this->input->post('input_id_detalle_requerimiento'),
	                'id_detalle_requerimiento_remp' => $this->input->post('id_detalle_requerimiento_remp')
            );
            $this->requerimiento->insert_requerimiento($arr);

            $this->requerimiento->name_table = "hist_requerimiento";
	        $arr_insert = array(
	                           'id_detalle_requerimiento' => $this->input->post('input_id_detalle_requerimiento'),
	                           'comet_hist_req' => 'Cambio de estado a ' . $estados_tickets[$this->input->post('__estado_req')],
	                           'id_usuario' => $this->input->post('id_usuario')  
                          );

	        $this->requerimiento->insert_requerimiento($arr_insert);

        }


		$this->requerimiento->name_table = "hist_requerimiento";
        $arr_insert = array(
                           'id_detalle_requerimiento' => $this->input->post('input_id_detalle_requerimiento'),
                           'comet_hist_req' => $this->input->post('req_hist_comentario'),
                           'id_usuario' => $this->input->post('id_usuario')  
                      );

        $this->requerimiento->insert_requerimiento($arr_insert);
        $arr_where = array( 'id_detalle_requerimiento' => $this->input->post('input_id_detalle_requerimiento'));
             $arr_update = array( 
                               'req_prioridad' => $this->input->post('__prioridad_req'),
                               'est' => $this->input->post('__estado_req'),
                               'fec_est_sistmas' => $this->input->post('fec_est_sistmas')
                           );
       
        $this->requerimiento->name_table = "detalle_requerimiento";
        $this->requerimiento->update($arr_update,$arr_where);

        $sql = "SELECT LPAD(id_detalle_requerimiento,6,'0') AS 'n_ticket',id_usuario,titulo_requerimiento FROM detalle_requerimiento WHERE id_detalle_requerimiento = '".$this->input->post('input_id_detalle_requerimiento')."'";

        $resp = $this->db->query($sql)->row();


        require FCPATH . 'vendor/autoload.php';
        $options = array(
                    'cluster' => 'mt1',
                    'useTLS' => true
                  );
        $pusher = new Pusher\Pusher(
            'c61cd220d9d9bf274c8b',
            '88c2cfb5fe85e755af7e',
            '817308',
            $options
        );
          
        $data['tip_req'] = 'respuesta';
        $data['usuarios'] = $resp->id_usuario;
        $data['tipo_usuario'] = 'usuario';
        $data['titulo'] = $resp->n_ticket . ' - ' . $resp->titulo_requerimiento;
        $data['descripcion'] =  'Nueva respuesta del requerimiento - ' . strip_tags($this->input->post('req_hist_comentario'));
        $pusher->trigger('my-channel', 'my-event', $data);

	}

	public function read_mensaje()
	{ 
		$this->json_format($this->requerimiento->read_mensaje());
	}
    
    public function mensaje()
    {
       $this->requerimiento->name_table = "mensaje";
		$where = array( 'id_mensaje' => '1');
       $this->requerimiento->update(array('des_mensaje' => $this->input->post('des_mensaje')),$where);
    }

       public function insert_reserva()
	{
          
		    $this->requerimiento->name_table = "reserva_equipo";
        $fecha = $this->input->post('fecha');
		    $fec_desde = strtotime($this->input->post('fec_desde'));
        $fec_desde_valida = date("Y-m-d H:i:s", $fec_desde);
        $fec_desde = date("H:i:s", $fec_desde);
        $fec_hasta = strtotime($this->input->post('fec_hasta'));
        $fec_hasta_valida = date("Y-m-d H:i:s", $fec_hasta);
        $fec_hasta = date("H:i:s", $fec_hasta);
        $validar_tiempo = $this->cal_time($fec_desde_valida, $fec_hasta_valida) / 60;
        $__conf = $this->requerimiento->list_conf_equipo();
        $est__ = ( $this->input->post('id_equipo') == 1 || $this->input->post('id_equipo') == 3  ) ? 1 : 2;
        $this->requerimiento->id_usuario = $this->input->post('id_usuario');
        $valida_reserva = $this->requerimiento->Count_x_bloqueo_ususario(1);

        $num = !empty( $valida_reserva->total ) ?  $valida_reserva->total : 0;

            if ( $est__ ==  1 ){                                                                                                           
                if ( $num <= $__conf->conf_nun_bloqueo ){
                   if ( floatval( $validar_tiempo ) <= $__conf->conf_tiempo_hora_max  ) {

                        if ( $this->input->post('id_equipo') == 3 ){
                              if ( empty($this->requerimiento->validar_reserva(3,$fec_desde,$fecha,'inicio')) AND empty($this->requerimiento->validar_reserva(3,$fec_hasta,$fecha,'fin')) ) {
                                  if ( empty($this->requerimiento->validar_reserva(1,$fec_desde,$fecha,'inicio')) AND empty($this->requerimiento->validar_reserva(1,$fec_hasta,$fecha,'fin')) ) {
                                      if ( empty($this->requerimiento->validar_reserva(2,$fec_desde,$fecha,'inicio')) AND empty($this->requerimiento->validar_reserva(2,$fec_hasta,$fecha,'fin')) ) {
                                          $arr_insert = array(
                                                           'id_equipo' => 3,
                                                           'id_usuario' => $this->input->post('id_usuario'),
                                                           'fecha' => $this->input->post('fecha'),
                                                           'fec_desde' => $fec_desde,
                                                           'fec_hasta' => $fec_hasta,
                                                           'res_motivo' => $this->input->post('res_motivo')
                                                      );

                                          $this->requerimiento->insert_requerimiento($arr_insert);

                                          if ( $num  > 0 )
                                               $resp = 'Usted tiene '. ( $valida_reserva->total  - $__conf->conf_tiempo_hora_max ) . ' oportunidades mas para devolver el proyector a sistemas ( 5 minutos de tolerancia ) & 1' ;
                                          else
                                             $resp = '1 & 1';
                                          
                                      } else {
                                         $resp = 'La Sala se encuentra separada & 0';
                                      }
                                  } else {
                                      $resp = 'El Proyector se encuentra reservado & 0';
                                  }
                              } else {
                                  $resp = 'El horario ya esta separado & 0';
                              }  
                          } else {
                              if ( empty($this->requerimiento->validar_reserva(3,$fec_desde,$fecha,'inicio')) AND empty($this->requerimiento->validar_reserva(3,$fec_hasta,$fecha,'fin')) ) {
                                  if (empty($this->requerimiento->validar_reserva($this->input->post('id_equipo'),$fec_desde,$fecha,'inicio')) AND empty($this->requerimiento->validar_reserva($this->input->post('id_equipo'),$fec_hasta,$fecha,'fin'))){
                                      $arr_insert = array(
                                                       'id_equipo' => $this->input->post('id_equipo'),
                                                       'id_usuario' => $this->input->post('id_usuario'),
                                                       'fecha' => $this->input->post('fecha'),
                                                       'fec_desde' => $fec_desde,
                                                       'fec_hasta' => $fec_hasta,
                                                       'res_motivo' => $this->input->post('res_motivo')
                                                      );

                                      $this->requerimiento->insert_requerimiento($arr_insert);

                                      if ( $num  > 0 )
                                           $resp = 'Usted tiene '. ( $__conf->conf_tiempo_hora_max - $valida_reserva->total  ) . ' oportunidades mas para devolver el proyector a sistemas ( 5 minutos de tolerancia ) & 1' ;
                                      else
                                           $resp = '1 & 1';
                                  } else {
                                      $resp = 'El horario ya esta separado & 0';
                                  }
                              } else {
                                  $resp = 'El horario ya esta separado & 0';
                              }
                          }
                              

                   } else {
                        $resp = 'son ' . $__conf->conf_tiempo_hora_max . ' horas como m�ximo para separar el equipo & 0';
                   }
                } else {
                     $resp = "Usted ya no tiene oportunidad de separar una nueva reserva, por favor comunicarse con sistemas. & 0" ;
                }   

            } else {
                if ( empty($this->requerimiento->validar_reserva(3,$fec_desde,$fecha,'inicio')) AND empty($this->requerimiento->validar_reserva(3,$fec_hasta,$fecha,'fin')) ) {
                    if (empty($this->requerimiento->validar_reserva($this->input->post('id_equipo'),$fec_desde,$fecha,'inicio')) AND empty($this->requerimiento->validar_reserva($this->input->post('id_equipo'),$fec_hasta,$fecha,'fin'))){
                        $arr_insert = array(
                                         'id_equipo' => $this->input->post('id_equipo'),
                                         'id_usuario' => $this->input->post('id_usuario'),
                                         'fecha' => $this->input->post('fecha'),
                                         'fec_desde' => $fec_desde,
                                         'fec_hasta' => $fec_hasta,
                                         'res_motivo' => $this->input->post('res_motivo')
                                        );

                        $this->requerimiento->insert_requerimiento($arr_insert);
                           $resp = '1 & 1';
                    } else {
                        $resp = 'El horario ya esta separado & 0';
                    }
                } else {
                    $resp = 'El horario ya esta separado & 0';
                }
            }
        

        echo $resp;
	}

	public function subir_adjunto()
	{ 
		move_uploaded_file($_FILES["file"]["tmp_name"], FCPATH. "assets/evidencia/".$_FILES['file']['name']);
	}

	public function insert_usuario_masivo()
    {
         
           include_once 'SimpleXLSX.php';
           $xlsx = new SimpleXLSX( '../../assets/import.xlsx');

           echo APPPATH.'/assets/import.xlsx';

           print_r( $xlsx->rows() );
	}
    
    public function notificaciones()
    {
    	require __DIR__ . '/vendor/autoload.php';

		  $options = array(
		    'cluster' => 'us3',
		    'useTLS' => true
		  );
		  
		  $pusher = new Pusher\Pusher(
		    'fe7f4c70a2b421c74676',
		    '9de44fc74e81694d6f80',
		    '803552',
		    $options
		  );

		  $data['message'] = 'hello world';
		  $pusher->trigger('my-channel', 'my-event', $data);
    }

    public function list_sist()
    {
    	$this->json_format($this->requerimiento->list_sist());
    }

    public function panel_vidoteca()
    {
        $directorio = opendir(FCPATH. 'videoteca'); //ruta actual
	    $arr = []; 
	    while ($archivo = readdir($directorio))
	    {
	    	$archivo__ = explode('.', $archivo);
	    	if ($archivo__[1] == 'mp4' || $archivo__[1] == 'wmv' ) 
	           $arr[] = array('ruta_video' => 'videoteca/'.$archivo,'nombre' => $archivo__[0]);
	    }

	    $this->json_format($arr);
    }
    
    public function update_responsable()
    {
        $arr_where = array( 'id_detalle_requerimiento' => $this->input->post('id_detalle_requerimiento'));
        $arr_update = array( 
                               'id_usuario_asig' => $this->input->post('id_usuario'),
                               'est' => '1'
                            );
        $this->requerimiento->name_table = "detalle_requerimiento";
        $this->requerimiento->update($arr_update,$arr_where);


        $this->requerimiento->name_table = "hist_req_ticket";
        $arr = array(
                'id_usuario' => $this->input->post('id_usuario'),
                'est_requerimiento' => '1',
                'id_detalle_requerimiento' => $this->input->post('id_detalle_requerimiento'),
                'id_detalle_requerimiento_remp' => ''
        );
        $this->requerimiento->insert_requerimiento($arr);

        $this->requerimiento->name_table = "hist_requerimiento";
            $arr_insert = array(
                               'id_detalle_requerimiento' => $this->input->post('id_detalle_requerimiento'),
                               'comet_hist_req' => 'Cambio de estado a en proceso',
                               'id_usuario' => $this->input->post('id_usuario')  
                          );

            $this->requerimiento->insert_requerimiento($arr_insert);
    }

    public function r_read_usuario()
    {
    	$this->json_format($this->requerimiento->r_read_usuario());
    }


    public function r_list_data()
    {
        
        if ( $this->input->get('exportar') == '1' ){
        	header('Content-type: application/vnd.ms-excel');
			header("Content-Disposition: attachment; filename=reporte-".date('Y-m-d H:i:s').".xls");
			header("Pragma: no-cache");
			header("Expires: 0");
        }

        $array = array();
        $this->requerimiento->fec_ini_cierre = $this->input->get('fec_ini_cierre');
        $this->requerimiento->fec_hasta_cierre = $this->input->get('fec_hasta_cierre');
        $this->requerimiento->id_usuario_asig = $this->input->get('id_usuario_asig');
        $tipo_requerimiento = $this->requerimiento->select_tipo();

        $fechaInicio=strtotime($this->requerimiento->fec_ini_cierre);
		$fechaFin=strtotime($this->requerimiento->fec_hasta_cierre);


		$html = "";

        foreach ($tipo_requerimiento as $tipo_requerimiento__ )
        {   
        	$html.= "<h5><span style='cursor: pointer' onclick='ver_grafico(".$tipo_requerimiento__->id_tip_requerimiento.")'>".$tipo_requerimiento__->des_tip_requerimiento."&nbsp&nbsp&nbsp&nbsp<i class='fa fa-bar-chart'></i></span></h5><hr>";
        	$html.= "<table class='table table-bordered'>";
            $html.= "<thead>";
            $html.= "<tr>";
            $html.= "<th scope='col'>Fecha</th>";

            $this->requerimiento->id_tip_requerimiento = $tipo_requerimiento__->id_tip_requerimiento;
            foreach ( $this->requerimiento->select_sub_tipo_() as $sub_tipo_requerimiento_) {
               $html.= "<th scope='col'>".$sub_tipo_requerimiento_->des_sub_tip_requerimiento."</th>";
            }
            $html.= "</tr>";
            $html.= "</thead>";
            $html.= "<tbody>";

            for($fecha=$fechaInicio; $fecha<=$fechaFin; $fecha+=86400){
            	$html.= "<tr>";
            	$html.= "<td scope='row'><center>".date("Y-m-d", $fecha)."</center></td>";
	            foreach ( $this->requerimiento->select_sub_tipo_() as $sub_tipo_requerimiento_) {
                    $html.= "<td><center>".$this->requerimiento->list_data_reporte($sub_tipo_requerimiento_->id_sub_tip_requerimiento,date("Y-m-d", $fecha))."</center></td>";
	            }
	            $html.= "</tr>";
            }

            $html.= "</tbody>";
            $html.= "</table>";
        }

        echo $html;
    }

    public function restablecer_contrasena()
    {
        $this->load->view('modulos/restablecer_contrasena');
    }

    public function data_grafico()
    {
		header('Access-Control-Allow-Origin: *');
        $array = array();
        $this->requerimiento->fec_ini_cierre = '2019-06-10';
        $this->requerimiento->fec_hasta_cierre = '2019-06-21';
        $this->requerimiento->id_usuario_asig = 9;
        $tipo_requerimiento = $this->requerimiento->select_tipo();

        $fechaInicio=strtotime($this->requerimiento->fec_ini_cierre);
		$fechaFin=strtotime($this->requerimiento->fec_hasta_cierre);

        foreach ($tipo_requerimiento as $tipo_requerimiento__ )
        {     
            $this->requerimiento->id_tip_requerimiento = $tipo_requerimiento__->id_tip_requerimiento;
            foreach ( $this->requerimiento->select_sub_tipo_() as $sub_tipo_requerimiento_) {
            	$array[] = array('name' => $sub_tipo_requerimiento_->des_sub_tip_requerimiento, 'data' => array(1,2,3,4,5,6,8,9));

            	/*
                for($fecha=$fechaInicio; $fecha<=$fechaFin; $fecha+=86400){

                	
                	$array['datos'][$i]['sub_tipo_requerimiento_'][$j]['data_grafico'][] = $this->requerimiento->list_data_reporte($sub_tipo_requerimiento_->id_sub_tip_requerimiento,date("Y-m-d", $fecha));
                	$array['datos'][$i]['sub_tipo_requerimiento_'][$j]['data_table'][] = array('fecha' => date("Y-m-d", $fecha),'total' => $this->requerimiento->list_data_reporte($sub_tipo_requerimiento_->id_sub_tip_requerimiento,date("Y-m-d", $fecha)));
				}
				$j++;*/
            }

        }
         
        $this->json_format($array);
    }

    public function read_det_reserva()
    { 
        $this->requerimiento->id_reser_equipo = $this->input->get('id_reser_equipo');
        $this->json_format($this->requerimiento->read_det_reserva());
    }

    public function delete_reserva()
    {
        $arr_where = array( 'id_reser_equipo' => $this->input->post('id_reser_equipo'));
        $arr_update = array( 'est' => '0');
        $this->requerimiento->name_table = "reserva_equipo";
        $this->requerimiento->update($arr_update,$arr_where);
    }
    
    public function establecer_contrasena_update()
    {
        $arr_where = array( 'id_usuario' => $this->session->userdata('id_usuario'));
        $arr_update = array( 
                              'est_pass' => '0',
                              'pass_vis' => $this->input->post('nueva_contrasena'),
                              'pass_inc' => md5($this->input->post('nueva_contrasena'))
        );
        $this->requerimiento->name_table = "usuario";
        $this->requerimiento->update($arr_update,$arr_where);
    }

    public function load_sub_requerimientos()
    {
        $this->json_format($this->requerimiento->load_sub_requerimientos());
    }

    public function insert_sub_tipo()
    {  
        $this->requerimiento->name_table = "sub_tipo_requerimiento";
        $this->requerimiento->insert_requerimiento((array) $this->input->post()); 
    }

    public function alert_reserva()
    {
        $resp = $this->requerimiento->alert_reserva();
        $ts = ( !empty($resp) ) ? $resp :  0 ;
        $this->json_format($ts);
    }

    public function terminar_reserva()
    { 
        $arr_where = array( 'id_reser_equipo' => $this->input->post('id_reser_equipo'));
        $arr_update = array( 'est_alert' => '0');
        $this->requerimiento->name_table = "reserva_equipo";
        $this->requerimiento->update($arr_update,$arr_where);         
    }

       /******************SLA***************/

    public function read_sla()
    {   
        $this->requerimiento->tip_sla_des = $this->input->post('tip_sla_des');
        $this->requerimiento->id_sub_tip_requerimiento = $this->input->post('id_sub_tip_requerimiento');
        $this->requerimiento->id_tip_requerimiento = $this->input->post('id_tip_requerimiento');
        if ( !empty($this->requerimiento->read_sla()) )
            $return = $this->requerimiento->read_sla();
        else
            $return = 0;
        
        $this->json_format($return);
    }

    public function ins_upd_sla()
    {
        $this->requerimiento->name_table = "detalle_sla";
        if ( $this->input->get('cond_insert_up') == 'update' ) {
             $arr_where = array( 'id_det_sla' => $this->input->get('id_det_sla'));
             $this->requerimiento->update((array)$this->input->post(),$arr_where);
        } else {
             $this->requerimiento->insert_requerimiento((array) $this->input->post()); 
        }
    }


    public function sla_programacion_TA()
    {
        $fecha = date('Y-m-d H:i:s');
        $_SLA_active = "SELECT * FROM detalle_sla dsla INNER JOIN tip_sla_des tsla ON tsla.id_tip_sla = dsla.tip_sla_des WHERE tsla.est = '1'";
        $___active_SLA = $this->db->query($_SLA_active)->result();
        $_pendientes_no_asignados = "SELECT r.id_detalle_requerimiento,r.titulo_requerimiento,r.desc_requerimiento,r.id_sub_tip_requerimiento,r.id_usuario,r.date_requerimiento_apertura,r.req_prioridad,r.est 
FROM detalle_requerimiento r
JOIN sub_tipo_requerimiento s ON r.id_sub_tip_requerimiento = s.id_sub_tip_requerimiento
WHERE r.est = '2' AND r.id_usuario_asig = '0' AND s.id_tip_requerimiento = '2' AND DATE_FORMAT(date_requerimiento_apertura, '%Y-%m-%d') >= CURDATE()";
        $___data_pendientes = $this->db->query($_pendientes_no_asignados)->result();
        $hora_actual = date('H:i:s');
        $hora_inicio = '07:00:00';
        $hora_fin = '20:55:00';
        $tiempos = 0;
        if ( strtotime($hora_inicio) <= strtotime($hora_actual) AND strtotime($hora_fin) >= strtotime($hora_actual) ) {
            foreach ( $___data_pendientes as $pendientes ) {   
                foreach ( $___active_SLA as $sla) {
                    if ( $sla->des_tip_sla == 'General' ) {
                       if ( $pendientes->req_prioridad == '1' )
                            $tiempos = strtotime('+'. $sla->TA_sla_alta_min .' minute', strtotime( $pendientes->date_requerimiento_apertura ) );
                       else if ( $pendientes->req_prioridad == '2' )
                            $tiempos = strtotime('+'. $sla->TA_sla_media_min .' minute', strtotime( $pendientes->date_requerimiento_apertura ) );
                       else if ( $pendientes->req_prioridad == '3' )
                            $tiempos = strtotime('+'. $sla->TA_sla_baja_min .' minute', strtotime( $pendientes->date_requerimiento_apertura ) );
                       if ( strtotime($fecha) > $tiempos ) {
                            $arr_where = array( 'id_detalle_requerimiento' => $pendientes->id_detalle_requerimiento);
                            $arr_update = array( 'TA' => 'Incumplimiento de SLA');
                            $this->requerimiento->name_table = "detalle_requerimiento";
                            $this->requerimiento->update($arr_update,$arr_where);
                       }
                    } else  {
                        if  ( $pendientes->id_sub_tip_requerimiento == $sla->id_sub_tip_requerimiento ) {
                            if ( $pendientes->req_prioridad == '1' )
                                 $tiempos = strtotime('+'. $sla->TA_sla_alta_min .' minute', strtotime( $pendientes->date_requerimiento_apertura ) );
                            else if ( $pendientes->req_prioridad == '2' )
                                 $tiempos = strtotime('+'. $sla->TA_sla_media_min .' minute', strtotime( $pendientes->date_requerimiento_apertura ) );
                            else if ( $pendientes->req_prioridad == '3' )
                                 $tiempos = strtotime('+'. $sla->TA_sla_baja_min .' minute', strtotime( $pendientes->date_requerimiento_apertura ) );
                            if ( strtotime($fecha) > $tiempos ) {
                                    $arr_where = array( 'id_detalle_requerimiento' => $pendientes->id_detalle_requerimiento);
                                    $arr_update = array( 'TA' => 'Incumplimiento de SLA');
                                    $this->requerimiento->name_table = "detalle_requerimiento";
                                    $this->requerimiento->update($arr_update,$arr_where);
                            }
                        }
                    }
                }                  
            }
        }
        
        /* reserva */

        $this->auto_no_devolucion_reserva();

        /**********************************************************************************************************************************************/
 
    }


    public function sla_programacion_TS()
    { 
        
        
        $fecha = date('Y-m-d H:i:s');
        $_SLA_active = "SELECT * FROM detalle_sla dsla INNER JOIN tip_sla_des tsla ON tsla.id_tip_sla = dsla.tip_sla_des WHERE tsla.est = '1'";
        $___active_SLA = $this->db->query($_SLA_active)->result();

        $__pendientes_asignados = "SELECT r.id_detalle_requerimiento,r.titulo_requerimiento,r.desc_requerimiento,r.id_sub_tip_requerimiento,r.id_usuario,r.date_requerimiento_apertura,r.req_prioridad,r.est 
FROM detalle_requerimiento r
JOIN sub_tipo_requerimiento s ON r.id_sub_tip_requerimiento = s.id_sub_tip_requerimiento
WHERE r.est IN ('1','3')
AND DATE_FORMAT(r.date_requerimiento_apertura, '%Y-%m-%d') >= CURDATE() AND s.id_tip_requerimiento = '2'";

        $___data_pendientes = $this->db->query($__pendientes_asignados)->result();
        $___tiempo = 0;
        $___cal_tiempo = 0;
        $___fec_tiempo_inicial = 0;
        $___TS_tiempo_ = 0;
        $___tiempo_validar = 0;
        $___tiempo_pausa = 0;


        foreach ($___data_pendientes as $__pendientes) {

            $sql_hist = "SELECT * FROM hist_req_ticket WHERE id_detalle_requerimiento = '".$__pendientes->id_detalle_requerimiento."' AND est_requerimiento != '2'";
            $result_hist = $this->db->query($sql_hist)->result();

            foreach ( $result_hist as $_hist ) { 

                if ( $_hist->est_requerimiento == '1' ){
                     $___tiempo+= 0;
                     $___fec_tiempo_inicial = $_hist->fec_hist_req;
                }
                
                if ( $_hist->est_requerimiento == '3' ) {
                     $___tiempo+= $this->cal_time($_hist->fec_hist_req,$___fec_tiempo_inicial);
                     $___tiempo_pausa = 1;
                }
                
            }
            
            if ( $___tiempo_pausa == 0 ) {
                 $___tiempo = $this->cal_time($___fec_tiempo_inicial,$fecha);
            }


            foreach ( $___active_SLA as $sla) {


                if ( $sla->des_tip_sla == 'General' ) {

                     if ( $__pendientes->req_prioridad == '1' ) {
                          if ( $___tiempo > $sla->TS_sla_alta_min ) {
                               $arr_where = array( 'id_detalle_requerimiento' => $__pendientes->id_detalle_requerimiento);
                               $arr_update = array( 'TS' => 'Incumplimiento de SLA','TTS' => $___tiempo );
                               $this->requerimiento->name_table = "detalle_requerimiento";
                               $this->requerimiento->update($arr_update,$arr_where);
                          }
                     }

                     if ( $__pendientes->req_prioridad == '2' ) {
                          if ( $___tiempo > $sla->TS_sla_media_min ) {
                               $arr_where = array( 'id_detalle_requerimiento' => $__pendientes->id_detalle_requerimiento);
                               $arr_update = array( 'TS' => 'Incumplimiento de SLA','TTS' => $___tiempo );
                               $this->requerimiento->name_table = "detalle_requerimiento";
                               $this->requerimiento->update($arr_update,$arr_where);
                          }
                     }

                     if ( $__pendientes->req_prioridad == '3' ) {
                          if ( $___tiempo > $sla->TS_sla_baja_min ) {
                               $arr_where = array( 'id_detalle_requerimiento' => $__pendientes->id_detalle_requerimiento);
                               $arr_update = array( 'TS' => 'Incumplimiento de SLA','TTS' => $___tiempo );
                               $this->requerimiento->name_table = "detalle_requerimiento";
                               $this->requerimiento->update($arr_update,$arr_where);
                          } 
                     }
                } else {

                     if  ( $__pendientes->id_sub_tip_requerimiento == $sla->id_sub_tip_requerimiento ) {
                            if ( $__pendientes->req_prioridad == '1' ) {
                                if ( $___tiempo > $sla->TS_sla_alta_min ) {
                                       $arr_where = array( 'id_detalle_requerimiento' => $__pendientes->id_detalle_requerimiento);
                                       $arr_update = array( 'TS' => 'Incumplimiento de SLA','TTS' => $___tiempo );
                                       $this->requerimiento->name_table = "detalle_requerimiento";
                                       $this->requerimiento->update($arr_update,$arr_where);
                                }  
                            }

                            if ( $__pendientes->req_prioridad == '2' ) {
                                if ( $___tiempo > $sla->TS_sla_media_min ) {
                                       $arr_where = array( 'id_detalle_requerimiento' => $__pendientes->id_detalle_requerimiento);
                                       $arr_update = array( 'TS' => 'Incumplimiento de SLA','TTS' => $___tiempo );
                                       $this->requerimiento->name_table = "detalle_requerimiento";
                                       $this->requerimiento->update($arr_update,$arr_where);
                                } 
                            }

                            if ( $__pendientes->req_prioridad == '3' ) {
                                if ( $___tiempo > $sla->TS_sla_baja_min ) {
                                       $arr_where = array( 'id_detalle_requerimiento' => $__pendientes->id_detalle_requerimiento);
                                       $arr_update = array( 'TS' => 'Incumplimiento de SLA','TTS' => $___tiempo );
                                       $this->requerimiento->name_table = "detalle_requerimiento";
                                       $this->requerimiento->update($arr_update,$arr_where);
                                } 
                            }
                      }
                }
            }

            $___tiempo = 0;
            $___cal_tiempo = 0;
            $___fec_tiempo_inicial = 0;
            $___TS_tiempo_ = 0;
            $___tiempo_validar = 0;
            $___tiempo_pausa = 0;
        }

    }


    /************************************/
    /********* REPORTE ******************/


    public function report_productividad()
    {
        $data = array();
        $this->requerimiento->fec_ini_cierre = $this->input->post('fec_ini_cierre');
        $this->requerimiento->fec_hasta_cierre = $this->input->post('fec_hasta_cierre');  
        $this->requerimiento->tip_reporte = $this->input->post('tip_reporte');
        $rest = $this->requerimiento->report_productividad();
        foreach ( $rest as $row ) {
    
            if ( $row->tipo_requerimiento == 'gerencia' ) {
                $data['G_labels'][] = $row->USUARIO;
                $data['G_TERMINADOS'][] = $row->TERMINADOS;
                $data['G_ENPROCESO'][] = $row->ENPROCESO;
                $data['G_PENDIENTE'][] = $row->PENDIENTE;
                $data['G_ENPAUSA'][] = $row->ENPAUSA;
            } else {
                if ( $row->tipo_requerimiento == 'Solicitud' ) {
                    $data['S_labels'][] = $row->USUARIO;
                    $data['S_TERMINADOS'][] = $row->TERMINADOS;
                    $data['S_ENPROCESO'][] = $row->ENPROCESO;
                    $data['S_PENDIENTE'][] = $row->PENDIENTE;
                    $data['S_ENPAUSA'][] = $row->ENPAUSA;
                } else if ( $row->tipo_requerimiento == 'Insidencia' ) {
                    $data['I_labels'][] = $row->USUARIO;
                    $data['I_TERMINADOS'][] = $row->TERMINADOS;
                    $data['I_ENPROCESO'][] = $row->ENPROCESO;
                    $data['I_PENDIENTE'][] = $row->PENDIENTE;
                    $data['I_ENPAUSA'][] = $row->ENPAUSA;
                }
            }
        }
        $this->json_format($data);
    } 

    public function load_requerimientos_sistemas()
  {   
      $this->requerimiento->op = $this->input->get('opt');
      $this->requerimiento->est = $this->input->get('est');
      $this->requerimiento->req_prioridad = $this->input->get('req_prioridad');
      $this->requerimiento->id_usuario = $this->input->get('id_usuario');
      $this->requerimiento->id_tip_requerimiento = $this->input->get('id_tip_requerimiento');
      $this->requerimiento->muestra_tikets = $this->input->get('muestra_tikets');
      $this->requerimiento->fec_ini_cierre = $this->input->get('start');
      $this->requerimiento->fec_hasta_cierre = $this->input->get('end');
      $this->requerimiento->fil_fec = $this->input->get('fec');
      $this->json_format($this->requerimiento->load_requerimientos_m_sistemas());
  }
       
    public function report_solucion()
    {  
         header('Content-type: application/vnd.ms-excel');
         header("Content-Disposition: attachment; filename=reporte-tiempo-soluci�n-".date('Y-m-d H:i:s').".xls");
         header("Pragma: no-cache");
         header("Expires: 0");
         $___tiempo = 0;
         $___fec_tiempo_inicial = 0;
         $sql = "SELECT id_detalle_requerimiento FROM detalle_requerimiento WHERE est = '0' AND TTR = 0";
         $detalle_requerimiento = $this->db->query($sql)->result();
         foreach ( $detalle_requerimiento as $_detalle_requerimiento_row ) {
             $sql_hist = "SELECT * FROM hist_req_ticket WHERE id_detalle_requerimiento = '".$_detalle_requerimiento_row->id_detalle_requerimiento."' AND est_requerimiento != '2'";
             $result_hist = $this->db->query($sql_hist)->result();
             foreach ( $result_hist as $__hist ) {
                if ( $__hist->est_requerimiento == '1' ){
                     $___tiempo+= 0;
                     $___fec_tiempo_inicial = $__hist->fec_hist_req;
                }
                if ( $__hist->est_requerimiento == '3' ) 
                     $___tiempo+= $this->cal_time($__hist->fec_hist_req,$___fec_tiempo_inicial);

                if ( $__hist->est_requerimiento == '0' )
                     $___tiempo+= $this->cal_time($___fec_tiempo_inicial,$__hist->fec_hist_req);
             }
             $arr_where = array( 'id_detalle_requerimiento' => $_detalle_requerimiento_row->id_detalle_requerimiento);
             $arr_update = array( 'TTR' => $___tiempo);
             $this->requerimiento->name_table = "detalle_requerimiento";
             $this->requerimiento->update($arr_update,$arr_where);
             $___tiempo = 0;
             $___fec_tiempo_inicial = 0;
         }
        $usuarios_sistemas = $this->requerimiento->usuarios_sistemas();
        $this->requerimiento->fec_ini_cierre = $this->input->get('fec_ini_cierre');
        $this->requerimiento->fec_hasta_cierre = $this->input->get('fec_hasta_cierre');
        $html = "";
        $html.= "<table border='1'>";
        $html.= "<tr>";
        $html.= "<td style='background: #303641;color: #fff'><b>NUN. TICKET<b></td>";
        $html.= "<td style='background: #303641;color: #fff'><b>TITULO<b></td>";
        $html.= "<td style='background: #303641;color: #fff'><b>TIPO</b></td>";
        $html.= "<td style='background: #303641;color: #fff'><b>SUB TIPO</b></td>";
        $html.= "<td style='background: #303641;color: #fff'><b>PRIORIDAD</b></td>";
        $html.= "<td style='background: #303641;color: #fff'><b>TT(d�as)</b></td>";
        $html.= "<td style='background: #303641;color: #fff'><b>TT(horas)</b></td>";
        $html.= "<td style='background: #303641;color: #fff'><b>TT(min)</b></td>";
        $html.= "<td style='background: #303641;color: #fff'><b>FEC. APERTURA</b></td>";
        $html.= "<td style='background: #303641;color: #fff'><b>FEC. CIERRE</b></td>";

        $html.= "</tr>";
        foreach ( $usuarios_sistemas as $__usuarios ) {
           $this->requerimiento->id_usuario_asig = $__usuarios->id_usuario;
           $html.= "<tr><td colspan='10' style='background: #003282;color: #fff'>" . utf8_decode($__usuarios->des_usuario) . "</td></tr>";
           foreach ($this->requerimiento->requerimientos_terminados() as $__req  ) {
                $html.= "<tr>";
                $html.= "<td>". $__req->id_detalle_requerimiento ."</td>";
                $html.= "<td>". utf8_decode($__req->titulo_requerimiento) ."</td>";
                $html.= "<td>". $__req->tipo_requerimiento ."</td>";
                $html.= "<td>". $__req->des_sub_tip_requerimiento ."</td>";
                $html.= "<td>". $__req->prioridad ."</td>";
                $html.= "<td>". round(( $__req->TTR / 60 ) / 24,2) ."</td>";
                $html.= "<td>". round(( $__req->TTR / 60 ),2) ."</td>";
                $html.= "<td>". round( $__req->TTR,2) ."</td>";
                $html.= "<td>". $__req->fec_apertura ."</td>";
                $html.= "<td>". $__req->fec_cierre ."</td>";  
                $html.= "</tr>";
           }
        }
        $html.= "</table>";
        echo $html;
    }

    public function report_subtipo()
    {   

        $this->requerimiento->fec_ini_cierre = $this->input->post_get('fec_ini_cierre');
        $this->requerimiento->fec_hasta_cierre = $this->input->post_get('fec_hasta_cierre');
        if ( $this->input->get('op') == 'grafico' ) { 

            $total_solicitud = 0;
            $total_incidencia = 0;
            $arr_data = [];
            $return_data = [];
            
            foreach ( $this->requerimiento->report_subtipo() as $row ) {
                if ( $row->tipo_requerimiento == 'Solicitud' )
                     $total_solicitud+= $row->TOTAL;
                else if ( $row->tipo_requerimiento == 'Incidencia' )
                     $total_incidencia+= $row->TOTAL;
                $arr_data[] = array( 'tipo_requerimiento' => $row->tipo_requerimiento, 'des_sub_tip_requerimiento' => $row->des_sub_tip_requerimiento, 'TOTAL' => $row->TOTAL );
            }
       
            foreach ($arr_data as $row_) {
                 if ( $row_['tipo_requerimiento'] == 'Solicitud' ) {
                     $return_data['label_solicitud'][] = $row_['des_sub_tip_requerimiento'];
                     $return_data['data_solicitudes'][] = round(($row_['TOTAL'] * 100) / $total_solicitud);
                } else {
                     $return_data['label_incidencia'][] = $row_['des_sub_tip_requerimiento'];
                     $return_data['data_incidencia'][] = round(($row_['TOTAL'] * 100) / $total_incidencia);
                }
            }

            $this->json_format($return_data);

        } else if ( $this->input->get('op') == 'exportar' ) {
            

            $data_solicitud = $this->requerimiento->report_subtipo_exportar();
            $data_agrupada = $this->groupArray((array)$data_solicitud,'tipo_requerimiento');

            $des_sub_tip_requerimiento = "";

            header('Content-type: application/vnd.ms-excel');
            header("Content-Disposition: attachment; filename=ReporteSubtipos-".date('Y-m-d H:i:s').".xls");
            header("Pragma: no-cache");
            header("Expires: 0");
            
            $html = "";
            $html.= "<table border='1'>";
            $html.= "<tr>";
            $html.= "<td style='background: #303641;color: #fff'><b>NUN. TICKET<b></td>";
            $html.= "<td style='background: #303641;color: #fff'><b>TITULO<b></td>";
            $html.= "<td style='background: #303641;color: #fff'><b>ESTADO<b></td>";
            $html.= "<td style='background: #303641;color: #fff'><b>PRIORIDAD</b></td>";
            $html.= "<td style='background: #303641;color: #fff'><b>TA</b></td>";
            $html.= "<td style='background: #303641;color: #fff'><b>TS</b></td>";
            $html.= "<td style='background: #303641;color: #fff'><b>TR(min)</b></td>";
            $html.= "<td style='background: #303641;color: #fff'><b>RESPONSABLE</td>";
            $html.= "<td style='background: #303641;color: #fff'><b>FEC. APERTURA</b></td>";
            $html.= "<td style='background: #303641;color: #fff'><b>FEC. CIERRE</b></td>";
            $html.= "</tr>";

            foreach ( $data_agrupada as $cabecera ) {
               $html.= "<tr>";
               $html.= "<td colspan='10' style='background: #003282;color: #fff'>".$cabecera['tipo_requerimiento']."</td>";
               $html.= "</tr>";
               foreach ( $cabecera['groupeddata'] as $detalle ) {
                  if ( $des_sub_tip_requerimiento != $detalle['des_sub_tip_requerimiento'] ) 
                       $html.= "<tr><td colspan='10' style='background: #1aa3e6;color: #fff'>".$detalle['des_sub_tip_requerimiento']."</td></tr>";
                  
                  $html.= "<tr>";
                  $html.= "<td>T".str_pad($detalle['id_detalle_requerimiento'],6,'0',STR_PAD_LEFT)."</td>";
                  $html.= "<td>".utf8_decode($detalle['titulo_requerimiento'])."</td>";
                  $html.= "<td>".$detalle['est_des']."</td>";
                  $html.= "<td>".$detalle['prioridad']."</td>";
                  if ( $detalle['TA'] == '0' )
                       $html.= "<td><center>-</center></td>";
                  else
                       $html.= "<td style='background: #ef5350 !important;color: #fff !important' >".$detalle['TA']."</td>";

                  if ( $detalle['TS'] == '0' ) {
                       $html.= "<td><center>-</center></td>";
                  } else { 
                       $html.= "<td style='background: #ef5350 !important;color: #fff !important' >".$detalle['TS']."</td>";  
                  }
                  $html.= "<td>".$detalle['TTR']."</td>";     
                  $html.= "<td>".utf8_decode($detalle['des_usuario'])."</td>";
                  $html.= "<td>".$detalle['date_requerimiento_apertura']."</td>";
                  $html.= "<td>".$detalle['date_requerimiento_cierre']."</td>";
                  $html.= "</tr>";
                  $des_sub_tip_requerimiento = $detalle['des_sub_tip_requerimiento'];
               }
               
            }

            $html.= "</table >";
            echo $html;

        }
    }

 
/*********************************************/

/**************HORARIOS**************/

    public function read_horarios()
    {   
        $this->requerimiento->id_usuario = $this->input->post('id_usuario');
        $this->json_format($this->requerimiento->horario_usuario());
    }

    public function insert_update_horario()
    {
        
        $return = "";
        if ( $this->input->get('op') == 'insertar' ) {
             $sql = "SELECT * FROM horario_usuario WHERE hor_dia = '".$this->input->post('hor_dia')."' AND id_usuario = '".$this->input->post('id_usuario')."' LIMIT 1";
             $resp = $this->db->query($sql)->row();

             if ( empty($resp) ) {
                  $this->requerimiento->name_table = "horario_usuario";
                  $this->requerimiento->insert_requerimiento((array) $this->input->post());
                  $return = 1;
             } else {
                  $return = 'Ya existe registro de ese D�a';
             }

        } else if ( $this->input->get('op') == 'update' ){
            $arr_where = array( 'id_horario_usuario' => $this->input->get('id_horario_usuario'));
            $arr_update = (array) $this->input->post();
            $this->requerimiento->name_table = "horario_usuario";
            $this->requerimiento->update($arr_update,$arr_where);
            $return = 1;
        }

        echo $return;

    }

    public function update_estado_requerimiento()
    {

        $id_detalle_requerimiento = $this->input->post('id_detalle_requerimiento');
        $est = $this->input->post('est');
        $id_usuario = $this->input->post('id_usuario');

        $this->requerimiento->name_table = "hist_req_ticket";
        $arr = array(
                      'id_usuario' => $id_usuario,
                      'est_requerimiento' => $est,
                      'id_detalle_requerimiento' => $id_detalle_requerimiento,
                      'id_detalle_requerimiento_remp' => ''
        );
        $this->requerimiento->insert_requerimiento($arr);

        $this->requerimiento->name_table = "hist_requerimiento";
        $arr_insert = array(
                             'id_detalle_requerimiento' => $id_detalle_requerimiento,
                             'comet_hist_req' => 'Cambio de estado a ' . $this->estados__($est),
                             'id_usuario' => $id_usuario  
                          );

        $this->requerimiento->insert_requerimiento($arr_insert);


        $arr_where = array( 'id_detalle_requerimiento' => $id_detalle_requerimiento);
        $arr_update = array( 
                               'est' => $est
                           );


        $this->requerimiento->name_table = "detalle_requerimiento";
        $this->requerimiento->update($arr_update,$arr_where);

    }

    public function update_ruta()
    {   
        $arr_where = array( 'id_detalle_requerimiento' => $this->input->post('id_detalle_requerimiento'));
        $arr_update = array('path_o_cod_video' => $this->input->post('valor'));
        $this->requerimiento->name_table = "detalle_requerimiento";
        $this->requerimiento->update($arr_update,$arr_where);  
    }

    /************************************/

    /************************************/

	function json_format($data = array())
	{
		header('Content-Type: application/json');
		echo json_encode($data);
	}
        
        function cal_time($fecha1,$fecha2)
    {
        if ( !empty($fecha1) AND  !empty($fecha2) ) {
             $date1 = new DateTime($fecha1);
             $date2 = new DateTime($fecha2);
             $diff = $date1->diff($date2);
             $return = ( ( ( $diff->days * 24 ) + $diff->h )  * 60 ) + ( $diff->i );
        } else {
             $return = 0;
        }
        return $return;
    }



    /*---------------------------------
    -----------------------------------
    ------CONFIGURACION DE ------------
    -----------RESERVA-----------------
    -----------------------------------
    ---------------------------------*/

    public function conf_reserva()
    {   
        $arr_where  = array( 'id_equipo' => $this->input->post('id_equipo'));
        $this->requerimiento->name_table = "conf_equipo";
        $this->requerimiento->update((array)$this->input->post(),$arr_where);
    }

    public function read_data_conf_equipo()
    {
        $this->requerimiento->id_equipo = $this->input->get('id_equipo');
        $this->json_format($this->requerimiento->read_data_conf_equipo());
    } 

    function estados__($pos)
    { 
       $est = array('0' => 'Terminado', '1' => 'En proceso', '2' => 'Pendiente', '3' => 'Pausa' );
       return $est[$pos];
    }

    public function desbloqueo()
    {
        $sql = "UPDATE reserva_cont_devoluciones_no_cumplidas SET est = '0' WHERE id_usuario = '". $this->input->post('id_usuario')."'";
        $this->db->query($sql);
    }


    
/************************************************************/

    public function auto_eliminar_reserva()
    {
        //$this->requerimiento->auto_eliminar_reserva(); 
    }

    function auto_no_devolucion_reserva()
    {   
        $__conf = $this->requerimiento->list_conf_equipo();
        $sql = "SELECT * FROM reserva_equipo WHERE fecha = CURDATE() AND ADDTIME(fec_desde, '00:10:00') >= TIME(NOW()) 
                AND ADDTIME(fec_desde, '00:10:00') <= ADDTIME(TIME(NOW()), '00:05:00') AND id_equipo != 2 AND est = '1' AND est_devolucion = '0'";
        $data__ = $this->db->query($sql)->result();

        foreach ($data__ as $row_reservas)
        {
            $this->requerimiento->name_table = "reserva_cont_devoluciones_no_cumplidas";
            $arr_insert = array(
                                   'id_usuario' => $row_reservas->id_usuario,
                                   'id_reser_equipo' => $row_reservas->id_reser_equipo
                                );
            $this->requerimiento->insert_requerimiento($arr_insert);
        }        
    }




    public function read_count_n_dev()
    {
       $arr = array();
       $__conf = $this->requerimiento->list_conf_equipo();
       foreach ( $this->requerimiento->Count_x_bloqueo_ususario(0) as $row ) {
          $cong = ( $row->total > $__conf->conf_nun_bloqueo ) ? 1 : 0;
          $arr[] = array(
                          'cong' => $cong,
                          'id_usuario' => $row->id_usuario,
                          'des_usuario' => $row->des_usuario,
                          'des_area' => $row->des_area,
                          'total' => $row->total
                        ); 
       }

       $this->json_format($arr);
    }

/************************************************************/


    function groupArray($array,$groupkey)
    {
         if (count($array)>0) {
            $keys = array_keys($array[0]);
            $removekey = array_search($groupkey, $keys);  
            if ($removekey===false)
                return array("Clave \"$groupkey\" no existe");
            else
                unset($keys[$removekey]);
            $groupcriteria = array();
            $return=array();
            foreach($array as $value) {
                $item=null;
                foreach ($keys as $key) {
                    $item[$key] = $value[$key];
                }
                $busca = array_search($value[$groupkey], $groupcriteria);
                if ($busca === false) {
                    $groupcriteria[]=$value[$groupkey];
                    $return[]=array($groupkey=>$value[$groupkey],'groupeddata'=>array());
                    $busca=count($return)-1;
                }
                $return[$busca]['groupeddata'][]=$item;
            }
            return $return;
         } else
            return array();
    }


/*********************************************************************/

    public function calificacion_requerimiento()
    {
       $id_tip_calificacion = $this->input->post('punt_');
       $id_detalle_requerimiento = $this->input->post('id_detalle_requerimiento');
       $sql = 'UPDATE detalle_requerimiento SET id_tip_calificacion = "'. $id_tip_calificacion .'" WHERE id_detalle_requerimiento = "'.$id_detalle_requerimiento.'"';
       $this->db->query($sql);
    }

    public function requerimiento_calificacion(){
        $arr_data = array();
        $id_usuario = $this->input->post('id_usuario');
        $_sql = "SELECT det.titulo_requerimiento,det.id_detalle_requerimiento,usu.des_usuario FROM detalle_requerimiento det
                 JOIN usuario usu ON det.id_usuario_asig = usu.id_usuario
                 WHERE det.id_usuario = '".$id_usuario."' AND det.est = '0' AND id_tip_calificacion = 0 AND det.date_requerimiento_apertura >= CURDATE() ORDER BY det.id_detalle_requerimiento";
        $_data = $this->db->query($_sql)->result();

        foreach ($_data as $__data) {
          $arr_data[] = array(
                                'titulo_requerimiento' => $__data->titulo_requerimiento,
                                'id_detalle_requerimiento_' => str_pad($__data->id_detalle_requerimiento,6,'0',STR_PAD_LEFT),
                                'id_detalle_requerimiento__' => $__data->id_detalle_requerimiento,
                                'des_usuario_asignado' => $__data->des_usuario
                             );
        }
            
        $this->json_format($arr_data);

    }

    public function report_calificacion()
    {
        header('Content-type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=reporte-tiempo - calificaci�n - ".date('Y-m-d H:i:s').".xls");
        header("Pragma: no-cache");
        header("Expires: 0");

        $usuarios_sistemas = $this->requerimiento->usuarios_sistemas();
        $this->requerimiento->fec_ini_cierre = $this->input->get('fec_ini_cierre');
        $this->requerimiento->fec_hasta_cierre = $this->input->get('fec_hasta_cierre');
        $html = "";
        $html.= "<table border='1'>";
        $html.= "<tr>";
        $html.= "<td style='background: #303641;color: #fff'><b>NUN. TICKET<b></td>";
        $html.= "<td style='background: #303641;color: #fff'><b>TITULO<b></td>";
        $html.= "<td style='background: #303641;color: #fff'><b>TIPO</b></td>";
        $html.= "<td style='background: #303641;color: #fff'><b>SUB TIPO</b></td>";
        $html.= "<td style='background: #303641;color: #fff'><b>PRIORIDAD</b></td>";
        $html.= "<td style='background: #303641;color: #fff'><b>�REA</b></td>";
        $html.= "<td style='background: #303641;color: #fff'><b>USUARIO</b></td>";
        $html.= "<td style='background: #303641;color: #fff'><b>CALIFICACI�N</b></td>";
        $html.= "</tr>";

        foreach ( $usuarios_sistemas as $__usuarios ) {
           $this->requerimiento->id_usuario_asig = $__usuarios->id_usuario;
           $html.= "<tr><td colspan='8' style='background: #003282;color: #fff'>" . utf8_decode($__usuarios->des_usuario) . "</td></tr>";
           foreach ($this->requerimiento->requerimientos_terminados() as $__req  ) {
                $html.= "<tr>";
                $html.= "<td>". $__req->id_detalle_requerimiento ."</td>";
                $html.= "<td>". utf8_decode($__req->titulo_requerimiento) ."</td>";
                $html.= "<td>". $__req->tipo_requerimiento ."</td>";
                $html.= "<td>". $__req->des_sub_tip_requerimiento ."</td>";
                $html.= "<td>". $__req->prioridad ."</td>";
                $html.= "<td>". $__req->des_usuario ."</td>";
                $html.= "<td>". $__req->des_area ."</td>";
                $html.= "<td>". $__req->calificacion ."</td>";
                $html.= "</tr>";
           }
        }
        
        $html.= "</table>";
        echo $html;

    }
 
/*************************************************************************/

    public function proveedor_reserva()
  {
      $sql = "SELECT est_spt_m,tel_res_spt_m,nun_tick_spt_m,email_res_spt_m,responsable_spt_m,motivo_spt_m,est_aten_spt_m,CASE ac_spt_m WHEN 1 THEN 'Servidor del sistema' WHEN 2 THEN 'Desktop' END AS ac_spt_m,
              CASE amb_spt_m WHEN 1 THEN 'Produccion' WHEN 2 THEN 'De prueba' WHEN 0 THEN '-----' END AS amb_spt_m,
              CASE mens_spt_m WHEN 'on' THEN 'Coordinar con los usuarios del sistema para que no trabajen dentro del horario programado' WHEN 'off' THEN '----' END AS mens_spt_m FROM tbl_spt_m WHERE id_spt_m = '".$this->input->get('id_spt_m')."'";
      header('Content-Type: application/json');
      echo json_encode($this->db->query($sql)->row());
  }

  public function reserva_tf()
  {
      $sql = "SELECT CONCAT(spt.fec_spt_m,'T',spt.hor_ini_spt_m) AS 'start', CONCAT(spt.fec_spt_m,'T',spt.hor_fin_spt_m) AS 'end', spt.id_spt_m AS 'id', 
              CONCAT(spt.responsable_spt_m,'-',CASE spt.ac_spt_m WHEN 1 THEN 'Servidor del sistema' WHEN 2 THEN 'Desktop' END,'-',spt.motivo_spt_m ) AS 'title',
              spt_user.color AS 'backgroundColor'
              FROM tbl_spt_m spt 
              JOIN stp_user spt_user ON spt.id_user_stp = spt_user.id_user_stp
              WHERE spt.est_spt_m = '1'
              UNION
              SELECT CONCAT(spt.fec_spt_m,'T',spt.hor_ini_spt_m) AS 'start', CONCAT(spt.fec_spt_m,'T',spt.hor_fin_spt_m) AS 'end', spt.id_spt_m AS 'id', 
              CONCAT(spt.responsable_spt_m,'-',CASE spt.ac_spt_m WHEN 1 THEN 'Servidor del sistema' WHEN 2 THEN 'Desktop' END,'-',spt.motivo_spt_m ) AS 'title',
              '#9e9e9e' AS 'backgroundColor'
              FROM tbl_spt_m spt 
              JOIN stp_user spt_user ON spt.id_user_stp = spt_user.id_user_stp
              WHERE spt.est_spt_m = '0'";
      $this->json_format($this->db->query($sql)->result()); 
  }

  public function delete_pro_reserva()
  {
      $sql = "UPDATE tbl_spt_m SET est_spt_m = '0' WHERE id_spt_m = '".$this->input->post('id_spt_m')."'";
      $this->db->query($sql);
  }  

  public function atender_pro_reserva()
  {

      $sql_usuario = "SELECT f_insertar_mayusculaMinuscula(des_usuario) as 'des_usuario' FROM usuario WHERE id_usuario = '".$this->input->post('id_usuario')."'";
      $des_usuario = $this->db->query($sql_usuario)->row();
      
      $sql_update = "UPDATE tbl_spt_m SET est_aten_spt_m = '1', datos_aten_spt_m = '".$des_usuario->des_usuario."' WHERE id_spt_m = '".$this->input->post('id_spt_m')."'";
      $this->db->query($sql_update);
    
  } 

  public function report_creacion_ticket()
  {   
      
      header('Content-type: application/vnd.ms-excel');
      header("Content-Disposition: attachment; filename=Reporte.xls");
      header("Pragma: no-cache");
      header("Expires: 0");
      $fec_ini_cierre = $this->input->get('fec_ini_cierre');
      $fec_hasta_cierre = $this->input->get('fec_hasta_cierre');

      $fec_ini = explode('-',$fec_ini_cierre);
      $fec_cierre = explode('-',$fec_hasta_cierre);

      $html = "";
      $html.= "<table border='1'>"; 
      $html.= "<tr>";
      $html.= "<td colspan='2'><b><center>H</center></b></td>";
      
      for ($meses = intval($fec_ini[1]); $meses <= intval($fec_cierre[1]); $meses++) {
           $numero = cal_days_in_month(CAL_GREGORIAN, $meses, 2019);
           $html.= "<td colspan='".$numero."' height='40'><b><center>".$this->meses($meses)."</center></b></td>";
      }
     
      $html.= "</tr>";
      $html.= "<tr>";
      $html.= "<td></td>";
      $html.= "<td></td>";

      for ($meses = intval($fec_ini[1]); $meses <= intval($fec_cierre[1]); $meses++) {
           $numero = cal_days_in_month(CAL_GREGORIAN, $meses, 2019);
           for ($i = 1; $i <= $numero; $i++) {  $html.= "<td>".$this->str_pad_r($i)."</td>"; }
      }

      for ($fec = 1; $fec < 24; $fec++) {
          $html.= "<tr>";
          $html.= "<td>". $fec ."</td>";
          $html.= "<td>". ($fec + 1) ."</td>";
          for ($meses = intval($fec_ini[1]); $meses <= intval($fec_cierre[1]); $meses++) {
               $numero = cal_days_in_month(CAL_GREGORIAN, $meses, 2019);
               for ($i = 1; $i <= $numero; $i++) { 
                   $sql = "SELECT count(*) as total FROM detalle_requerimiento WHERE MONTH(date_requerimiento_apertura) = '".$meses."' AND DAY(date_requerimiento_apertura) = '".$i."' AND HOUR(date_requerimiento_apertura) = '".$fec."'";
                   $resp = $this->db->query($sql)->row();
                   $total = ( $resp->total ==  0 ) ? '-' : $resp->total;
                   $html.= "<td style='mso-number-format:0.00'>". $total ."</td>";
               }
          }  
          $html.= "</tr>";
      }
      $html.= "</tr></table>";
      echo $html;
  }

  public function report_termino_ticket()
  {   
      
      header('Content-type: application/vnd.ms-excel');
      header("Content-Disposition: attachment; filename=Reporte.xls");
      header("Pragma: no-cache");
      header("Expires: 0");
      $fec_ini_cierre = $this->input->get('fec_ini_cierre');
      $fec_hasta_cierre = $this->input->get('fec_hasta_cierre');

      $fec_ini = explode('-',$fec_ini_cierre);
      $fec_cierre = explode('-',$fec_hasta_cierre);

      $html = "";
      $html.= "<table border='1'>"; 
      $html.= "<tr>";
      $html.= "<td colspan='2'><b><center>H</center></b></td>";
      
      for ($meses = intval($fec_ini[1]); $meses <= intval($fec_cierre[1]); $meses++) {
           $numero = cal_days_in_month(CAL_GREGORIAN, $meses, 2019);
           $html.= "<td colspan='".$numero."' height='40'><b><center>".$this->meses($meses)."</center></b></td>";
      }
     
      $html.= "</tr>";
      $html.= "<tr>";
      $html.= "<td></td>";
      $html.= "<td></td>";

      for ($meses = intval($fec_ini[1]); $meses <= intval($fec_cierre[1]); $meses++) {
           $numero = cal_days_in_month(CAL_GREGORIAN, $meses, 2019);
           for ($i = 1; $i <= $numero; $i++) {  $html.= "<td>".$this->str_pad_r($i)."</td>"; }
      }

      for ($fec = 1; $fec < 24; $fec++) {
          $html.= "<tr>";
          $html.= "<td>". $fec ."</td>";
          $html.= "<td>". ($fec + 1) ."</td>";
          for ($meses = intval($fec_ini[1]); $meses <= intval($fec_cierre[1]); $meses++) {
               $numero = cal_days_in_month(CAL_GREGORIAN, $meses, 2019);
               for ($i = 1; $i <= $numero; $i++) { 
                   $sql = "SELECT count(*) as total FROM detalle_requerimiento WHERE MONTH(date_requerimiento_cierre) = '".$meses."' AND DAY(date_requerimiento_cierre) = '".$i."' AND HOUR(date_requerimiento_cierre) = '".$fec."'";
                   $resp = $this->db->query($sql)->row();
                   $total = ( $resp->total ==  0 ) ? '-' : $resp->total;
                   $html.= "<td style='mso-number-format:0.00'>". $total ."</td>";
               }
          }  
          $html.= "</tr>";
      }
      $html.= "</tr></table>";
      echo $html;
  }


  /************************************************************************/
  /************************************************************************/
  /************************************************************************/
  /************************************************************************/
  /************************************************************************/
  /************************************************************************/
  
  public function cmi()
  {
      $total             = 0;
      $seismeses         = "";
      $meses             = array();
      $meses__           = array( '01' => 'Enero', '02' => 'Febrero', '03' => 'Marzo', '04' => 'Abril', '05' => 'Mayo', '06' => 'Junio',
                                  '07' => 'Julio', '08' => 'Agosto', '09' => 'Septiembre', '10' => 'Octubre', '11' => 'Noviembre', '12' => 'Diciembre' );
      if ( $this->input->get('option_perfil') != '2' ) 
      {
           $usuario = ( $this->input->get('des_tip_usu') == 'supervisor' ) ? 0 : $this->input->get('id_usuario');
           for ($i=0; $i < 6; $i++){ $meses[] = date('Y-m', strtotime("-$i month")); }
                $meses = array_reverse($meses);
           foreach ( $meses as $mes_s ) { $seismeses .= "'".$mes_s."',";  }
           $seismeses = trim($seismeses,',');
           
           $result_productuvidad = $this->requerimiento->graf_productividad_cmi($seismeses,$this->input->get('id_tip_requerimiento'),$usuario);
           foreach ( $result_productuvidad as $productividad )
           {
              $total = $productividad->TERMINADOS + $productividad->PENDIENTE + $productividad->ENPROCESO + $productividad->ENPAUSA;
              $data_productividad_grafico['fecha'][]      = $productividad->FECHA;
              $data_productividad_grafico['TERMINADOS'][] = ( $productividad->TERMINADOS  * 100 > 0 ) ? round(( $productividad->TERMINADOS  * 100 ) / $total,2)  : 0 ;
              $data_productividad_grafico['PENDIENTE'][]  = ( $productividad->PENDIENTE * 100 > 0 ) ? round(( $productividad->PENDIENTE * 100 ) / $total,2) : 0;
              $data_productividad_grafico['ENPROCESO'][]  = ( $productividad->ENPROCESO * 100 > 0 ) ? round(( $productividad->ENPROCESO * 100 ) / $total,2) : 0;
              $data_productividad_grafico['ENPAUSA'][]    = ( $productividad->ENPAUSA * 100 > 0 ) ? round(( $productividad->ENPAUSA * 100 ) / $total,2) : 0;
           }
           
           $result_calificacion  = $this->requerimiento->graf_calificacion_cmi($seismeses,$this->input->get('id_tip_requerimiento'),$usuario);
           foreach ( $result_calificacion as $calificacion ) 
           {
              $total = $calificacion->MUYMALO + $calificacion->POBRE + $calificacion->NIBIENNIMAL + $calificacion->BUENO + $calificacion->MUYBUENO;
              $data_calificacion_grafico['FECHA'][]       = $calificacion->FECHA;
              $data_calificacion_grafico['POBRE'][]       = ( $calificacion->POBRE * 100 > 0 ) ? round(( $calificacion->POBRE * 100 ) / $total) : 0;
              $data_calificacion_grafico['NIBIENNIMAL'][] = ( $calificacion->NIBIENNIMAL * 100 > 0 ) ? round(( $calificacion->NIBIENNIMAL * 100 ) / $total) : 0;
              $data_calificacion_grafico['BUENO'][]       = ( $calificacion->BUENO * 100 > 0 ) ? round(( $calificacion->BUENO * 100 ) / $total) : 0;
              $data_calificacion_grafico['MUYBUENO'][]    = ( $calificacion->MUYBUENO * 100 > 0 ) ? round(( $calificacion->MUYBUENO * 100 ) / $total) : 0;
              $data_calificacion_grafico['MUYMALO'][]     = ( $calificacion->MUYMALO * 100 > 0 ) ? round(( $calificacion->MUYMALO * 100 ) / $total) : 0;
           }

           $result_SLA_TA        = $this->requerimiento->graf_sla_TA_cmi($seismeses,$this->input->get('id_tip_requerimiento'),$usuario);

           foreach ($result_SLA_TA as $SLA_TA) 
           {
              $total  = $SLA_TA->SICUMPLIO + $SLA_TA->NOCUMPLIO;
              $data_sla_ta['FECHA'][]                     = $SLA_TA->FECHA;
              $data_sla_ta['SICUMPLIO'][]                 = ( $SLA_TA->SICUMPLIO * 100 > 0 ) ? round(( $SLA_TA->SICUMPLIO * 100 ) / $total) : 0;
              $data_sla_ta['NOCUMPLIO'][]                 = ( $SLA_TA->NOCUMPLIO * 100 > 0 ) ? round(( $SLA_TA->NOCUMPLIO * 100 ) / $total) : 0; 
           }
           
           $result_SLA_TS        = $this->requerimiento->graf_sla_TS_cmi($seismeses,$this->input->get('id_tip_requerimiento'),$usuario);

           foreach ($result_SLA_TS as $SLA_TS) 
           {
              $total  = $SLA_TS->SICUMPLIO + $SLA_TS->NOCUMPLIO;
              $data_sla_ts['FECHA'][]                     = $SLA_TS->FECHA;
              $data_sla_ts['SICUMPLIO'][]                 = ( $SLA_TS->SICUMPLIO * 100 > 0 ) ? round(( $SLA_TS->SICUMPLIO * 100 ) / $total) : 0;
              $data_sla_ts['NOCUMPLIO'][]                 = ( $SLA_TS->NOCUMPLIO * 100 > 0 ) ? round(( $SLA_TS->NOCUMPLIO * 100 ) / $total) : 0; 
           }


           $arry_requerimientos_terminados = array();
           $arry_requerimientos_pendientes = array();
           $anio                           = date('Y');
           $mes___                         = [];

           foreach ( $meses__ as $key => $mes__ ) {
               $mes___[] = $mes__;
               $resp_requemientos_pendientes = $this->requerimiento->graf_productividad_anio($key,$anio,$this->input->get('id_tip_requerimiento'),$usuario,'pendientes');
               $arry_requerimientos_pendientes['label'][] = $mes__;
               $arry_requerimientos_pendientes['valor'][] = $resp_requemientos_pendientes->total;
               
               $resp_requerimientos_terminados = $this->requerimiento->graf_productividad_anio($key,$anio,$this->input->get('id_tip_requerimiento'),$usuario,'terminados');
               $arry_requerimientos_terminados['label'][] = $mes__;
               $arry_requerimientos_terminados['valor'][] = $resp_requerimientos_terminados->total;
               $arry_requerimientos_terminados['total'][] = $resp_requemientos_pendientes->total + $resp_requerimientos_terminados->total;  
           }
             
           $json = array(
                            'data_productividad_grafico' =>  $data_productividad_grafico,
                            'data_calificacion_grafico'  =>  $data_calificacion_grafico,
                            'data_sla_ta'                =>  $data_sla_ta,
                            'data_sla_ts'                =>  $data_sla_ts,
                            'PRODUCTIVIDAD_ANIO'         =>  array(
                                                                    'meses'      => $mes___,
                                                                    'terminado'  => $arry_requerimientos_terminados,
                                                                    'pendiente'  => $arry_requerimientos_pendientes
                                                             )

                        );
           }    

           $this->json_format($json);

  }


  function meses($mes)
  {
      $meses = array(  
                        1  => 'ENERO',
                        2  => 'FEBRERO',
                        3  => 'MARZO',
                        4  => 'ABRIL',
                        5  => 'MAYO',
                        6  => 'JUNIO',
                        7  => 'JULIO',
                        8  => 'AGOSTO',
                        9  => 'SEPTIEMBRE',
                        10 => 'OCTUBRE',
                        11 => 'NOVIEMBRE',
                        12 => 'DICIEMBRE'
                    );

      return $meses[$mes];
  }
  


  function str_pad_r($numero)
  {
      return str_pad($numero,2,"0",STR_PAD_LEFT);
  }

  function estados() {
      $arr_estados___ = array( 
                                 0 => 'Terminado',
                                 1 => 'En proceso',
                                 2 => 'Pendiente',
                                 3 => 'Pausa'
                             );

      return $arr_estados___[$this->estado];
  }

  function calificacion_()
  {
      $arr_calificacion__ = array(
                                   1 => 'Muy malo',
                                   2 => 'Pobre',
                                   3 => 'Ni bien, ni mal',
                                   4 => 'Bueno' ,
                                   5 => 'Muy bueno'
                            );

      return $arr_calificacion__[$this->calificacion];
  }

  public function autocomplete()
  {
      $param = $this->input->post('term');
      if ( isset($param['filters'][0]['value']) ) {
           $this->requerimiento->des_usuario = $param['filters'][0]['value'];
           $return = $this->requerimiento->autocomplete();
           $this->json_format($return); 
      } 
  }


  function sla_ta___()
  {
      $arr_sla_ta =  array(

                            '0'                     => 'Cumplimiento de SLA',
                            'Incumplimiento de SLA' => 'Incumplimiento de SLA'
                     );
      return $arr_sla_ta[$this->sla_ta_];
  } 

 
}

/* End of file Sis_Requerimiento_C.php */
/* Location: ./application/controllers/Sis_Requerimiento_C.php */