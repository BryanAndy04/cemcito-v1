  $array = array();
        $this->requerimiento->fec_ini_cierre = $this->input->post('fec_ini_cierre');
        $this->requerimiento->fec_hasta_cierre = $this->input->post('fec_hasta_cierre');
        $this->requerimiento->id_usuario_asig = $this->input->post('id_usuario_asig');
        $tipo_requerimiento = $this->requerimiento->select_tipo();

        $fechaInicio=strtotime($this->requerimiento->fec_ini_cierre);
		$fechaFin=strtotime($this->requerimiento->fec_hasta_cierre);
		 
		$i = 0;
		$j = 0; 

        foreach ($tipo_requerimiento as $tipo_requerimiento__ )
        {     
            $this->requerimiento->id_tip_requerimiento = $tipo_requerimiento__->id_tip_requerimiento;
            $array['datos'][] = array('tipo_requerimiento' => $tipo_requerimiento__->des_tip_requerimiento,'sub_tipo_requerimiento_' => array());
            foreach ( $this->requerimiento->select_sub_tipo_() as $sub_tipo_requerimiento_) {
            	$array['datos'][$i]['sub_tipo_requerimiento_'][] = array('name' => $sub_tipo_requerimiento_->des_sub_tip_requerimiento, 'data_table' => array(), 'data_grafico' => array() );
                for($fecha=$fechaInicio; $fecha<=$fechaFin; $fecha+=86400){

                	
                	$array['datos'][$i]['sub_tipo_requerimiento_'][$j]['data_grafico'][] = $this->requerimiento->list_data_reporte($sub_tipo_requerimiento_->id_sub_tip_requerimiento,date("Y-m-d", $fecha));
                	$array['datos'][$i]['sub_tipo_requerimiento_'][$j]['data_table'][] = array('fecha' => date("Y-m-d", $fecha),'total' => $this->requerimiento->list_data_reporte($sub_tipo_requerimiento_->id_sub_tip_requerimiento,date("Y-m-d", $fecha)));
				}
				$j++;
            }
            $j = 0;
            $i++;

        }