<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$route['login_json'] = 'Requerimiento_C/login_json';
$route['panel'] = 'Requerimiento_C/panel_view';
$route['default_controller'] = 'Requerimiento_C/login_view';
$route['cerrar-session'] = 'Requerimiento_C/login_cerrar';
$route['cerrar-session-pausar'] = 'Requerimiento_C/cerrar_session_pausar';
$route['modules'] = 'Requerimiento_C/modules';
$route['load-requerimientos'] = 'Requerimiento_C/load_requerimientos';
$route['load-requerimientos-sistemas'] = 'Requerimiento_C/load_requerimientos_sistemas';
$route['load-historial-requerimiento'] = 'Requerimiento_C/load_historial_requerimiento';
$route['datalle-requerimiento'] = 'Requerimiento_C/datalle_requerimiento';
$route['tip_requerimiento'] = 'Requerimiento_C/tip_requerimiento';
$route['sub_tip_requerimiento'] = 'Requerimiento_C/sub_tip_requerimiento';
$route['insert-requerimiento'] = 'Requerimiento_C/insert_requerimiento';
$route['insert-historial-requerimiento'] = "Requerimiento_C/insert_historial_requerimiento";
$route['insert-usuario'] = 'Requerimiento_C/insert_usuario';
$route['delete-usuario'] = 'Requerimiento_C/delete_usuario';
$route['detalle-usuario'] = 'Requerimiento_C/detalle_usuario';
$route['update-usuario'] = 'Requerimiento_C/update_usuario';
$route['insert-area'] = 'Requerimiento_C/insert_area';
$route['update-area'] = 'Requerimiento_C/update_area';
$route['delete-area'] = 'Requerimiento_C/delete_area';
$route['detalle-area'] = 'Requerimiento_C/detalle_area';
$route['load-user'] = 'Requerimiento_C/load_user';
$route['load-area'] = 'Requerimiento_C/load_area';
$route['abrir-requerimiento'] = 'Requerimiento_C/abrir_requerimiento';
$route['insert-historia'] = 'Requerimiento_C/insert_historia';
$route['load-tipo-usuario'] = 'Requerimiento_C/load_tipo_usuario';
$route['list-reservas'] = 'Requerimiento_C/list_reservas';
$route['insert-reserva'] = 'Requerimiento_C/insert_reserva';
$route['subir-adjunto'] = 'Requerimiento_C/subir_adjunto';
$route['importar-datos'] = 'Requerimiento_C/insert_usuario_masivo';
$route['list-sist'] = 'Requerimiento_C/list_sist';
$route['update-responsable'] = 'Requerimiento_C/update_responsable';
$route['load-tikets-com'] = 'Requerimiento_C/load_tikets_com';
$route['mensaje'] = 'Requerimiento_C/mensaje'; 
$route['read-mensaje'] = 'Requerimiento_C/read_mensaje';
$route['notificaciones'] = 'Requerimiento_C/notificaciones';
$route['panel-vidoteca'] = 'Requerimiento_C/panel_vidoteca';
$route['reporte-read-usuario'] = "Requerimiento_C/r_read_usuario";
$route['reporte-read-data'] = "Requerimiento_C/r_list_data";
$route['exportar-reporte-read-data'] = "Requerimiento_C/exportar_reporte_read_data";
$route['reporte-read-data-grafico'] = "Requerimiento_C/data_grafico";
$route['restablecer-contrasena'] = "Requerimiento_C/restablecer_contrasena";
$route['read-det-reserva'] = "Requerimiento_C/read_det_reserva";
$route['delete-reserva'] = "Requerimiento_C/delete_reserva";
$route['restablecer-contrasena-update'] = 'Requerimiento_C/establecer_contrasena_update';
$route['load-sub-requerimientos'] = 'Requerimiento_C/load_sub_requerimientos';
$route['delete-sub-tipo'] = 'Requerimiento_C/delete_sub_tipo';
$route['insert-sub-tipo'] = 'Requerimiento_C/insert_sub_tipo';
/// alerta proyector //

$route['alert-reserva'] = 'Requerimiento_C/alert_reserva';
$route['terminar-reserva'] = 'Requerimiento_C/terminar_reserva';

///////////SLA ////////////////
$route['read-sla'] = 'Requerimiento_C/read_sla';
$route['ins-upd-sla'] = 'Requerimiento_C/ins_upd_sla';
$route['TA-SLA'] = 'Requerimiento_C/sla_programacion_TA';
$route['TS-SLA'] = 'Requerimiento_C/sla_programacion_TS';

// reportes //s
$route['report-productividad'] = "Requerimiento_C/report_productividad";
$route['report-solucion'] = "Requerimiento_C/report_solucion";
$route['report-subtipo'] = "Requerimiento_C/report_subtipo";
//*** REPORTE AUMENTO***/

$route['report-creacion-ticket'] = 'Requerimiento_C/report_creacion_ticket';
$route['report-termino-ticket'] = 'Requerimiento_C/report_termino_ticket';

//////////////////////////////////////////////////////////////////
$route['update-ruta'] = 'Requerimiento_C/update_ruta';
$route['update-estado-requerimiento'] = 'Requerimiento_C/update_estado_requerimiento';
//////////////////////////////////////////////////////////////////

//RESERVA CONFIGURACION //
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

$route['conf-reserva'] = "Requerimiento_C/conf_reserva";
$route['read-data-conf-equipo'] = "Requerimiento_C/read_data_conf_equipo"; 
$route['auto-no-devolucion-reserva'] = 'Requerimiento_C/auto_no_devolucion_reserva';
$route['read-count-n-dev'] = 'Requerimiento_C/read_count_n_dev';
$route['devolucion-reserva'] = 'Requerimiento_C/devolucion_equipo';
$route['requerimiento-calificacion'] = 'Requerimiento_C/requerimiento_calificacion';

/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

/// HORARIOS ///
$route['read-horarios'] = 'Requerimiento_C/read_horarios';
$route['insert-update-horario'] = 'Requerimiento_C/insert_update_horario';

$route['404_override'] = '';
$route['translate_uri_dashes'] = TRUE;