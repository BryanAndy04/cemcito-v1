<?php $this->load->view('includes/header'); ?>

    <div class="login-area login-s2">
        <div class="container">
            <div class="login-box">
                <form>
                    <div class="login-form-head">
                        <img src="<?php echo base_url('assets/images/icon/CENcito.png') ?>" style="width: 200px;">
                        <h4>Sistema de Gestión de Soporte v.1.1.0</h4>
                    </div>
                    <div class="login-form-body">
                        <div class="form-gp">
                            <label for="txt_usuario">Usuario</label>
                            <input type="text" id="txt_usuario">
                            <i class="ti-email"></i>
                        </div>
                        <div class="form-gp">
                            <label for="txt_contrasena">Contraseña</label>
                            <input type="password" id="txt_contrasena" onkeypress="enter(event)">
                            <i class="ti-lock"></i>
                        </div>
                        <div class="submit-btn-area">
                            <button id="btn_login" onclick="auch_log(this)" type="button">Ingresar <i class="ti-arrow-right"></i></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <footer>
                <div class="copyright">
                    Desarrollado por el Área de Sistemas CEM
                </div>
        </footer>

    </div>
    



<?php $this->load->view('includes/footer'); ?>