<?php $this->load->view('includes/header'); ?>

<div class="page-container">
        <!-- sidebar menu area start -->
        <?php $this->load->view('includes/sidebar') ?>
        <!-- sidebar menu area end -->
        <!-- main content area start -->
        <div class="main-content">

            <!-- page title area start -->
               <?php $this->load->view('includes/header_detalle.php') ?>
            <!-- page title area end -->
            <div class="main-content-inner" id='__content_panel'></div>
        </div>
        <!-- main content area end -->
    </div>
    <!-- page container area end -->
<?php $this->load->view('includes/footer'); ?>