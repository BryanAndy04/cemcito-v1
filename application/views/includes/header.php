<!doctype html>
<html lang="es">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>.:SIS - REQUERIMIENTO:.</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/png" href="<?php echo base_url('assets/images/icon/favicon.ico');?>">
        <link rel='stylesheet' href="<?php echo base_url('assets/css/kanon.css') ?>">
        <link rel='stylesheet' href="<?php echo base_url('assets/css/material.css') ?>">
        <link rel='stylesheet' href="<?php echo base_url('assets/css/mobile.css') ?>">
        <link rel='stylesheet' href="<?php echo base_url('assets/css/core.css') ?>">
        <link rel='stylesheet' href="<?php echo base_url('assets/css/daygrid.css') ?>">
        <link rel='stylesheet' href="<?php echo base_url('assets/css/timegrid.css') ?>">
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome.min.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/themify-icons.css') ?> ">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/metisMenu.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/slicknav.min.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/typography.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/default-css.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/styles.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/responsive.css') ?>">
        <!-- modernizr css -->
        <script src="assets/js/vendor/modernizr-2.8.3.min.js"></script>

    </head>

    <body>
        <input type="hidden" id='id_usuario' value="<?php echo $this->session->userdata('id_usuario') ?>">
        <input type="hidden" id='des_tip_usu' value="<?php echo $this->session->userdata('des_tip_usu') ?>">
        <div id="preloader">
            <div class="loader"></div>
        </div>

        <div id='preloader_inst'></div>