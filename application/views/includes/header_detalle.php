<div class="page-title-area">

    <div class="row align-items-center">
        <div class="col-sm-6">

            <div class="nav-btn pull-left">
                <span></span>
                <span></span>
                <span></span>
            </div>

            <div class="breadcrumbs-area clearfix">
                <h4 class="page-title pull-left" id="tittle">Inicio</h4>
            </div>
        </div>
        <div class="col-sm-6 clearfix">
            <div class="user-profile pull-right">
                <img class="avatar user-thumb" src="assets/images/author/avatar.png" alt="avatar">
                <h4 class="user-name dropdown-toggle" data-toggle="dropdown"><?php echo $this->session->userdata('des_usuario') ?><i class="fa fa-angle-down"></i></h4>
                <div class="dropdown-menu">
                    
                    <?php  if ( $this->session->userdata('des_tip_usu') == 'sistemas' ) { ?>
                        <a  href="javascript:void(0)" class="dropdown-item" onclick="cerrar_session_pausar()">Cerrar Session y pausar requerimientos</a>
                    <?php } else { ?>
                        <a  href="javascript:void(0)" class="dropdown-item" onclick="cerrar_session()">Cerrar Session</a>
                    <?php } ?>
                </div> 
            </div>
        </div>
    </div>
    
</div>