        <audio id="notif_audio"><source src="<?php echo base_url('assets/sounds/notify.ogg');?>" type="audio/ogg"><source src="<?php echo base_url('assets/sounds/notify.mp3');?>" type="audio/mpeg"><source src="<?php echo base_url('assets/sounds/notify.wav');?>" type="audio/wav"></audio>
        <div id="ventana_"></div>   
        <span id="notification" style="display:none;"></span>
        <script src="<?php echo base_url('assets/js/logica/pusher.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/logica/push.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/vendor/jquery-2.2.4.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/kendo.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/core.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/daygrid.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/timegrid.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/lenguaje.js') ?>"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
        <script src="<?php echo base_url('assets/js/char.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/utils.js') ?>"></script>
        <!-- bootstrap 4 js -->
        <script src="<?php echo base_url('assets/js/popper.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/metisMenu.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.slimscroll.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.slicknav.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/scripts.js') ?> "></script>
        <!-- logica -->
        <script src="<?php echo base_url('assets/js/logica/funciones.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/logica/logica.js') ?>"></script>

        <script id="successTemplate" type="text/x-kendo-template">
            <div class="upload-success">
                <img src="../content/web/notification/success-icon.png" />
                <h3>#= message #</h3>
            </div>
        </script>
        
        <script type="text/javascript">


                

                //

                var url = "<?php echo base_url() ?>"

                if( $('#des_tip_usu').val() == 'sistemas' || $('#des_tip_usu').val() == 'supervisor' ) { 
                     proyector()
                }
                
                Pusher.logToConsole = true

                var pusher = new Pusher('c61cd220d9d9bf274c8b', {
                    cluster: 'mt1',
                    forceTLS: true
                });

                var channel = pusher.subscribe('my-channel')
                channel.bind('my-event', function(data) {

                    if ( data.tip_req == 'nuevo' ) {
                         
                        if ( $('#des_tip_usu').val() == data.tipo_usuario ) {
                             Push.create(data.titulo, {
                                body: data.descripcion,
                                icon: url + "assets/images/icon/CENcito.png",
                                timeout: 50000,
                                vibrate: [200, 100, 200, 100, 200, 100, 200],
                                onClick: function () {
                                    window.focus()
                                    $('li[class="active"]').click()
                                }
                             });
                        }

                    } else if ( data.tip_req == 'respuesta' ) {
                        
                        
                        if ( $('#des_tip_usu').val() == data.tipo_usuario || $('#des_tip_usu').val() == 'supervisor' ) {
                                if ( $('#id_usuario').val() == data.usuarios ) {
                                     Push.create(data.titulo, {
                                        body: data.descripcion,
                                        icon: url + "assets/images/icon/CENcito.png",
                                        timeout: 50000,
                                        vibrate: [200, 100, 200, 100, 200, 100, 200],
                                        onClick: function () {
                                            window.focus()
                                            $('li[class="active"]').click()
                                        }
                                     });
                                }
                        }

                    }

                })



                /*
                var pusher = new Pusher('fe7f4c70a2b421c74676', {
                      cluster: 'us3',
                      forceTLS: true
                });






                var channel = pusher.subscribe('my-channel');
                    channel.bind('my-event', function(data) {

                        
                        alert(JSON.stringify(data));




/*
                        Push.create(data.titulo, {
                            body: "Nuevo Requerimiento",
                            icon: url + "assets/images/icon/CENcito.png",
                            timeout: 500000,
                            onClick: function () {
                                window.focus();
                            }
                        });

                       
                    })
                    */


                    function proyector(){
                        setInterval(function(){
                            $.ajax({
                                url:  url + 'alert-reserva',
                                type: 'GET',
                                dataType: 'json',
                                success: function(json){
                                    if ( json == 0 ) {

                                    } else {
                                        $('#notif_audio')[0].play();
                                        Push.create(json.titulo, {
                                            body: 'Inicio : '  + json.fec_desde + ', Termino : ' + json.fec_hasta + ', Motivo : ' + json.res_motivo + ', Usuario : ' + json.usuario ,
                                            icon: url + "assets/images/icon/CENcito.png",
                                            timeout: 10000,
                                            vibrate: [200, 100, 200, 100, 200, 100, 200],
                                            onClick: function () {
                                                window.focus()
                                                $('li[mdl="reservar_proyector"]').click()
                                            }
                                         });
                                    }
                                }
                            })
                            
                            $.post( url + 'TA-SLA')
                            $.post( url + 'TS-SLA')

                        },20000)
                    }

                 
        </script>
    </body>
</html>
