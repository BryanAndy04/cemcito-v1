<div class="sidebar-menu">
    <div class="sidebar-header">
        <div class="logo">
            <a href="javascript:void(0)"><img src="<?php echo base_url("assets/images/img/logo_especialidades.png"); ?>" alt="logo"></a>
        </div>
    </div>
    <div class="main-menu">
        <div class="menu-inner">
            <nav>
                <ul class="metismenu" id="menu">
                    <?php  if (  $this->session->userdata('des_tip_usu') == "usuario" || ( $this->session->userdata('des_tip_usu') == "supervisor" AND $this->session->userdata('id_area') != "4" ) ) { ?>
                                 <li op="click"  onclick="options(this)" title="Inicio" mdl="inicio" class="active"><a href="javascript:void(0)"><i class="ti-map-alt"></i> <span>Inicio</span></a></li>
                                 <li op="click" onclick="options(this)"  title="Requerimiento" mdl="requerimiento"><a href="javascript:void(0)"> <i class="ti-map-alt"></i> <span>Requerimiento</span></a></li>
                                 <li op="click" onclick="options(this)" title="Reserva" mdl="reservar_proyector"><a href="javascript:void(0)"><i class="ti-receipt"></i> <span>Reserva</span></a></li>
                                 <li op="click" onclick="options(this)" title="Videoteca" mdl="videoteca"><a href="javascript:void(0)"><i class="ti-receipt"></i> <span>Videoteca</span></a></li>
                    <?php  }  else if  ( $this->session->userdata('des_tip_usu') == "sistemas" || ( $this->session->userdata('des_tip_usu') == "supervisor" AND $this->session->userdata('id_area') == "4" ) ) { ?>

                                 <li class="active">> <a href="javascript:void(0)" aria-expanded="true"><i class="ti-palette"></i><span>CEMCITO</span></a>
                                 <ul class="collapse">
                                 <li op="click"  onclick="options(this)" title="Tablero de control(CMI)" mdl="tabl_control" class="active"><a href="javascript:void(0)"><span>Tablero de control(CMI)</span></a></li>
                                 <li op="click"  onclick="options(this)" title="Requerimientos" mdl="requerimientos_sis"><a href="javascript:void(0)"> <span>Requerimientos</span></a></li>
                                 <li op="click"  onclick="options(this)" title="Mantenimiento de Usuario" mdl="mantusuario"><a href="javascript:void(0)"><span>Usuarios</span></a></li>
                                 <li op="click"  onclick="options(this)" title="Mantenimiento de Áreas" mdl="mantarea"><a href="javascript:void(0)"><span>Área</span></a></li>
                                 <li op="click"  onclick="options(this)" title="Mantenimiento de Sub tipos de requerimiento" mdl="sub_tipos"><a href="javascript:void(0)"> <span>Sub Tipos</span></a></li>
                                 <!--<li op="click"  onclick="options(this)" title="Horarios" mdl="horarios"><a href="javascript:void(0)"><span>Horarios</span></a></li> --> 
                                 <li op="click"  onclick="options(this)" title="Mensaje" mdl="mensaje"><a href="javascript:void(0)"><span>Mensaje</span></a></li>
                                 <li op="click"  onclick="options(this)" title="Reporte" mdl="reporte"><a href="javascript:void(0)"><span>Reporte</span></a></li>
                                 <li op="click"  onclick="options(this)" title="SLA" mdl="sla"><a href="javascript:void(0)"><span>SLA</span></a></li>
                                 <li op="click"  onclick="options(this)" title="Configuración Reserva" mdl="conf_reserva"><a href="javascript:void(0)"><span>Configuración Reserva</span></a></li>
                                 <li op="click" onclick="options(this)"  title="Requerimiento" mdl="requerimiento_sis_reg"><a href="javascript:void(0)"> <span>Requerimiento Sistemas</span></a></li>
                                 <li op="click" onclick="options(this)" title="Reserva" mdl="reservar_proyector"><a href="javascript:void(0)"><span>Reserva</span></a></li>
                                 </ul> 
                                 </li>
                                 <li > <a href="javascript:void(0)" aria-expanded="true"><i class="ti-palette"></i><span>PROVEEDORES</span></a>
                                       <ul class="collapse">
                                            <li op="click"  onclick="options(this)" title="Control de proveedores" mdl="tabl_control_proveedores"><a href="javascript:void(0)"><span>Control de proveedores</span></a></li>
                                       </ul>
                                 <li > 

                    <?php  } else {  ?> 
                                 <li op="click"  onclick="options(this)" title="Inicio" mdl="inicio" class="active"><a href="javascript:void(0)"><i class="ti-map-alt"></i> <span>Inicio</span></a></li>
                                 <li op="click" onclick="options(this)"  title="Requerimiento" mdl="requerimiento"><a href="javascript:void(0)"> <i class="ti-map-alt"></i> <span>Requerimiento</span></a></li>
                                 <li op="click" onclick="options(this)" title="Reserva" mdl="reservar_proyector"><a href="javascript:void(0)"><i class="ti-receipt"></i> <span>Reserva</span></a></li>
                                 <li op="click" onclick="options(this)" title="Videoteca" mdl="videoteca"><a href="javascript:void(0)"><i class="ti-receipt"></i> <span>Videoteca</span></a></li> 
                    <?php  }  ?> 

                </ul>
            </nav>
        </div>
    </div>
</div>
