<div class="row">
    <div class="col-lg-3 col-ml-3">
    	<div class="row">
            <div class="col-12 top_div">
            	<div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="des_usuario" class="col-form-label">Usuario :<span style="color: red"> (*) </span></label>
                            <select class="custom-select" id='cbo_id_usuario'>
                                <option value='todos'>Seleccione</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="des_usuario" class="col-form-label">Día<span style="color: red"> (*) </span></label>
                            <select class="custom-select" id='hor_dia'>
                            	<option value="1">Lunes</option>
                            	<option value="2">Martes</option>
                                <option value="3">Miercoles</option>
                                <option value="4">Jueves</option>
                                <option value="5">Viernes</option>
                                <option value="6">Sabado</option>
                            </select>
                        </div>
                        <hr>
                         <center>Hora de trabajo</center>
                        <hr>
                        <input type="hidden" id='op_ins_up' value='insertar'>
                        <input type="hidden" id='id_horario_usuario' value='0'>
                        <div class="form-group">
                            <label for="email_usuario" class="col-form-label">Hora de Inicio : <span style="color: red"> (*) </span></label>
                            <input class="form-control time_fec" type="text" id="hor_de_ingreso">
                        </div>
                        <div class="form-group">
                            <label for="email_usuario" class="col-form-label">Hora de Fin : <span style="color: red"> (*) </span></label>
                            <input class="form-control time_fec" type="text" id="hor_de_salida">
                        </div>

                        <hr>
                         <center>Hora de receso</center>
                        <hr>
                        <div class="form-group">
                            <label for="email_usuario" class="col-form-label">Hora de Inicio : <span style="color: red"> (*) </span></label>
                            <input class="form-control time_fec" type="text" id="hor_de_receso_ingreso">
                        </div>
                        <div class="form-group">
                            <label for="email_usuario" class="col-form-label">Hora de Fin : <span style="color: red"> (*) </span></label>
                            <input class="form-control time_fec" type="text" id="hor_de_receso_salida">
                        </div>
                        <center>
                        	<button type="button" class="btn btn-outline-primary mb-3" id="guardar_reserva" tip="insert">Guardar</button>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-9 col-ml-9">
    	<div class="row">
            <div class="col-12 top_div">
            	<div class="card">
                    <div class="card-body">
                        <div class="single-table">
	                        <div class="table-responsive">
	                            <table class="table table-bordered">
	                                <thead class="text-uppercase">
	                                	<tr>
	                                        <th rowspan="2" style="margin: 0px"><center>Día</center></th>
	                                        <th colspan="2"><center>Hora de Trabajo</center></th>
	                                        <th colspan="2"><center>Hora de Receso</center></th>
	                                    </tr>
	                                    <tr>
	                                        <th scope="col"><center>Hora de Entrada</center></th>
	                                        <th scope="col"><center>Hora de Salidad</center></th>
	                                        <th scope="col"><center>Hora de Entrada</center></th>
	                                        <th scope="col"><center>Hora de Salidad</center></th>
	                                    </tr>
	                                </thead>
	                                <tbody id='cesta_horarios'>
	                                  
	                                </tbody>
	                            </table>
	                        </div>
	                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>	

<script type="text/javascript">
    
    var html,
        hor_dia = $('#hor_dia'),
		hor_de_ingreso = $('#hor_de_ingreso'),
		hor_de_salida = $('#hor_de_salida'),
		hor_de_receso_ingreso = $('#hor_de_receso_ingreso'),
		hor_de_receso_salida = $('#hor_de_receso_salida'),
		cbo_id_usuario = $('#cbo_id_usuario'),
		param = {},
		arr_valida = [],
        dia = [ '','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo']

    carga_tipos_requerimiento($('#cbo_id_usuario'),'list-sist',{ f : 0 })
    $('.time_fec').timepicker({
        timeFormat: 'HH:mm:ss',
        interval: 30,
        minTime: '07:00',
        maxTime: '21:00',
        defaultTime: '07:00',
        startTime: '07:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    })

    $('#cbo_id_usuario').change(function(event) {
    	load_requerimiento($(this).val());
    });

    function load_requerimiento(id_usuario) {
        $.ajax({
        	url: url + 'read-horarios',
        	type: 'POST',
        	dataType: 'json',
        	data: { id_usuario : id_usuario },
        	success : function(data) {

               console.log(data) 

        	   html = ''

        	   $.each(data, function(index, val) {
                   html+= '<tr onclick="op_edit(this)" style="cursor: pointer">'
                   html+= '<td style="display:none">'+val.id_horario_usuario+'</td>'
                   html+= '<td style="display:none">'+val.id_usuario+'</td>'
                   html+= '<td dia="'+val.hor_dia+'">'+dia[val.hor_dia]+'</td>'
                   html+= '<td>'+val.hor_de_ingreso+'</td>'
                   html+= '<td>'+val.hor_de_salida+'</td>'
                   html+= '<td>'+val.hor_de_receso_ingreso+'</td>'
                   html+= '<td>'+val.hor_de_receso_salida+'</td>'
                   html+= '</tr>'
        	   });

        	   $('#cesta_horarios').html(html)

        	}
        })
    }

    $('#guardar_reserva').click(function() {

        arr_valida.length = 0

        if ( hor_de_ingreso.val() == 0 )
        	 arr_valida.push('Seleccione Hora de ingreso')

        if ( hor_de_salida.val() == 0 )
        	 arr_valida.push('Seleccione Hora de Salida')	

        if ( hor_dia.val() == 0 )
             arr_valida.push('Seleccione Día')

        if ( cbo_id_usuario.val() == 0 )  
             arr_valida.push('Seleccione Usuario')

        if ( arr_valida.length == 0 ) {
             
             param = {
						id_usuario            :  cbo_id_usuario.val(),
						hor_dia               :  hor_dia.val(),
						hor_de_ingreso        :  hor_de_ingreso.val(),
						hor_de_salida         :  hor_de_salida.val(),
						hor_de_receso_ingreso :  hor_de_receso_ingreso.val(),
						hor_de_receso_salida  :  hor_de_receso_salida.val()
             }

             $.ajax({
             	url: url + 'insert-update-horario?op='+$('#op_ins_up').val()+'&id_horario_usuario='+$('#id_horario_usuario').val(),
             	type: 'POST',
             	data: param,
             	success : function(json){
                    if ( json == 1 ) {
                         load_requerimiento($('#cbo_id_usuario option:selected').val())
                         $('#op_ins_up').val('insertar');
                         $('#id_horario_usuario').val('0');
                         cbo_id_usuario.val("")
                         hor_dia.val(1)
                         hor_de_ingreso.val("")
                         hor_de_salida.val("")
                         hor_de_receso_ingreso.val("")
                         hor_de_receso_salida.val("")
                    } else {
                         kendo.alert(json);
                         $('.k-dialog-titlebar').remove();
                    } 
             	}
             })

        } else {
            html = ''
            for (var i = 1; i <= arr_valida.length; i++)
                html+= "<span style='color:red;font-size:14px'>"+i+"- "+arr_valida[i-1]+"</span></br>"
          
            kendo.alert(html);
            $('.k-dialog-titlebar').remove();
        }
    });


    function op_edit(id) {
        $('#op_ins_up').val('update');
        $('#id_horario_usuario').val($(id).find('td').eq(0).text());
        cbo_id_usuario.val($(id).find('td').eq(1).text())
        hor_dia.val($(id).find('td').eq(2).attr('dia'))
        hor_de_ingreso.val($(id).find('td').eq(3).text())
        hor_de_salida.val($(id).find('td').eq(4).text())
        hor_de_receso_ingreso.val($(id).find('td').eq(5).text())
        hor_de_receso_salida.val($(id).find('td').eq(6).text())
    }


</script>