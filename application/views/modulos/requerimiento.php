    <div class="row">
        <div class="col-md-12 col-lg-12 top_div">
            <div class="card" id='requerimiento'>
                <div class="card-body">
                    <div id='calificacion_requerimiento'>
                        <div class="row" id='calificacion_requerimiento_add'></div>
                    </div>  
                    <div id='req_q'>
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">LISTAR</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">GENERAR TICKET</a>
                            </li>
                        </ul>
                        <div class="tab-content mt-3" id="myTabContent">
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                
                                <select class="custom-select" id='muestra_tikets' style="width: 150px !important" onchange="load_requerimientos()">
                                    <option value='miti'>Mis Tickets</option>
                                    <?php echo ( $this->session->userdata('des_tip_usu') == 'supervisor' ) ? '<option value="todos"> Tickets del Área </option>'  : '' ?>
                                </select>

                                <label> Tipo de Ticket</label>
                                <select class="custom-select" id='tip_tikets' style="width: 150px !important" onchange="load_requerimientos()">
                                    <option value='todos'>Todos</option>
                                    <option value='1'>Solicitud</option>
                                    <option value='2'>Insidencia</option>
                                </select>
                                
                                <label> Estado : </label>
                                <select class="custom-select" id='estado_tikets' style="width: 150px !important" onchange="load_requerimientos()">
                                        <option value="todos">Todos</option>
                                        <option value='2'>Pendiente</option>
                                        <option value='1'>En proceso</option> 
                                        <option value='0'>Terminado</option>
                                        <option value='3'>Pausa</option>
                                </select>

                                <div class="single-table" style="margin-top: 20px" id='grid'></div>

                            </div>
                            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <label for="titulo_requerimiento" class="col-form-label">Título del Requerimiento <span style="color: red"> (*) </span></label>
                                                    <input class="form-control" type="text" id="titulo_requerimiento">
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-form-label">Tipo de Requerimiento <span style="color: red"> (*) </span></label>
                                                    <select class="custom-select" id='tip_requerimiento'><option value=''>Seleccione opción</option></select>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-form-label">Sub Tipo de Requerimiento <span style="color: red"> (*) </span></label>
                                                    <select class="custom-select" id='sub_tip_requerimiento'><option value=''>Seleccione opción</option></select>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-form-label">Prioridad <span style="color: red"> (*) </span></label>
                                                    <select class="custom-select" id='cbo_prioridad'>
                                                        <option value='1'>Alta</option>
                                                        <option value='2'>Media</option>
                                                        <option value='3'>Baja</option>
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <label for="edesc_requerimiento" class="col-form-label">Descripción del Requerimiento <span style="color: red"> (*) </span> </label>
                                                    <textarea class="form-control textarea_des_obj" id="desc_req" style="height: 450px"> </textarea>
                                                </div>

                                                <div class="form-group" id='soli_obj_req' style="display: none">
                                                    <label for="edesc_requerimiento" class="col-form-label">Objeticos de la Solicitud<span style="color: red"> (*) </span> </label>
                                                    <textarea class="form-control textarea_des_obj" id="obj_requerimiento" style="height: 300px"> </textarea>
                                                </div>

                                                <div class="form-group" id='fec_soli_term' style="display: none">
                                                    <label for="fec_termino" class="col-form-label">Fecha estimada de término<span style="color: red"> (*) </span> </label>
                                                    <input  type='text' class="form-control" id="fec_est_termino">
                                                </div>

                                                <form method="post" action="#" enctype="multipart/form-data">
                                                    <div class="form-group">
                                                        <label for="image" class="col-form-label">Adjuntar Documento(evidencia)</label>
                                                        <input class="form-control" type="file" id="image" value='images/'>
                                                    </div>
                                                </form>

                                                <center><button type="button" class="btn btn-outline-primary mb-3" id="guardar_requerimiento">Guardar</button></center>
                                            </div>
                                        </div>
                                    </div>

                                    <div class='col-md-4'>
                                        <div class="card">
                                            <div class="card-body">
                                                <h4 style='color: black'>INCIDENTE</h4>
                                                <span>En la terminología de ITIL, un “INCIDENTE” se define como cualquier suceso que no forme parte del funcionamiento estándar de un servicio y que motive, o pueda motivar, una interrupción o reducción de la calidad de tal servicio y de la productividad del usuario.<br>
                                                Ejemplo :<br>
                                                Error en procesos de facturación, admisión, contable, no enciende la computadora, problemas de hardware,error de seguridad, continuidad de negocio y operaciones diarias,etc.</span>
                                            </div>
                                        </div>

                                        <div class="card">
                                            <div class="card-body">
                                                <h4 style='color: black'>SOLICITUD</h4>
                                                <span>Una “SOLICITUD” se define como peticiones de servicio para proponer un cambio en cualquier componente de la Infraestructura TI o en cualquier aspecto de un Servicio TI. <br>
                                                Ejemplo : <br>
                                                Creación de nuevos reportes, desarrollo de nuevas funcionalidas en el software, requerimiento de monitor, teclados, etc,creacion de perfiles, correos, accesos, capacitaciones
                                                </span>
                                            </div>
                                        </div>

                                        <div class="card">
                                            <div class="card-body">
                                                <h5 style='color: red'>Prioridad Alta</h5>
                                                <span>Se Resuelve en el Día(inmediatamente)</span>
                                                <br>
                                                <span>Error de seguridad, continuidad de negocio y operaciones diarias</span>
                                            </div>
                                        </div>

                                        <div class="card">
                                            <div class="card-body">
                                                <h5 style='color: orange'>Prioridad Media</h5>
                                                <span>Se resuelve hasta en dos días</span>
                                                <br>
                                                <span>Incluyen las tareas, que inciden en la funcionalidad des Software ó Hardware es decir, cosas que no están funcionando como deberían en algún caso, lo que no se supone un error crítico pero que se debe solventar lo antes posible, o algún cambio mínimo en la bae de datos, como ampliar el número de caracteres que va a aceptar un campo de tabla </span>
                                            </div>
                                        </div>

                                        <div class="card">
                                            <div class="card-body">
                                                <h5 style='color: green'>Prioridad Baja</h5>
                                                <span>Se resuelve hasta en tres días</span>
                                                <br>
                                                <span>Aquí se incluye, mejoras de lo que ya funciona, optimizaciones o nuevas funcionalidades que se pueden implemetar</span>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>  
        </div>
    </div>

    </div>
    
    <div class="modal fade bd-example-modal-lg show" id='mdl_detalle_requerimiento' style="display: none">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="mdl_detalle_requerimiento_tittle">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                </div>
                <div class="modal-body" id='modal'>
                    <input type="hidden" id='input_id_detalle_requerimiento'>
                    <div class="row">
                        <div class="col-lg-7 col-ml-7">
                            <div class="row">
                                <div class="col-12">
                                    <p><b>Número de Requerimiento  : </b> <span id='codigo_req'></span></p>
                                    <p><b>Fecha / hora de apertura  : </b> <span id='capertura_req'></span></p>
                                    <p id='fec_est_ter__' style="display: none"><b>Fecha estimada de término  : </b> <span id='fec_est_ter'></span></p>
                                    <p id='fec_est_ter_sis__' style="display: none"><b>Fecha estimada por sistemas : </b> <span id='fec_est_sis_ter'></span></p>
                                    <p><b>Prioridad : </b> <span id='prioridad_req'></span></p>
                                    <p><b>Estado : </b> <span id='estado_req'></span></p>
                                    <p><b>Descargar Adjunto : </b><span id='descargar_evi__' arch="" style="cursor: pointer !important;color: blue"></span></p>
                                    <hr>
                                </div>
                                <div class="col-12" id='descripcion_req'>
                                   <p style="text-align: justify;"><b>Descripción : </b></p>
                                   <div id="_descripcion_req"> </div>
                                </div>
                                <div class="col-12" id="objetivo_solicitud">
                                   <p style="text-align: justify;"><b>Objetivo de la Solitud : </b></p>
                                   <div id="_objetivo_solicitud" > </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-5 col-ml-5">

                            <div class="row">
                                <div class="col-12"> 
                                    <div class="card" style="overflow-y: scroll !important;height: 600px !important">
                                        <div class="card-body">
                                            
                                            <h4 class="header-title">Historial</h4>
                                            <div class="timeline-area" id='cest_historial'>
                                                <div class="card" id='requerimiento'>
                                                    <div class="card-body">
                                                         
                                                    </div>
                                                </div>     
                                            </div>
                                        </div>
                                    </div>
                                </div>   

                                <div class="col-12" id='com_solicitud'>
                                    <input type="hidden" id='input_id_detalle_requerimiento'>
                                    <p> Comentario : </p>
                                    <textarea class="form-control textarea_des_obj" id="req_hist_comentario" style="width: 100% !important" > </textarea>
                                    <hr>
                                    <center><button type="button" class="btn btn-primary" id='save_hist_req'>Guardar comentario</button></center>
                                </div>
                            </div>
                        </div>    
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary"  id='btn_abrir_requerimiento'>Volver Abrir <span id ='btn_tip_requerimiento'></span></button>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">

            $('#req_q').hide()
            $('[data-toggle="tooltip"]').tooltip()
            $('#requerimiento').height($(window).height() - 100) 
            $('#requerimiento').css('overflow-y', 'scroll');

            var arr_validar = new Array(),
            msg = '',
            titulo_requerimiento = $('#titulo_requerimiento'),
            tip_requerimiento = $('#tip_requerimiento'),
            sub_tip_requerimiento = $('#sub_tip_requerimiento'),
            desc_req = $('#desc_req'),ruta_imagen = $('#image'),
            cbo_prioridad = $('#cbo_prioridad'),
            obj_requerimiento = $('#obj_requerimiento'),
            fec_est_termino = $('#fec_est_termino'),
            param,html = ""

            $('#tip_requerimiento').change(function(event) {
                if ( $(this).val() == '1' ){
                    $('#soli_obj_req').css('display','block')
                    $('#fec_soli_term').css('display', 'block');
                } else {
                    $('#soli_obj_req').css('display','none')
                    $('#fec_soli_term').css('display', 'none');
                }
            })

            load_requerimientos()

            $('.textarea_des_obj').kendoEditor({
                tools: [
                    "bold",
                    "italic",
                    "underline",
                    "strikethrough",
                    "justifyLeft",
                    "justifyCenter",
                    "justifyRight",
                    "justifyFull",
                    "insertUnorderedList",
                    "insertOrderedList",
                    "indent",
                    "outdent",
                    "createLink",
                    "unlink",
                    "insertImage",
                    "insertFile",
                    "tableWizard",
                    "createTable",
                    "addColumnLeft",
                    "addColumnRight",
                    "deleteRow",
                    "deleteColumn",
                    "cleanFormatting",
                    "fontName",
                    "fontSize",
                    "foreColor",
                    "backColor"
                ]
            });

            $("#fec_est_termino").kendoDatePicker({ format: "yyyy-MM-dd" });
            $("#fec_est_termino").click(function(){
               $(this).data("kendoDatePicker").open()
            })

            function fun_calificacion(id) {

                var param  = { 
                                  id_detalle_requerimiento : $(id).attr('cod'),
                                  punt_ : $(id).attr('punt')
                             }

                
                    kendo.confirm('Desea asignar esa calificación ?').then(function () {
                          $.post( url  + 'Requerimiento_C/calificacion_requerimiento', param , function(data, textStatus, xhr) {
                                  $('li[class="active"]').click()
                          })  
                    })

                    $('.k-dialog-titlebar').remove();
                     
            }

            function load_requerimientos(){

                    $.post( url + 'requerimiento-calificacion', {  id_usuario : $('#id_usuario').val()  }, function(data, textStatus, xhr) 
                    {
                        console.log(data)
                        if ( data.length > 0 ) {
                             $('#req_q').hide()
                             var tpml = '';
                             $.each(data, function(index, val) {
                                  tpml+= '<div class="col-4 card" style="border: 1px solid;height: 240px;">'
                                      tpml+= '<div class="card-body">'
                                          tpml+= '<span style="font-size: 14px">N° Ticket : </span> <span style="font-size: 13px"> ' + val.id_detalle_requerimiento_ + ' </span><br>'
                                          tpml+= '<span style="font-size: 14px">Titulo : </span> <span style="font-size: 13px"> ' + val.titulo_requerimiento + ' </span><br>'
                                          tpml+= '<span style="font-size: 14px">Responsable : </span> <span style="font-size: 13px"> ' + val.des_usuario_asignado + ' </span><br>'
                                          tpml+= '<span style="font-size: 14px">Valorar : </span> <span style="font-size: 13px"><br><br>'
                                          tpml+= '<span><b> ¿ Cómo fue nuestro servicio para este requerimiento ?</b></span><br><br>'
                                          tpml+= '<center>'
                                              tpml+= ' <div style="position: absolute;"><i class="fa fa-star-o" onclick="fun_calificacion(this)" style="font-size: 30px;cursor:pointer;color:#1aa3e6" title="Muy malo" punt="1" cod="'+ val.id_detalle_requerimiento__+'" ></i><br><span>Muy malo</span></div>'

                                              tpml+= ' <div style="position: absolute;left: 125px;"><i class="fa fa-star-o" onclick="fun_calificacion(this)" style="font-size: 30px;cursor:pointer;color:#1aa3e6" title="Pobre" punt="2" cod="'+ val.id_detalle_requerimiento__+'" ></i><br><span>Pobre</span></div>'
                                              
                                              tpml+= ' <div style="position: absolute;left: 175px;"><i class="fa fa-star-o" onclick="fun_calificacion(this)" style="font-size: 30px;cursor:pointer;color:#1aa3e6" title="Ni bien, ni mal" punt="3" cod="'+ val.id_detalle_requerimiento__+'" ></i><br><span>Ni bien, ni mal</span></div>'


                                              tpml+= ' <div style="position: absolute;left: 275px;"><i class="fa fa-star-o" onclick="fun_calificacion(this)" style="font-size: 30px;cursor:pointer;color:#1aa3e6" title="Bueno" punt="4" cod="'+ val.id_detalle_requerimiento__+'" ></i><br><span>Bueno</span></div>'

                                              tpml+= ' <div style="position: absolute;left: 330px"><i class="fa fa-star-o" onclick="fun_calificacion(this)" style="font-size: 30px;cursor:pointer;color:#1aa3e6" title="Muy bueno" punt="5" cod="'+ val.id_detalle_requerimiento__+'" ></i><br><span>Muy bueno</span></div>'
                                          tpml+= '</center>'
                                      tpml+= '</div>'
                                  tpml+= '</div>'
                             });

                             $("#calificacion_requerimiento_add").html(tpml);
                             
                        } else {

                            $('#req_q').show() 

                            $("#grid").kendoGrid({
                                dataSource: {
                                    type: "json",
                                    transport: {
                                        read: url + 'load-requerimientos?muestra_tikets='+$('#muestra_tikets').val()+'&est='+$('#estado_tikets option:selected').val()+'&id_tip_requerimiento='+$('#tip_tikets option:selected').val()+'&id_usuario='+$('#id_usuario').val()+'&opt=normal&req_prioridad=0'
                                    },
                                    pageSize: 150
                                },
                                height: $(window).height() - 270,
                                sortable: true,
                                dataBound: onDataBound,
                                filterable: true,
                                groupable: true,
                                pageable: {
                                  refresh: true,
                                  pageSizes: false,
                                  buttonCount: 100,
                                  messages:{
                                    display:"{0} - {1} de {2} registros",
                                    itemsPerPage: "registros por pagina",
                                    empty: "No hay registros"
                                  }
                                },columns: [
                                {
                                    field: "id_detalle_requerimiento",
                                    title: "Nº Ticket",
                                    width: 150,
                                    filterable: {
                                        extra: false, 
                                        operators: {
                                            string: {
                                                contains: "Contiene",
                                            }
                                        },
                                        cell: {
                                            operator: "contains",
                                            suggestionOperator: "contains"
                                        }
                                    }
                                }, {
                                    field: "titulo_requerimiento",
                                    title: "Título",
                                    width: 350,
                                    filterable: {
                                        extra: false,
                                        operators: {
                                            string: {
                                                contains: "Contiene",
                                            }
                                        },
                                        cell: {
                                            operator: "contains",
                                            suggestionOperator: "contains"
                                        }
                                    }
                                }, {
                                    field: "est_des",
                                    title: "Estado",
                                    width: 150,
                                    filterable: {
                                        extra: false,
                                        operators: {
                                            string: {
                                                contains: "Contiene",
                                            }
                                        },
                                        cell: {
                                            operator: "contains",
                                            suggestionOperator: "contains"
                                        }
                                    }
                                }, {
                                    field: "prioridad",
                                    title: "Prioridad",
                                    width: 150,
                                    filterable: {
                                        extra: false,
                                        operators: {
                                            string: {
                                                contains: "Contiene",
                                            }
                                        },
                                        cell: {
                                            operator: "contains",
                                            suggestionOperator: "contains"
                                        }
                                    }
                                },{
                                    field: "fec_apertura",
                                    title: "Fecha de apertura",
                                    width: 150,
                                    filterable: {
                                        extra: false,
                                        operators: {
                                            string: {
                                                contains: "Contiene",
                                            }
                                        },
                                        cell: {
                                            operator: "contains",
                                            suggestionOperator: "contains"
                                        }
                                    }
                                },{
                                    field: "usuario",
                                    title: "Usuario",
                                    width: 250,
                                    filterable: {
                                        extra: false,
                                        operators: {
                                            string: {
                                                contains: "Contiene",
                                            }
                                        },
                                        cell: {
                                            operator: "contains",
                                            suggestionOperator: "contains"
                                        }
                                    }
                                },{
                                    field: "area_usu",
                                    title: "Area",
                                    width: 250,
                                    filterable: {
                                        extra: false,
                                        operators: {
                                            string: {
                                                contains: "Contiene",
                                            }
                                        },
                                        cell: {
                                            operator: "contains",
                                            suggestionOperator: "contains"
                                        }
                                    }
                                },{
                                    field: "responsable",
                                    title: "Responsable",
                                    width: 350,
                                    filterable: {
                                        extra: false,
                                        operators: {
                                            string: {
                                                contains: "Contiene",
                                            }
                                        },
                                        cell: {
                                            operator: "contains",
                                            suggestionOperator: "contains"
                                        }
                                    }
                                },{
                                    field: "fec_cierre",
                                    title: "Fecha de cierre",
                                    width: 150
                                }]
                            });

                        }
                    
                    });

            }

            $('#save_hist_req').click(function(){
                
                if ( $('#req_hist_comentario').data("kendoEditor").value().length > 5  ){
                     
                     $.ajax({
                         url: url + 'insert-historial-requerimiento',
                         type: 'POST',
                         data: { id_detalle_requerimiento : $('#input_id_detalle_requerimiento').val(),comet_hist_req :  $('#req_hist_comentario').data("kendoEditor").value(),id_usuario : $('#id_usuario').val() },
                         success : function(resp){
                             load_historial($('#input_id_detalle_requerimiento').val())
                         }
                     })
                
                } else {
                    alert('Poner Comentario')
                }

            })


            function view_requerimiento(id_detalle_requerimiento){
                return '<i class="ti-receipt" onclick="view_detalle_req('+parseInt(id_detalle_requerimiento)+')"></i>'
            }


            //load_requerimientos('')
  
            carga_tipos_requerimiento($('#tip_requerimiento'),'tip_requerimiento', {ty:0})

            $('#tip_requerimiento').change(function(event) {
                event.preventDefault()
                if( $(this).val().length > 0 ) {
                    carga_tipos_requerimiento($('#sub_tip_requerimiento'),'sub_tip_requerimiento',{ id : $(this).val() })
                }
        
            })
              
            $('#guardar_requerimiento').click(function(event) {

                arr_validar.length = 0
                if ( desc_req.data("kendoEditor").value().length < 5 )
                     arr_validar.push("Llenar Descripción")
                if ( titulo_requerimiento.val().length == 0 ) 
                     arr_validar.push("LLenar Titulo")  
                if ( tip_requerimiento.val().length == 0 ) 
                     arr_validar.push("Seleccione Tipo de requerimiento")
                if ( sub_tip_requerimiento.val().length == 0 ) 
                     arr_validar.push("Seleccione sub tipo de requerimiento")
                if(  cbo_prioridad.val().length == 0 )
                     arr_validar.push("Seleccione Prioridad")
                if ( $('#tip_requerimiento').val() == '1' ) {
                     if(  obj_requerimiento.data("kendoEditor").value().length < 5 )
                          arr_validar.push("Llenar Objetivo")
                     if( fec_est_termino.val().length ==  0 )
                         arr_validar.push("Seleccione  una fecha estimada de término")
                }

                if ( arr_validar.length == 0 ) {

                      if ( $('#image')[0].files[0] != undefined )
                           ruta_imagen = ( $('#image')[0].files[0].name.length > 0 ) ? $('#image')[0].files[0].name : ''
                      else
                           ruta_imagen = " " 

                      param = { 
                                  titulo_requerimiento : titulo_requerimiento.val(),
                                  desc_requerimiento : desc_req.data("kendoEditor").value(),
                                  path_adjunto : ruta_imagen,
                                  id_sub_tip_requerimiento : sub_tip_requerimiento.val(),
                                  id_usuario : $('#id_usuario').val(),
                                  id_usuario_reg : $('#id_usuario').val(),
                                  req_prioridad : cbo_prioridad.val(),
                                  obj_requerimiento :  obj_requerimiento.data("kendoEditor").value(),
                                  fec_est_termino : fec_est_termino.val()
                              }



                     $.ajax({
                         url: url + 'insert-requerimiento',
                         type: 'POST',
                         data: param,
                         beforeSend: function(){
                             

                         },success : function(resp){

                            if ( ruta_imagen.length > 0  ){
                                var formData = new FormData();
                                var files = $('#image')[0].files[0];

                                formData.append('file',files);
                                $.ajax({
                                    url: url + 'subir-adjunto?',
                                    type: 'post',
                                    data: formData,
                                    contentType: false,
                                    processData: false,
                                    success: function(response) {
                                        limpiar_observaciones();
                                        $('li[class="active"]').click(); 
                                    }
                                });
                            }
                         }
                     })
                } else {
                   msg = ''
                   for (var i = 1; i <= arr_validar.length; i++)
                        msg+= "<span style='color:red;font-size:14px'>"+i+"- "+arr_validar[i-1]+"</span></br>"
                   
                   kendo.alert(msg);
                   $('.k-dialog-titlebar').remove();
                }
            })

        
            $('#descargar_evi__').click(function(event) {
                 window.open(url + "assets/evidencia/"+$(this).attr('arch')); 
            });

            function limpiar_observaciones() {
                titulo_requerimiento.val("")
                tip_requerimiento.val("")
                sub_tip_requerimiento.val("")
                $('#image').val("")
                desc_req.val("")
            }

            function view_detalle_req(id_detalle_requerimiento) {
                $.ajax({
                     url: url + 'datalle-requerimiento',
                     type: 'POST',
                     dataType: 'json',
                     data: { id_detalle_requerimiento: id_detalle_requerimiento },
                     success : function(resp) {
                          load_historial(id_detalle_requerimiento)
                          $('#mdl_detalle_requerimiento_tittle').html( resp.tipo_requerimiento + ' - ' + resp.titulo_requerimiento )
                          $('#_descripcion_req').html(resp.desc_requerimiento)
                          $('#_objetivo_solicitud').html(resp.obj_requerimiento)
                          $('#fec_est_ter').html(resp.fec_est_termino)
                          $('#fec_est_sis_ter').html(resp.fec_est_sistmas)
                          $('#codigo_req').html(resp.id_detalle_requerimiento)
                          $('#input_id_detalle_requerimiento').val(id_detalle_requerimiento)
                          $('#capertura_req').html(resp.fec_apertura)
                          $('#estado_req').html(resp.est_des)
                          $('#prioridad_req').html(resp.prioridad)
                          $('#__prioridad_req').val(resp.prioridad)
                          $('#__estado_req').val(resp.est_des)
                          $('.btn_tip_requerimiento').html(resp.tipo_requerimiento)
                          $('#descargar_evi__').html(resp.path_adjunto)
                          $('#descargar_evi__').attr('arch',resp.path_adjunto)

                          if ( resp.tipo_requerimiento == 'Solicitud' ) {
                               $('#fec_est_ter__').css('display','block')
                               $('#objetivo_solicitud').css('display', 'block')
                               $('#fec_est_ter_sis__').css('display', 'block')
                               $('#_descripcion_req').css({
                                   'width':'100%',
                                   'overflow-y':'scroll',
                                   'height':'430px'
                               });
                               $('#_objetivo_solicitud').css({
                                   'width':'100%',
                                   'overflow-y':'scroll',
                                   'height':'200px'
                               });
                          } else {
                               $('#fec_est_ter__').css('display','none')
                               $('#objetivo_solicitud').css('display', 'none');
                               $('#fec_est_ter_sis__').css('display', 'none')
                               $('#_descripcion_req').css({
                                   'width':'100%',
                                   'overflow-y':'scroll',
                                   'height':'620px'
                               });
                          }

                          if( resp.id_usuario == $('#id_usuario').val() ) {
                              if ( resp.est == 1 || resp.est == 2 ) {
                                  $('#btn_abrir_requerimiento').css('display','none')
                                  $('#com_solicitud').css('display','block');
                                  $('#btn_asignar_rq').css('display','block')
                              } else if (resp.est == 0 ) {
                                  $('#btn_asignar_rq').css('display','none')
                                  $('#btn_abrir_requerimiento').css('display','block')
                                  $('#com_solicitud').css('display','none');
                              }
                          } else {
                              if ( resp.est == 1 || resp.est == 2 ) {
                                  $('#btn_abrir_requerimiento').css('display','none')
                                  $('#com_solicitud').css('display','none');
                                  $('#btn_asignar_rq').css('display','none')
                              } else if (resp.est == 0 ) {
                                  $('#btn_asignar_rq').css('display','none')
                                  $('#btn_abrir_requerimiento').css('display','none')
                                  $('#com_solicitud').css('display','none');
                              }
                          }
                          
                          $('#mdl_detalle_requerimiento').modal('show')
                     }
                })
            }

            $('#btn_abrir_requerimiento').click(function(){
                $.ajax({
                    url: url + 'abrir-requerimiento',
                    type: 'POST',
                    data: { id_detalle_requerimiento : parseInt($('#codigo_req').text()), id_usuario : $('#id_usuario').val() },
                    success : function(){
                        $('#btn_abrir_requerimiento').css('display','none')
                        $('#com_solicitud').css('display','block')
                        load_requerimientos()
                    }
                })
            })

            function onDataBound(arg){
                var grid = $("#grid").data("kendoGrid");
                var data = grid.dataSource.data();
                for (var i= 0; i < data.length ; i++) {
                      $('tr[data-uid="' + data[i].uid + '"] ').attr('onclick','view_detalle_req("'+parseInt(data[i].id_detalle_requerimiento)+'")') 
                      $('tr[data-uid="' + data[i].uid + '"] ').css('cursor','pointer') 
                      if (data[i].req_prioridad == '1')
                          $('tr[data-uid="' + data[i].uid + '"] ').addClass('red');
                      else if ( data[i].req_prioridad == '2')
                          $('tr[data-uid="' + data[i].uid + '"] ').addClass('orange');
                      else if ( data[i].req_prioridad == '3' )
                          $('tr[data-uid="' + data[i].uid + '"] ').addClass('green');
                }
            }

    </script>