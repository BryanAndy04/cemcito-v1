<div class="row">
        <div class="col-lg-3 col-ml-3">
            <div class="row">
                <div class="col-12 top_div">
                    <div class="card">
                        <div class="card-body">

                            <div class="form-group">
                                <label for="des_usuario" class="col-form-label">Equipo :<span style="color: red"> (*) </span></label>
                                <select class="custom-select" id='cbo_equipo'>
                                    <option value='1'>Proyector</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="des_usuario" class="col-form-label">Horas max:<span style="color: red"> (*) </span></label>
                                <input class="form-control" id="conf_tiempo_hora_max">
                            </div>

                            <div class="form-group">
                                <label for="email_usuario" class="col-form-label">Días max:<span style="color: red"> (*) </span></label>
                                <input class="form-control" type="text" id="conf_tiempo_dia_max">
                            </div>

                            <div class="form-group">
                                <label for="email_usuario" class="col-form-label">Minuto estimado de recepción:<span style="color: red"> (*) </span></label>
                                <input class="form-control" type="text" id="conf_tiempo_min_recep">
                            </div>

                            <div class="form-group">
                                <label for="email_usuario" class="col-form-label">Minuto estimado de devolución:<span style="color: red"> (*) </span></label>
                                <input class="form-control" type="text" id="conf_tiempo_min_devol">
                            </div>

                            <div class="form-group">
                                <label for="email_usuario" class="col-form-label">Número de Bloqueo :<span style="color: red"> (*) </span></label>
                                <input class="form-control" type="text" id="conf_nun_bloqueo">
                            </div>

                            <center><button type="button" class="btn btn-outline-primary mb-3" id="guardar_configuracion" tip="insert">Guardar</button></center>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-9 col-ml-9">
            <div class="row">
                 <div class="col-12 top_div">
                    <div class="card">
                        <div class="card-body">
                            <div>
                                <h5 style='color: #e57373'><b>BLOQUEO DE USO DEL PROYECTOR</b></h5>
                            </div>
                            <hr>
                            <div id='bloqueo_equipo'></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <script type="text/javascript">
         read_data_bloqueo()
         var id_equipo               = $('#cbo_equipo'),
             conf_tiempo_hora_max    = $('#conf_tiempo_hora_max'),
             conf_tiempo_dia_max     = $('#conf_tiempo_dia_max'),
             conf_tiempo_min_recep   = $('#conf_tiempo_min_recep'),
             conf_tiempo_min_devol   = $('#conf_tiempo_min_devol'),
             conf_nun_bloqueo        = $('#conf_nun_bloqueo'),
             param                   = {}
          
         read_data()
       
         $('#guardar_configuracion').click(function(){

                param = { 
                            id_equipo                 : id_equipo.val(),
                            conf_tiempo_hora_max      : conf_tiempo_hora_max.val(),
                            conf_tiempo_dia_max       : conf_tiempo_dia_max.val(),
                            conf_tiempo_min_recep     : conf_tiempo_min_recep.val(),
                            conf_tiempo_min_devol     : conf_tiempo_min_devol.val(),
                            conf_nun_bloqueo          : conf_nun_bloqueo.val()
                        }

                $.ajax({
                    url     : url + 'conf-reserva',
                    type    : 'POST',
                    data    : param,
                    success : function(obj) {
                        alert('se modifico correctamente')
                    }
                })
            
         });

         function read_data() {
            $.get( url + 'read-data-conf-equipo', { id_equipo  :  id_equipo.val() } ,function(json) {
                conf_tiempo_hora_max.val(json.conf_tiempo_hora_max)
                conf_tiempo_dia_max.val(json.conf_tiempo_dia_max)
                conf_tiempo_min_recep.val(json.conf_tiempo_min_recep)
                conf_tiempo_min_devol.val(json.conf_tiempo_min_devol)
                conf_nun_bloqueo.val(json.conf_nun_bloqueo)
            });
         }

         function read_data_bloqueo() {
            $("#bloqueo_equipo").kendoGrid({
                dataSource: {
                    type: "json",
                    transport: {
                        read: url + 'read-count-n-dev'
                    },
                    pageSize: 100
                },
                height: $(window).height() - 200,
                sortable: true,
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 100
                },filterable: {
                        mode: "row"
                },columns: [{
                    title: "Acc",
                    template: "#=acciones(cong,id_usuario)#",
                    width: 120
                },{
                    field: "des_usuario",
                    title: "Descripción",
                    width: 200,
                    filterable: {
                        extra: false,
                        operators: {
                            string: {
                                contains: "Contiene",
                            }
                        },cell: {
                            operator: "contains",
                            suggestionOperator: "contains"
                        }
                    }
                },{
                    field: "des_area",
                    title: "Área",
                    width: 200,
                    filterable: {
                        extra: false,
                        operators: {
                            string: {
                                contains: "Contiene",
                            }
                        },cell: {
                            operator: "contains",
                            suggestionOperator: "contains"
                        }
                    }
                },{
                    field: "total",
                    title: "Total",
                    width: 100,
                    filterable: false
                }]
            })
         }

         function acciones(cong,id_usuario) {
            var html = ""
            if ( cong == 1 ) 
                 html+= '<center><button type="button" class="btn btn-outline-primary" style="padding: 1px 8px;" onclick="desbloquear('+id_usuario+')">Desbloquear</button></center>'
            return html
         }
         
         function desbloquear(id_usuario) {
             $.post(url + 'Requerimiento_C/desbloqueo', { id_usuario : id_usuario }, function(data, textStatus, xhr) {
                 $('#bloqueo_equipo').data('kendoGrid').dataSource.read()
             });
         }

    </script>