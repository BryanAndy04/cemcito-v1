<div class="row">
        <div class="col-lg-4 col-ml-4">
            <div class="row">
                <div class="col-12 top_div">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="tip_requerimiento" class="col-form-label">Tipo de Requerimiento :<span style="color: red"> (*) </span></label>
                                 <select class="custom-select" id='tip_requerimiento'>
                                    <option value='1'>Solicitud</option>
                                    <option value='2'>Incidencia</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="des_usuario" class="col-form-label">Sub tipo :<span style="color: red"> (*) </span></label>
                                <input class="form-control" type="text" id="sub_tipo">
                            </div>
                            <input type="hidden" id='id_area'>
                            <center><button type="button" class="btn btn-outline-primary mb-3" id="guardar_subtipo" tip="insert">Guardar</button></center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-8 col-ml-8">
            <div class="row">
                 <div class="col-12 top_div">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="header-title">Sub tipos</h4>
                            <div class="single-table" id='grid'>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <script type="text/javascript">
         var arr_valida = new Array(),
             des_sub_tipo = $('#sub_tipo'),
             ruta_t


         $(function(){

             load_sub_tipo()


             $('#guardar_subtipo').click(function(event) {
                 event.preventDefault()
                 arr_valida.length = 0
                 if ( des_sub_tipo.val().length == 0 )
                      arr_valida.push("LLenar Descripción")

                 if( $(this).attr('tip') == 'insert' ) 
                     ruta_t = 'insert-sub-tipo'

                 if ( arr_valida.length == 0 ){
                      
                      $.ajax({
                         url:  url + ruta_t,
                         type: 'POST',
                         data: {  des_sub_tip_requerimiento : des_sub_tipo.val(), id_tip_requerimiento : $('#tip_requerimiento').val() },
                         success : function(){
                             load_sub_tipo()
                             limpia_form()
                         }
                     })

                 } else  {
                    msg = '' 
                    for (var i = 1; i <= arr_valida.length; i++)
                            msg+= "<span style='color:red;font-size:14px'>"+i+"- "+arr_valida[i-1]+"</span></br>"
                       
                    kendo.alert(msg);
                    $('.k-dialog-titlebar').remove();
                 }

                 

             });

         })


         function delete_area(id){
             $.post( url + 'delete-sub-tipo', { id_sub_tip_requerimiento : id }, function(data, textStatus, xhr) {
                load_sub_tipo()
             });
         }


         function load_sub_tipo(){

                $("#grid").kendoGrid({
                    dataSource: {
                        type: "json",
                        transport: {
                            read: url + 'load-sub-requerimientos',
                            data : { hola : 'hola' }
                        },
                        pageSize: 100
                    },
                    height: $(window).height() - 200,
                    sortable: true,
                    pageable: {
                        refresh: true,
                        pageSizes: true,
                        buttonCount: 100
                    },filterable: {
                            mode: "row"
                    },columns: [{
                        title: "Acc",
                        template: "#=acciones(id_sub_tip_requerimiento)#",
                        width: 100
                    },{
                        field: "des_tip_requerimiento",
                        title: "Tipo",
                        width: 300,
                        filterable: {
                            cell: {
                                operator: "contains",
                                suggestionOperator: "contains"
                            }
                        }
                    },{
                        field: "des_sub_tip_requerimiento",
                        title: "Sub tipo",
                        width: 300,
                        filterable: {
                            cell: {
                                operator: "contains",
                                suggestionOperator: "contains"
                            }
                        }
                    }]
                });
        }

        
        function acciones(id) {
            var html = ''
            html+= '<ul class="d-flex justify-content-center"><li><a class="text-danger" onclick="delete_area('+id+')"><i class="ti-trash"></i></a></li></ul>'
            return html
        }

        function update_area(id) {
             $.post( url + 'detalle-area', { id_area: id }, function(resp, textStatus, xhr) {
                  $('#guardar_area').attr('tip','update');
                  $('#id_area').val(resp.id)
                  $('#des_area').val(resp.des)
             });
        }

        function limpia_form() {
                $('#guardar_area').attr('tip','insert')
                $('#sub_tipo').val('')
        }



    </script>