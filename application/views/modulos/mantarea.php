<div class="row">
        <div class="col-lg-4 col-ml-4">
            <div class="row">
                <div class="col-12 top_div">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="des_usuario" class="col-form-label">Descripción :<span style="color: red"> (*) </span></label>
                                <input class="form-control" type="text" id="des_area">
                            </div>
                            <input type="hidden" id='id_area'>
                            <center><button type="button" class="btn btn-outline-primary mb-3" id="guardar_area" tip="insert">Guardar</button></center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-8 col-ml-8">
            <div class="row">
                 <div class="col-12 top_div">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="header-title">Áreas</h4>
                            <div class="single-table" id='grid'>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <script type="text/javascript">
         var arr_valida = new Array(),
             des_area = $('#des_area'),
             ruta_t,msg


         $(function(){

             load_area__()


             $('#guardar_area').click(function(event) {
                 event.preventDefault()
                 arr_valida.length = 0
                 if ( des_area.val().length == 0 )
                      arr_valida.push("LLenar Descripción")

                 if( $(this).attr('tip') == 'insert' ) 
                     ruta_t = 'insert-area'
                 else
                     ruta_t = 'update-area?id_area='+$('#id_area').val()




                 if ( arr_valida.length == 0 ){
                      
                      $.ajax({
                         url:  url + ruta_t,
                         type: 'POST',
                         data: { des_area : des_area.val() },
                         success : function(){
                             load_area()
                             limpia_form()
                         }
                     })


                 } else  {

                           msg = '' 
                           for (var i = 1; i <= arr_valida.length; i++)
                                msg+= "<span style='color:red;font-size:14px'>"+i+"- "+arr_valida[i-1]+"</span></br>"
                           
                           kendo.alert(msg);
                           $('.k-dialog-titlebar').remove();
                 }

                 

             });




              
            

         })


         function delete_area(id){
             $.post( url + 'delete-area', { id_area : id }, function(data, textStatus, xhr) {
                load_area__()
             });
         }


         function load_area__(){

                $("#grid").kendoGrid({
                    dataSource: {
                        type: "json",
                        transport: {
                            read: url + 'load-area'
                        },
                        pageSize: 100
                    },
                    height: $(window).height() - 200,
                    sortable: true,
                    pageable: {
                        refresh: true,
                        pageSizes: true,
                        buttonCount: 100
                    },filterable: {
                            mode: "row"
                    },columns: [{
                        title: "Acc",
                        template: "#=acciones(id)#",
                        width: 100
                    },{
                        field: "des",
                        title: "Descripción",
                        width: 600,
                        filterable: {
                            cell: {
                                operator: "contains",
                                suggestionOperator: "contains"
                            }
                        }
                    }]
                });
        }

        
        function acciones(id) {
            var html = ''
            html+= '<ul class="d-flex justify-content-center"><li class="mr-3"><a class="text-secondary" onclick="update_area('+id+')" ><i class="fa fa-edit"></i></a></li><li><a class="text-danger" onclick="delete_area('+id+')"><i class="ti-trash"></i></a></li></ul>'
            return html
        }

        function update_area(id) {
             
             $.post( url + 'detalle-area', { id_area: id }, function(resp, textStatus, xhr) {
                  $('#guardar_area').attr('tip','update');
                  $('#id_area').val(resp.id)
                  $('#des_area').val(resp.des)
load_area__()
             });
        }


        function limpia_form() {
                $('#guardar_area').attr('tip','insert')
                $('#id_area').val('')
                des_area.val('')
        }

    </script>