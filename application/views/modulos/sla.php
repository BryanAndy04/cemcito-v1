<div class="row">
	<div class="col-lg-12 col-ml-12 top_div">
    	<div class="card" id='__sla'>
    		<div class="card-body">
	            <div class="row">
                    <div class="col-12">
                    	<label>Seleccione : </label>
                        <input type="hidden" id='cond_insert_up'>
                        <input type="hidden" id='id_det_sla'>
                    	<select class="custom-select" id='cbo_genen' style="width: 200px !important">  
                        	<option value='1'>General</option>
                            <option value='2'>Por Subtipos</option>
                        </select>
 
                        <div id='div_subtipos' style="position: absolute;top: 0px;left: 295px;display: none" >  
		            	    <label>Tipo de requerimiento : </label> 
		            	    <select class="custom-select" id='tip_tikets' style="width: 150px !important">
		            	    	<option value=''>Seleccione ...</option>
	                            <option value='1'>Solicitud</option>
	                            <option value='2'>Insidencia</option>
	                        </select>
	                        <label>Sub Tipo de requerimiento : </label>
	                        <select class="custom-select" id='Sub_tip_tikets' style="width: 200px !important">  
	                        	<option value=''>Seleccione ...</option>
	                        </select>   
                        </div>

                        <button type="button" class="btn btn-secondary" id='buscar_reslt' onclick="buscar_reslt()"> Buscar </button>
                        <hr>

	            	</div> 

	            	<div id='div_content' style="display: none">

		            	<div class="col-12" style="padding-top: 20px">
		            	    <p style="text-align: justify;font-size: 15px"><b>Tiempo de Atención(TA) :</b> Se entiende como tiempo de atención al tiempo tomado desde que la solicitud del servicio es recibida por el ÁREA DE SISTEMAS, hasta que OPERADOR responsable se pone en contacto y a disposición del usuario líder.</p>
		            	</div>

		            	<div class="col-12" style="padding-top: 20px">
		            		<div class="single-table">
	                            <div class="table-responsive">
	                                <table class="table table-hover progress-table text-center">
	                                	<thead>
				            		 	 	<tr>
			                                    <th style="background: #003282;color: #fff;font-size: 15px !important" rowspan="2">Plazos</th>
			                                    <th style="background: #003282;color: #fff;font-size: 15px !important" colspan="3">Severidad de Incidentes </th>
			                                    <th style="background: #003282;color: #fff;font-size: 15px !important" rowspan="2">Cumplimiento</th>  
				            		 	 	</tr>
				            		 	 	<tr>
			                                    <th style="background: #1aa3e6;color: #fff;" > Alta </th>
			                                    <th style="background: #1aa3e6;color: #fff;" > Media </th>
			                                    <th style="background: #1aa3e6;color: #fff;" > Baja </th>  
				            		 	 	</tr>
				            		 	 	<tr>
				            		 	 		<th style="background: #003282;color: #fff;font-size: 15px !important" > Contacto </th>
			                                    <th style="background: #f2f7fa;color: #000;" > <input class="form-control" type="text" name="txt_alta_TA"> </th>
			                                    <th style="background: #f2f7fa;color: #000;" > <input class="form-control" type="text" name="txt_media_TA"> </th>
			                                    <th style="background: #f2f7fa;color: #000;" > <input class="form-control" type="text" name="txt_baja_TA"> </th>  
			                                    <th style="background: #f2f7fa;color: #000;" > Minimo un 90% </th>  
				            		 	 	</tr>
				            		 	</thead>
	                                </table>
	                            </div>
	                        </div>
		            	</div>

		            	<div class="col-12" style="padding-top: 50px">
		            	    <p style="text-align: justify;font-size: 15px"><b>Tiempo de solución(TS) :</b> Se entiende como tiempo de solución al tiempo contabilizado desde que se toma una acción sobre el incidente reportado hasta que se soluciona el incidente. Este tiempo es adicional al tiempo de atención.</p>
		            	</div>

		            	<div class="col-12" style="padding-top: 20px">
		            		<div class="single-table">
	                            <div class="table-responsive">
	                                <table class="table table-hover progress-table text-center">
	                                	<thead>
				            		 	 	<tr>
			                                    <th style="background: #003282;color: #fff;font-size: 15px !important" rowspan="2">Plazos</th>
			                                    <th style="background: #003282;color: #fff;font-size: 15px !important" colspan="3">Severidad de Incidentes </th>
			                                    <th style="background: #003282;color: #fff;font-size: 15px !important" rowspan="2">Cumplimiento</th>  
				            		 	 	</tr>
				            		 	 	<tr>
			                                    <th style="background: #1aa3e6;color: #fff;" > Alta </th>
			                                    <th style="background: #1aa3e6;color: #fff;" > Media </th>
			                                    <th style="background: #1aa3e6;color: #fff;" > Baja </th>  
				            		 	 	</tr>
				            		 	 	<tr>
				            		 	 		<th style="background: #003282;color: #fff;font-size: 15px !important" > Respuesta </th>
			                                    <th style="background: #f2f7fa;color: #000;" > <input class="form-control" type="text" name="txt_alta_TS"> </th>
			                                    <th style="background: #f2f7fa;color: #000;" > <input class="form-control" type="text" name="txt_media_TS"> </th>
			                                    <th style="background: #f2f7fa;color: #000;" > <input class="form-control" type="text" name="txt_baja_TS"> </th>  
			                                    <th style="background: #f2f7fa;color: #000;" > Minimo un 90% </th>  
				            		 	 	</tr>
				            		 	</thead>
	                                </table>
	                            </div>
	                        </div>
		            	</div>

		            	<div class="col-12" style="padding-top: 20px">
		            		<center><button type="button" class="btn btn-secondary" onclick="guardar_r()">Guardar</button></center>
		            	</div>
	        </div>
	    </div>
	</div>    
</div>

<script type="text/javascript">
    
    /**************************************/
    /*************************************/
      
      var txt_alta_TA = $('input[name="txt_alta_TA"]'),
          txt_media_TA = $('input[name="txt_media_TA"]'),
          txt_baja_TA = $('input[name="txt_baja_TA"]'),
          txt_alta_TS = $('input[name="txt_alta_TS"]'),
          txt_media_TS = $('input[name="txt_media_TS"]'),
          txt_baja_TS = $('input[name="txt_baja_TS"]'),
          arr_valida = [],
          msg = ''

    /**************************************/
    /*************************************/


	$('#__sla').height($(window).height() - 100)
    $('#cbo_genen').change(function(){
    	if ( $(this).val() == '2' ) {
    		$('#div_subtipos').show();
    		$('#buscar_reslt').css('margin-left','660px')
    	} else {
            $('#div_subtipos').hide();
            $('#buscar_reslt').css('margin-left','0px')
    	} 
        	
    });

    $('#tip_tikets').change(function(event){
        event.preventDefault()
        if( $(this).val().length > 0 ) {
            carga_tipos_requerimiento( $('#Sub_tip_tikets'),'sub_tip_requerimiento',{ id : $(this).val() } )
        }
    })

    function buscar_reslt(){
        
        $('#div_content').css('display','block')
        var param = {},
            option = $('#cbo_genen option:selected').val()

        if ( option == '1' ) {
             param = {
                tip_sla_des : '1'
             }
        } else { 
            param = {
                tip_sla_des : '2',
                id_tip_requerimiento : $('#tip_tikets').val(),
                id_sub_tip_requerimiento : $('#Sub_tip_tikets').val()
            }
        }

        $.ajax({

        	url     : url + 'read-sla',
        	type    : 'POST',
        	dataType: 'json',
        	data    : param,
        	success : function(json) {

         		if ( json != 0 ) {
                    $('#cond_insert_up').val('update')
                    $('#id_det_sla').val(json.id_det_sla)
                    txt_alta_TA.val(json.TA_sla_alta_min)
                    txt_media_TA.val(json.TA_sla_media_min)
                    txt_baja_TA.val(json.TA_sla_baja_min)
                    txt_alta_TS.val(json.TS_sla_alta_min)
                    txt_media_TS.val(json.TS_sla_media_min)
                    txt_baja_TS.val(json.TS_sla_baja_min)

        		} else {
        			$('#cond_insert_up').val('insertar')
        			$('#id_det_sla').val('0')
        			txt_alta_TA.val('')
              txt_media_TA.val('')
              txt_baja_TA.val('')
              txt_alta_TS.val('')
              txt_media_TS.val('')
              txt_baja_TS.val('')
        		}
        	}

        })
    }


    function guardar_r() {
     	 arr_valida.length = 0
     	 if (txt_alta_TA.val().length == 0 )
     	 	arr_valida.push('Llenar TA - Alta' )

     	 if (txt_media_TA.val().length == 0 )
     	 	arr_valida.push('Llenar TA - Media' )

     	 if (txt_baja_TA.val().length == 0 )	
            arr_valida.push('Llenar TA - Baja' )

         if (txt_alta_TS.val().length == 0 )
         	arr_valida.push('Llenar TS - Alta' )

         if (txt_media_TS.val().length == 0 )
         	arr_valida.push('Llenar TS - Media' )

         if (txt_baja_TS.val().length == 0 )
         	arr_valida.push('Llenar TS - Baja' )

         if ( arr_valida.length	== 0 ) {
            $.ajax({
	           	url: url + 'ins-upd-sla?cond_insert_up='+$('#cond_insert_up').val()+'&id_det_sla='+$('#id_det_sla').val(),
	           	type: 'POST',
	           	data: {  
	           		     tip_sla_des : $('#cbo_genen option:selected').val(),
	           		     id_tip_requerimiento : $('#tip_tikets option:selected').val(),
	           		     id_sub_tip_requerimiento : $('#Sub_tip_tikets option:selected').val(),
	           		     TA_sla_alta_min : txt_alta_TA.val(),
	           		     TA_sla_media_min : txt_media_TA.val(),
	           		     TA_sla_baja_min : txt_baja_TA.val(),
	           		     TS_sla_alta_min : txt_alta_TS.val(),
	           		     TS_sla_media_min : txt_media_TS.val(),
	           		     TS_sla_baja_min : txt_baja_TS.val()
	           	      },
	           	success : function(){
	           		 $('li[class="active"]').click()
	           	}      
            })
         } else {
         	msg = ""
         	for (var i = 1; i <= arr_valida.length; i++)
                msg+= "<span style='color:red;font-size:14px'>"+i+"- "+arr_valida[i-1]+"</span></br>" 
            kendo.alert(msg);
            $('.k-dialog-titlebar').remove();
         }     	
    } 

</script>