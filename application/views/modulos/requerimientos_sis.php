    <div class="row">
        <div class="col-lg-12 col-ml-12">
            <div class="row">
                <!-- basic form start -->
                <input type="hidden" id='filter_fecha'>
                <div class="col-12 top_div">
                    <div class="card">
                        <div class="card-body">
                            <label> Estado : </label>
                            <select class="custom-select" id='estado_tikets' style="width: 150px !important;height: 32px !important" onchange="change_estado()">
                                    <option value='"3","2","1"'>Pendiente</option>
                                    <option value='0'>Terminado</option>
                                    <option value="todos">Todos</option>
                            </select> 
                            <label> Fechas : </label>
                            <select class="custom-select" id='filter_fec' style="width: 80px !important;height: 32px !important" onchange="active_fechas(this)">
                                    <option value='0'>No</option>
                                    <option value='1'>Si</option>
                            </select>

                            <div id='fec_des_' style="position: absolute;top: 26px;left: 365px;display: none">
                                <label id='fec_des'>Desde : </label> 
                                <input id="start" style="width: 150px !important" value="<?php echo date('Y-m-d') ?>" />
                                <label id='fec_has'>Hasta : </label>
                                <input id="end" style="width: 150px !important" value="<?php echo date('Y-m-d') ?>"/>
                            </div>


                            <div class="single-table" id='grid' style="margin-top: 20px">
                                
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Custom file input end -->
            </div>
        </div>
    </div>
    
    
    <!-- modal detalle del requerimiento -->

    <div class="modal fade bd-example-modal-lg show" id='mdl_detalle_requerimiento' style="display: none;">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="mdl_detalle_requerimiento_tittle">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                </div>
                <div class="modal-body" id='modal_req' style="overflow-y: scroll !important; ">
                   
                    <div class="row">
                        <div class="col-lg-7 col-ml-7">
                            <div class="row">
                                <div class="col-12">
                                    <p><b>Número de Requerimiento  : </b> <span id='codigo_req'></span>.</p>
                                    <p><b>Usuario : </b><span id='usu_apertura'></span>.</p>
                                    <p><b>Área : </b><span id='v_area_usu'></span>.</p>
                                    <p><b>Fecha / hora de apertura  : </b> <span id='capertura_req'></span>.</p>
                                    <p id='fec_est_ter__' style="display: none"><b>Fecha estimada de término  : </b> <span id='fec_est_ter'></span>.</p>
                                    <p id='fec_est_ter_sis__' style="display: none"><b>Fecha estimada por sistemas : </b> <span id='fec_est_sis_ter'></span>.</p>
                                    <p><b>Prioridad : </b> <span id='prioridad_req'></span>.</p>
                                    <p><b>Estado : </b> <span id='estado_req'></span>.</p>
                                    <p><b>Adjunto : </b><span id='descargar_evi__' arch="" style="cursor: pointer !important;"></span>.</p>
                                    <hr>
                                </div>
                                <div class="col-12" id='descripcion_req'>
                                   <p style="text-align: justify;"><b>Descripción : </b></p>
                                   <div id="_descripcion_req"> </div>
                                </div>
                                <div class="col-12" id="objetivo_solicitud">
                                   <p style="text-align: justify;"><b>Objetivo de la Solitud : </b></p>
                                   <div id="_objetivo_solicitud" > </div>
                                </div>
                            </div>  
                        </div>
                        <div class="col-lg-5 col-ml-5">
                           <div  class="col-12">
                              <div class="card" style="overflow-y: scroll !important;height: 430px !important">
                                  <div class="card-body">
                                      <h4 class="header-title">Historial</h4>
                                      <div class="timeline-area" id='cest_historial'></div>
                                  </div>
                              </div>
                            </div>
                            <div class="col-12" id='com_solicitud'>
                                <input type="hidden" id='input_id_detalle_requerimiento'>
                                <p> 
                                    <div class="form-group">
                                        <label for="__prioridad_req" class="col-form-label">Prioridad :<span style="color: red"> (*) </span></label>
                                        <select class="custom-select" id="__prioridad_req">
                                            <option value='3'>Baja</option>
                                            <option value='2'>Media</option>
                                            <option value='1'>Alta</option> 
                                        </select>
                                    </div>
                                </p>
                                <p> 
                                    <div class="form-group">
                                        <label for="__estado_req" class="col-form-label">Estado :<span style="color: red"> (*) </span></label>
                                        <select class="custom-select" id='__estado_req'>
                                            <option value='0'>Terminado</option>
                                            <option value='2'>Pendiente</option>
                                            <option value='1'>En proceso</option>
                                            <option value='3'>Pausa</option>
                                        </select>
                                    </div>
                                </p>

                                <p > 
                                    <div class="form-group" id='tihet_select' style="display: none">
                                        <label for="__tikets_req" class="col-form-label">Ticket  </span></label>
                                        <select class="custom-select" id='__tikets_req'>
                                        </select>
                                    </div>
                                </p>

                                <p> 
                                    <div class="form-group" id='fec_est_sistemas' style="display: none;">
                                        <label for="fec_est_sistemas_" class="col-form-label">Fecha de entrega estimada  </span></label>
                                        <input type="text" id='fec_est_sistemas_' class="form-control" value="<?php echo date('Y-m-d') ?>">
                                    </div>
                                </p>

                                <p> Comentario : </p>
                                <textarea class="form-control" id="req_hist_comentario" style="width: 100% !important"> </textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" id='close'>Close</button>
                    <button type="button" class="btn btn-primary" id='btn_asignar_rq'>Guardar <span class='btn_tip_requerimiento'></span></button>
                </div>
            </div>
        </div>
    </div>

    <!-- -->

    <script type="text/javascript">


            $("#start").kendoDatePicker({
                format: "yyyy-MM-dd"
            }).data("kendoDatePicker");

            $("#end").kendoDatePicker({
                change: change_estado,
                format: "yyyy-MM-dd" 
            }).data("kendoDatePicker");

            $('#modal_req').height($(window).height() - 170)

            carga_tipos_requerimiento($('#__tikets_req'),'load-tikets-com',{ id_usuario : $('#id_usuario').val() })

            $("#fec_est_sistemas_").kendoDatePicker({ format: "yyyy-MM-dd" });
            $("#fec_est_sistemas_").click(function(){
               $(this).data("kendoDatePicker").open()
            });

            $('#filter_fec').change(function(){
               if( $(this).val() == '0' )
                   $('#fec_des_').hide();
               else 
                   $('#fec_des_').show();
            });

            $('#__estado_req').change(function(event) {
                if( $(this).val() == 3 )
                    $('#tihet_select').css('display', 'block');
                else
                    $('#tihet_select').css('display', 'none');
            });

            var cbo_trabajadores = ""
            $.ajax({
                url:  url + 'list-sist',
                type: 'POST',
                dataType: 'json',
                success : function(resp) {
                  cbo_trabajadores+= "<option value=''>SELECCIONE</option>"

                  $.each(resp, function(index, val) {
                       cbo_trabajadores+= "<option value='"+val.id+"'>"+val.des+"</option>"
                  })


                }
            })

            var __prioridad_req = $('#__prioridad_req'),
                __estado_req = $('#__estado_req'),
                req_hist_comentario = $('#req_hist_comentario'),
                arr_valida = new Array(),
                param

        
            $('#descargar_evi__').click(function(event) {
                 window.open(url + "assets/evidencia/"+$(this).attr('arch')); 
            }); 

            load_requerimientos()

           $('#req_hist_comentario').kendoEditor({
                tools: [
                    "bold",
                    "italic",
                    "underline",
                    "strikethrough",
                    "justifyLeft",
                    "justifyCenter",
                    "justifyRight",
                    "justifyFull",
                    "insertUnorderedList",
                    "insertOrderedList",
                    "indent",
                    "outdent",
                    "createLink",
                    "unlink",
                    "insertImage",
                    "insertFile",
                    "tableWizard",
                    "createTable",
                    "addColumnLeft",
                    "addColumnRight",
                    "deleteRow",
                    "deleteColumn",
                    "cleanFormatting",
                    "fontName",
                    "fontSize",
                    "foreColor",
                    "backColor"
                ]
            }); 


           function load_requerimientos(){
                
                $("#grid").kendoGrid({
                        dataSource: {
                            type: "json",
                            transport: {
                                read : { 
                                    url: url + 'load-requerimientos',
                                    dataType: "json",
                                    complete:function(){
                                       
                                    }
                                },parameterMap:function(a,b){
                                       return{
                                           est : $('#estado_tikets').val(),
                                           id_tip_requerimiento : 0,
                                           req_prioridad : 0,
                                           opt : 'sistemas',
                                           fec : $('#filter_fec').val(),
                                           start : $("#start").val(),
                                           end : $("#end").val()
                                       }
                                }   
                            },
                            pageSize: 300
                        },
                        height: $(window).height() - 220,
                        sortable: false,
                        dataBound: onDataBound,
                        filterable: true,
                        groupable: true,
                        pageable: {
                          refresh: true,
                          pageSizes: true,
                          buttonCount: 100,
                          messages:{
                            display:"{0} - {1} de {2} registros",
                            itemsPerPage: "registros por pagina",
                            empty: "No hay registros"
                          }
                        },columns: [{
                            title: "Acc",
                            template: "#=view_requerimiento(id_detalle_requerimiento,est_des) #",
                            width: 90,
                            locked: true
                        },{
                            title: "Evaluación",
                            //template: "#=video(id_detalle_requerimiento,path_o_cod_video) #",
                            field: "des_tip_calificacion",
                            width: 130,
                            locked: true
                        },{
                            title: "Responsable",
                            field: "responsable",
                            template: "#=asignar_(id_detalle_requerimiento,responsable,est_des) #",
                            width: 250,
                            locked: true,
                            filterable: {
                                extra: false,
                                operators: {
                                    string: {
                                        contains: "Contiene",
                                    }
                                },
                                cell: {
                                    operator: "contains",
                                    suggestionOperator: "contains"
                                }
                            }
                        },{
                            field: "id_detalle_requerimiento",
                            title: "Nº Ticket",
                            width: 100,
                            aggregates: ["count"],
                            groupHeaderColumnTemplate: "Total : #=count#",
                            filterable: {
                                extra: false,
                                operators: {
                                    string: {
                                        contains: "Contiene",
                                    }
                                },
                                cell: {
                                    operator: "contains",
                                    suggestionOperator: "contains"
                                }
                            }
                        },{
                            field: "titulo_requerimiento",
                            title: "Titulo",
                            width: 350,
                            filterable: {
                                extra: false,
                                operators: {
                                    string: {
                                        contains: "Contiene",
                                    }
                                },
                                cell: {
                                    operator: "contains",
                                    suggestionOperator: "contains"
                                }
                            }
                        },{
                            field: "est_des",
                            title: "Estado",
                            width: 100,
                            filterable: {
                                extra: false,
                                operators: {
                                    string: {
                                        contains: "Contiene",
                                    }
                                },
                                cell: {
                                    operator: "contains",
                                    suggestionOperator: "contains"
                                }
                            }

                        },{
                            field: "tiempo_atencion",
                            title: "TA",
                            width: 110,
                            filterable: {
                                extra: false,
                                operators: {
                                    string: {
                                        contains: "Contiene",
                                    }
                                },
                                cell: {
                                    operator: "contains",
                                    suggestionOperator: "contains"
                                }
                            }
                        }, {
                            field: "prioridad",
                            title: "Prioridad",
                            width: 110,
                            filterable: {
                                extra: false,
                                operators: {
                                    string: {
                                        contains: "Contiene",
                                    }
                                },
                                cell: {
                                    operator: "contains",
                                    suggestionOperator: "contains"
                                }
                            }
                        },{
                            field: "tiempo_solucion",
                            title: "TS",
                            width: 110,
                            filterable: {
                                extra: false,
                                operators: {
                                    string: {
                                        contains: "Contiene",
                                    }
                                },
                                cell: {
                                    operator: "contains",
                                    suggestionOperator: "contains"
                                }
                            }
                        }, {
                            field: "tipo_requerimiento",
                            title: "Tipo",
                            width: 100,
                            filterable: {
                                extra: false,
                                operators: {
                                    string: {
                                        contains: "Contiene",
                                    }
                                },
                                cell: {
                                    operator: "contains",
                                    suggestionOperator: "contains"
                                }
                            }
                        },{
                            field: "usuario",
                            title: "Usuario",
                            width: 250,
                            filterable: {
                                extra: false,
                                operators: {
                                    string: {
                                        contains: "Contiene",
                                    }
                                },
                                cell: {
                                    operator: "contains",
                                    suggestionOperator: "contains"
                                }
                            }
                        },{
                            field: "area_usu",
                            title: "Area",
                            width: 250,
                            filterable: {
                                extra: false,
                                operators: {
                                    string: {
                                        contains: "Contiene",
                                    }
                                },
                                cell: {
                                    operator: "contains",
                                    suggestionOperator: "contains"
                                }
                            }
                        },{
                            field: "fec_apertura",
                            title: "Fecha",
                            width: 130,
                            filterable: false
                        }]
                    });
            }

            function datime_(element){
                element.kendoDatePicker({
                  format:'{0: dd-MM-yyyy}',
                  selectable: "multiple"
                })
            }

function view_requerimiento(id_detalle_requerimiento,est_des){
                var html = ''

                if ( est_des == 'En Proceso' )
                     html+= '<center><span class="ti-ticket" style="font-size: 25px !important;cursor:pointer;color: #5c444c" est="'+est_des+'" onclick="view_detalle_req('+parseInt(id_detalle_requerimiento)+')"></span> <span class="ti-control-play" onclick="cambio_estado('+parseInt(id_detalle_requerimiento)+',3)" style="font-size: 25px !important;cursor:pointer;color: #5c444c" est="'+est_des+'"></span></center>'
                else if ( est_des == 'Pausa' )
                     html+= '<center><span class="ti-ticket" style="font-size: 25px !important;cursor:pointer;color: #5c444c" est="'+est_des+'" onclick="view_detalle_req('+parseInt(id_detalle_requerimiento)+')"></span> <span class="ti-control-pause" style="font-size: 25px !important;cursor:pointer;color: #5c444c" est="'+est_des+'" onclick="cambio_estado('+parseInt(id_detalle_requerimiento)+',1)"></span></center>' 
                else 
                     html+= '<center><span class="ti-ticket" style="font-size: 25px !important;cursor:pointer;color: #5c444c" est="'+est_des+'" onclick="view_detalle_req('+parseInt(id_detalle_requerimiento)+')"></span></center>' 

                return html
            }

            function asignar_(id_detalle_requerimiento,responsable,est)
            {
                var cbo_html
                
                if ( est != 'Terminado' ) { 
                if ( responsable.length < 5 ){
                    cbo_html= "<select class='custom-select' style='width: 150px !important;height: 15px !important;' id_detalle_requerimiento='"+id_detalle_requerimiento+"' onchange='_responsable(this)'>"
                    cbo_html+= cbo_trabajadores
                    cbo_html+= "</select>"
                } else {
                    cbo_html= "<span style='cursor:pointer' id_detalle_requerimiento='"+id_detalle_requerimiento+"' onclick='cbo_trabajadores__(this)'>"+responsable+"</span>"
                }
                } else {
                    cbo_html= "<span id_detalle_requerimiento='"+id_detalle_requerimiento+"'>"+responsable+"</span>"  
                }
                    
                return cbo_html 
            }

            function cbo_trabajadores__(id)
            {
                var cbo_html,id_detalle_requerimiento = $(id).attr('id_detalle_requerimiento')
                cbo_html= "<select class='custom-select' style='width: 150px !important;height: 15px !important;' id_detalle_requerimiento='"+id_detalle_requerimiento+"' id='cbo____'>"
                cbo_html+= cbo_trabajadores
                cbo_html+= "</select>"
                
                kendo.confirm(cbo_html).then(function () {
                    _responsable('#cbo____')
                })
                $('.k-dialog-titlebar').remove();
            }

            function video(id_detalle_requerimiento,path_o_cod_video) {
                path_o_cod_video_v = ( path_o_cod_video == 0 ) ? '' : path_o_cod_video
                return '<input class="form-control" valor="'+id_detalle_requerimiento+'"  value="'+path_o_cod_video_v+'" onkeypress="guardar_ruta(event,this)"  style="width: 70px !important;height: 8px !important;">'
            } 

            function _responsable(id) {
               
               if ($(id).val().length > 0 ){
                    $.ajax({
                        url: url + 'update-responsable',
                        type: 'POST',
                        data: { id_detalle_requerimiento : parseInt($(id).attr('id_detalle_requerimiento')), id_usuario : $(id).val() },
                        success : function (resp) {
                            change_estado()
                        }
                    })
                } else {
                   alert("Seleccione un Responsable")
                }
            }

            function change_estado() {
                $('#grid').data('kendoGrid').dataSource.read()
            }

            function view_detalle_req(id_detalle_requerimiento){
              $.ajax({
                 url: url + 'datalle-requerimiento',
                 type: 'POST',
                 dataType: 'json',
                 data: { id_detalle_requerimiento: id_detalle_requerimiento },
                 success : function(resp) {

                      load_historial(id_detalle_requerimiento)
                      $('#mdl_detalle_requerimiento_tittle').html( resp.tipo_requerimiento + ' - ' + resp.titulo_requerimiento )
                      $('#_descripcion_req').html(resp.desc_requerimiento)
                      $('#usu_apertura').html(resp.usuario)
                      $('#_objetivo_solicitud').html(resp.obj_requerimiento)
                      $('#fec_est_ter').html(resp.fec_est_termino)
                      $('#v_area_usu').html(resp.area_usu)
                      $('#codigo_req').html(resp.id_detalle_requerimiento)
                      $('#fec_est_sis_ter').html(resp.fec_est_sistmas)
                      $('#input_id_detalle_requerimiento').val(id_detalle_requerimiento)
                      $('#capertura_req').html(resp.fec_apertura)
                      $('#estado_req').html(resp.est_des)
                      $('#prioridad_req').html(resp.prioridad)
                      $('#__prioridad_req').val(resp.req_prioridad)
                      $('#fec_est_sistemas_').val(resp.fec_est_sistmas)
                      $('#__estado_req').val(resp.est)
                      $('.btn_tip_requerimiento').html(resp.tipo_requerimiento)
                      $('#descargar_evi__').html(resp.path_adjunto)
                      $('#descargar_evi__').attr('arch',resp.path_adjunto)

                      if ( resp.tipo_requerimiento == 'Solicitud' ) {
                           $('#fec_est_ter__').css('display','block')
                           $('#objetivo_solicitud').css('display', 'block')
                           $('#fec_est_ter_sis__').css('display','block')
                           $('#fec_est_sistemas').css('display','block')

                           $('#_descripcion_req').css({
                               'width':'100%',
                               'overflow-y':'scroll',
                               'height':'430px'
                           });

                           $('#_objetivo_solicitud').css({
                               'width':'100%',
                               'overflow-y':'scroll',
                               'height':'300px'
                           });

                      } else {
                           $('#fec_est_ter__').css('display','none')
                           $('#objetivo_solicitud').css('display', 'none')
                           $('#fec_est_ter_sis__').css('display','none')
                           $('#fec_est_sistemas').css('display','none')
                           $('#_descripcion_req').css({
                               'width':'100%',
                               'overflow-y':'scroll',
                               'height':'820px'
                           });
                      }

                      if ( resp.est == 1 || resp.est == 2 ) {
                          $('#btn_abrir_requerimiento').css('display','none')
                          $('#com_solicitud').css('display','block')
                          $('#btn_asignar_rq').css('display','block')
                          if ( resp.id_usuario_asig == '0' ){
                               $('#com_solicitud').css('display','none')
                               $('#btn_asignar_rq').css('display','none')
                          } else {
                               if ( resp.id_usuario_asig == $('#id_usuario').val()){
                                    $('#com_solicitud').css('display','block')
                                    $('#btn_asignar_rq').css('display','block')
                               }else{
                                    $('#com_solicitud').css('display','none')
                                    $('#btn_asignar_rq').css('display','none')
                               }
                          }
                      } else if (resp.est == 0 ) {
                          $('#btn_asignar_rq').css('display','none')
                          $('#com_solicitud').css('display','none');
                      }

                      $('#mdl_detalle_requerimiento').modal('show')
                 }
               }) 
            }



            $('#btn_asignar_rq').click(function(event) {
                
                arr_valida.length = 0

                if (__prioridad_req.val().length == 0 )
                    arr_valida.push('Llenar Prioridad')

                if (__estado_req.val().length == 0 )
                    arr_valida.push('Seleccione Estado')

                if (req_hist_comentario.val().length == 0 )
                    arr_valida.push("Llenar Comentario")

                if ( $('.btn_tip_requerimiento').text() == 'Solicitud' ) {
                     if ($('#fec_est_sistemas_').val().length == 0 )
                     arr_valida.push("Llenar fecha de termino")
                }

                if ( arr_valida.length == 0 ) {

                     param = {
                         input_id_detalle_requerimiento : $('#input_id_detalle_requerimiento').val(),
                         __prioridad_req : __prioridad_req.val(),
                         __estado_req : __estado_req.val(),
                         req_hist_comentario : req_hist_comentario.data("kendoEditor").value(),
                         id_usuario :  $('#id_usuario').val(),
                         id_detalle_requerimiento_remp : $('#__tikets_req').val(),
                         fec_est_sistmas : $('#fec_est_sistemas_').val()
                     }

                     $.ajax({
                         url:  url + 'insert-historia',
                         type: 'POST',
                         data: param,
                         success : function(){
                            load_historial($('#input_id_detalle_requerimiento').val())
                            change_estado()
                            $('#tihet_select').css('display', 'none');
                            $('#close').click()
                            req_hist_comentario.data("kendoEditor").value("")
                         }
                     })

                }  else {
                    alert(arr_valida)
                }
                
            });


            function onDataBound(arg) {
                var grid = $("#grid").data("kendoGrid");
                var data = grid.dataSource.data();
                for (var i= 0; i < data.length ; i++) {
                    if (data[i].req_prioridad == '1')
                      $('tr[data-uid="' + data[i].uid + '"] ').addClass('red')
                    else if ( data[i].req_prioridad == '2')
                      $('tr[data-uid="' + data[i].uid + '"] ').addClass('orange')
                    else if ( data[i].req_prioridad == '3' )
                      $('tr[data-uid="' + data[i].uid + '"] ').addClass('green')
                    if ( data[i].tiempo_atencion == "Incumplimiento de SLA" )
                        $('tr[data-uid="' + data[i].uid + '"] ').find('td').eq(6).css({ 'background' : '#003282','color' : '#fff'})
                    if ( data[i].tiempo_solucion == "Incumplimiento de SLA" )
                        $('tr[data-uid="' + data[i].uid + '"] ').find('td').eq(8).css({ 'background' : '#424242 ','color' : '#fff'})
                }  
            }
            
            function guardar_ruta(e,id) {
                
                if (e.keyCode == 13) {

                    kendo.confirm("Desea guardar ?").then(function () {
                        $.ajax({
                            url : url + 'update-ruta',
                            data : { valor : $(id).val(), id_detalle_requerimiento : $(id).attr('valor') },
                            type: 'post',
                            success : function(data){
                                change_estado()
                            }
                        })
                    });

                    $('.k-dialog-titlebar').remove()
                }

            }

            function cambio_estado(id_detalle_requerimiento,estado) {

                 var mensaje = ( estado == 1 ) ? 'Desea modificar el estado del requerimiento a "En proceso" ?' : 'Desea modificar el estado del requerimiento a "Pausa" ?'
                 kendo.confirm(mensaje).then(function () {
                    $.ajax({
                           url:  url + 'update-estado-requerimiento',
                           type: 'POST',
                           data: { id_detalle_requerimiento: id_detalle_requerimiento, est : estado, id_usuario : $('#id_usuario').val() },
                           success : function(resp){
                                change_estado()
                           }
                    })

                 })
                 $('.k-dialog-titlebar').remove();
            }

    </script>