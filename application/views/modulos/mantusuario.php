<div class="row">
        <div class="col-lg-4 col-ml-4">
            <div class="row">
                <div class="col-12 top_div">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="des_usuario" class="col-form-label">Apellidos y Nombres :<span style="color: red"> (*) </span></label>
                                <input class="form-control" type="text" id="des_usuario">
                            </div>
                            <input type="hidden" id='id_usuario'>
                            <div class="form-group">
                                <label for="email_usuario" class="col-form-label">Email :<span style="color: red"> (*) </span></label>
                                <input class="form-control" type="text" id="email_usuario">
                            </div>
                            <div class="form-group">
                                <label class="col-form-label">Área <span style="color: red"> (*) </span></label>
                                <select class="custom-select" id='id_area'><option value=''>Seleccione opción</option></select>
                            </div>
                            <div class="form-group">
                                <label class="col-form-label">Tipo <span style="color: red"> (*) </span></label>
                                <select class="custom-select" id='id_tip_usu'><option value=''>Seleccione opción</option></select>
                            </div>
                            <div class="form-group">
                                <label for="usuario" class="col-form-label">Usuario :<span style="color: red"> (*) </span></label>
                                <input class="form-control" type="text" id="usuario">
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="hidden" id="pass_vis" value='123456'>
                            </div>
                            <div class="form-group" id='div_est_pass'>
                                <label for="est_pass" class="col-form-label">Restablecer Contraseña :<span style="color: red"> (*) </span></label>
                                <select class="custom-select" id='est_pass'>
                                    <option value='1'>Si</option>
                                    <option value='0'>No</option>
                                </select>
                            </div>

                            <center><button type="button" class="btn btn-outline-primary mb-3" id="guardar_usuario" tip="insert">Guardar</button></center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-8 col-ml-8">
            <div class="row">
                 <div class="col-12 top_div">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="header-title">Usuarios</h4>
                            <div class="single-table" id='grid'>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">

    
        var des_usuario = $('#des_usuario'),
                email_usuario = $('#email_usuario'),
                id_area = $('#id_area'),
                id_tip_usu = $('#id_tip_usu'),
                usuario = $('#usuario'),
                pass_vis = $('#pass_vis'),
                est_pass = $('#est_pass'),
                param,ruta_t,
                msg,
                arr_valida = new Array()


            carga_tipos_requerimiento($('#id_area'),'load-area',{ ft : '1' })
            carga_tipos_requerimiento($('#id_tip_usu'),'load-tipo-usuario',{ ft : '1' })
            load_usuarios()
            
                $('#guardar_usuario').click(function(event) {
                    event.preventDefault();
                    if ( $(this).attr('tip') == 'insert' ) 
                         ruta_t = "insert-usuario"
                    else
                         ruta_t = "update-usuario?id_usuario="+$('#id_usuario').val()

                    arr_valida.length = 0
                    if( des_usuario.val().length == 0 )
                    arr_valida.push("Llenar Nombres y Apellidos")
                    if( email_usuario.val().length == 0 )
                        arr_valida.push("Llenar Email")
                    if( id_area.val().length == 0 )
                        arr_valida.push("Seleccione área")
                    if( id_tip_usu.val().length == 0 )
                        arr_valida.push("Seleccione Tipo")
                    if( usuario.val().length == 0 )
                        arr_valida.push("Llenar usuario")
                    if( pass_vis.val().length == 0 )
                        arr_valida.push("Llenar password")
                    if ( arr_valida.length == 0 ) {
                         param = {
                                     des_usuario : des_usuario.val(),
                                     email_usuario : email_usuario.val(),
                                     id_area : id_area.val(),
                                     id_tip_usu : id_tip_usu.val(),
                                     usuario : usuario.val(),
                                     pass_vis : pass_vis.val(),
                                     pass_inc : md5(pass_vis.val()),
                                     est_pass : $('#est_pass').val()
                                 }

                         $.ajax({
                             url: url + ruta_t,
                             type: 'POST',
                             data: param,
                             success : function(){
                                  load_usuarios()
                                  limpia_form()
                             }
                         })
                    } else {
                           msg = '' 
                           for (var i = 1; i <= arr_valida.length; i++)
                                msg+= "<span style='color:red;font-size:14px'>"+i+"- "+arr_valida[i-1]+"</span></br>"
                           
                           kendo.alert(msg);
                           $('.k-dialog-titlebar').remove();
                    }

                });
       

      
        function load_usuarios(){

                $("#grid").kendoGrid({
                    dataSource: {
                        type: "json",
                        transport: {
                            read: url + 'load-user'
                        },
                        pageSize: 100
                    },
                    height: $(window).height() - 200,
                    sortable: true,
                    pageable: {
                        refresh: true,
                        pageSizes: true,
                        buttonCount: 100
                    },filterable: {
                            mode: "row"
                    },columns: [{
                        title: "Acc",
                        template: "#=acciones(id_usuario) #",
                        width: 100
                    },{
                        field: "des_usuario",
                        title: "Datos",
                        width: 400,
                        filterable: {
                            cell: {
                                operator: "contains",
                                suggestionOperator: "contains"
                            }
                        }
                    }, {
                        field: "des_area",
                        title: "Área",
                        width: 250,
                        filterable: {
                            cell: {
                                operator: "contains",
                                suggestionOperator: "contains"
                            }
                        }
                    }, {
                        field: "des_tip_usu",
                        title: "Tipo de usuario",
                        width: 150,
                        filterable: {
                            cell: {
                                operator: "contains",
                                suggestionOperator: "contains"
                            }
                        }
                    },{
                        field: "usuario",
                        title: "Usuario",
                        width: 150,
                        filterable: {
                            cell: {
                                operator: "contains",
                                suggestionOperator: "contains"
                            }
                        }
                    }]
                });
        }

        function acciones(id_usuario) {
            var html = '<ul class="d-flex justify-content-center"><li class="mr-3"><a class="text-secondary" onclick="update_usu('+id_usuario+')" ><i class="fa fa-edit"></i></a></li><li><a class="text-danger" onclick="delete_usu('+id_usuario+')"><i class="ti-trash"></i></a></li></ul>'
            return html
        }

        function delete_usu(id) {
            $.post( url + 'delete-usuario', { id_usuario : id }, function(data, textStatus, xhr) {
                 load_usuarios()
            });
        }

        function update_usu(id) {
            $.post( url + 'detalle-usuario', { id_usuario: id }, function(resp, textStatus, xhr) {
                console.log(resp)
                $('#div_est_pass').css('display','block')
                $('#guardar_usuario').attr('tip','update')
                $('#id_usuario').val(resp.id_usuario)
                des_usuario.val(resp.des_usuario)
                email_usuario.val(resp.email_usuario)
                id_area.val(resp.id_area)
                id_tip_usu.val(resp.id_tip_usu)
                usuario.val(resp.usuario)
                pass_vis.val(resp.pass_vis)
                est_pass.val(resp.est_pass)
            });
        }

        function limpia_form() {
                $('#guardar_usuario').attr('tip','insert')
                $('#id_usuario').val('')
                des_usuario.val('')
                email_usuario.val('')
                id_area.val('')
                id_tip_usu.val('')
                usuario.val('')
                pass_vis.val('')
        }

    </script>