<div class="row">
        <div class="col-lg-12 col-ml-12">
            <div class="row">
                 <div class="col-12 top_div">
                    <div class="card">
                        <div class="card-body">
                            <div id='calendar'></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
      
      var html = ''

		  var calendarEl = document.getElementById('calendar')
		  var calendar = new FullCalendar.Calendar(calendarEl, {
          height: $(window).height() - 230,
			    plugins: [ 'timeGrid' ],
          locale: 'es',
			    timeZone: 'UTC',
			    defaultView: 'timeGridWeek',
			    header: {
			      left: 'prev,next',
			      center: 'title',
			      right: 'timeGridDay,timeGridWeek'
			    },
			    views: {
			      timeGridWeek: {
			        type: 'timeGrid'
			      }
			    },
			    events: url + 'Requerimiento_C/reserva_tf',
          eventClick: function(info) {

              $.get( url + 'Requerimiento_C/proveedor_reserva', { id_spt_m : info.event.id } ,function(json) {

                  var tpm = "<span><b>Reserva de : </b> "+json.ac_spt_m+"</span><br>"
                      tpm+= "<span><b>Responsable: </b> "+json.responsable_spt_m+"</span><br>"
                      tpm+= "<span><b>Celular    : </b> "+json.tel_res_spt_m+ "</span><br>"
                      tpm+= "<span><b>Email      : </b> "+json.email_res_spt_m+ "<span><br>"
                      tpm+= "<span><b>N°de Ticket: </b> "+json.nun_tick_spt_m+ "<span><br>"
                      tpm+= "<span><b>Ambiente   : </b> "+json.amb_spt_m+"</span><br>"
                      tpm+= "<span><b>Motivo     : </b> "+json.motivo_spt_m+"</span><br>"
                      tpm+= "<span><b>Observación: </b> "+json.mens_spt_m+"<span><br><br><br>"

               if ( json.est_spt_m == '1') {

                           if ( json.est_aten_spt_m == '0') {
                                tpm+= "<center><button type='button' class='btn btn-secondary' data-dismiss='modal' onclick='cerrar_window()'>Cerrar</button> <button type='button' class='btn btn-danger' data-dismiss='modal' id='close' onclick='delete_reserva("+info.event.id+")' >Eliminar</button> <button type='button' class='btn btn-primary' data-dismiss='modal' id='close' onclick='atender_reserva("+info.event.id+")' >Atender</button></center>"
                           } else {
                                tpm+= "<center><button type='button' class='btn btn-secondary' data-dismiss='modal' onclick='cerrar_window()'>Cerrar</button> <button type='button' class='btn btn-danger' data-dismiss='modal' id='close' onclick='delete_reserva("+info.event.id+")' >Eliminar</button></center>"
                           }
                           
                           

                      } else {
                           tpm+= "<center><button type='button' class='btn btn-secondary' data-dismiss='modal' onclick='cerrar_window()'>Cerrar</button></center>"
                      }

                           
                      $('#ventana_').html(tpm)
                      $('#ventana_').kendoWindow({
                            width: "350px",
                            height: "220px",
                            title: 'Detalle',
                            visible: false,
                            actions: [
                                "Close"
                            ]
                      }).data("kendoWindow").center().open()


              });
           }
			  });

 			  calendar.render();

        function cerrar_window(id_reser_equipo) {
           $('#ventana_').data("kendoWindow").close()
        }

        function delete_reserva(id_spt_m) {
           $.post( url + 'Requerimiento_C/delete_pro_reserva', {id_spt_m: id_spt_m}, function(data, textStatus, xhr) {
               $('#ventana_').data("kendoWindow").close()  
               $('li[class="active"]').click();
           });
        }

        function atender_reserva(id_spt_m) {
           
           $.post( url + 'Requerimiento_C/atender_pro_reserva', {id_spt_m: id_spt_m,id_usuario : $('#id_usuario').val() }, function(data, textStatus, xhr){
               $('#ventana_').data("kendoWindow").close()  
               $('li[class="active"]').click();
           });

        }

    </script>