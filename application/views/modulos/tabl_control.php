
<div class="row">
    
    <div class="col-ml-12 col-lg-12 col-md-12 top_div" style="margin-top: 20px !important">
        <label> Tipo : </label>
        <select class="custom-select" style="width: 150px !important" id='option_tipo'>
            <option value='2'>Incidencia</option>
            <option value='1'>Solicitud</option>
        </select>

        <label> Opción : </label>
        <select class="custom-select" style="width: 150px !important" id='option_perfil'>
            <?php if ( $this->session->userdata('des_tip_usu') == "supervisor" ) { ?>
                <option value='1'>Gerencial</option>
                <option value='2'>Jefatura</option>
            <?php } else { ?>
            <option value='3'>Usuario</option>
            <?php } ?>
        </select>
    </div>
        
    <div class="col-ml-6 col-lg-6 col-md-6 top_div" >
        <div class="card h-full">
            <div class="card-body" id="__productividad_mes_actual">
                <h4 class="header-title" style="font-size: 15px !important"> Productividad ( % ) </h4>
            </div>
        </div>
    </div>

    <div class="col-ml-6 col-lg-6 col-md-6 top_div" >
        <div class="card h-full">
            <div class="card-body" id='__calificacion_mes_actual'>
                <h4 class="header-title" style="font-size: 15px !important">Calificación  ( % ) </h4>
            </div> 
        </div>
    </div>

    <div class="col-ml-6 col-lg-6 col-md-6 top_div" >
        <div class="card h-full">
            <div class="card-body" id="__SLA_TA_mes_actual"> 
                <h4 class="header-title" style="font-size: 15px !important">SLA - tiempo de atención ( % )</h4>
            </div>
        </div>
    </div>

    <div class="col-ml-6 col-lg-6 col-md-6 top_div" >
        <div class="card h-full">
            <div class="card-body" id='__SLA_TS_mes_actual'>
                <h4 class="header-title" style="font-size: 15px !important"> SLA - tiempo de solución ( % ) </h4>
            </div>
        </div>
    </div>

    <div class="col-ml-12 col-lg-12 col-md-12 top_div" id='__productividad_anio_'>
        <div class="card h-full">
            <div class="card-body" id='__productividad_anio'>
                <h4 class="header-title" style="font-size: 15px !important"> Productividad del año <?php echo date('Y') ?> ( Total ) </h4>
            </div>
        </div>
    </div>

    <div class="col-ml-12 col-lg-12 col-md-12 top_div" id='__tiket_registrados_'>
        <div class="card h-full">
            <div class="card-body" id='__tiket_registrados'>
                <h4 class="header-title" style="font-size: 15px !important"> Requerimientos registrados de año <?php echo date('Y') ?> (Total) </h4>
            </div>
        </div>
    </div>

    <div class="col-ml-12 col-lg-12 col-md-12 top_div" id='__tiket_terminados_'>
        <div class="card h-full">
            <div class="card-body" id='__tiket_terminados'>
                <h4 class="header-title" style="font-size: 15px !important"> Requerimientos terminados de año <?php echo date('Y') ?> (Total)</h4>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
    var barChartData

    cargar_data()
  
    $('#option_perfil').change(function(event) {
        cargar_data()
    })

    $('#option_tipo').change(function(event) {
        cargar_data()
    })

    function cargar_data() {
        $.get( url + 'Requerimiento_C/cmi' , {  option_perfil : $('#option_perfil option:selected').val(), id_usuario : $('#id_usuario').val(), id_tip_requerimiento : $('#option_tipo option:selected').val(), des_tip_usu : $('#des_tip_usu').val() } , function(data, textStatus, xhr) {

            if ( $('#option_perfil option:selected').val() == 1 ||  $('#option_perfil option:selected').val() == 3 ) {
                 productividad(data)
                 calificacion(data)
                 SLA_TA(data)
                 SLA_TS(data)
                 productividad_anio(data)
                 $('#__productividad_anio_').css('display','block')
                 $('#__tiket_registrados').css('display','none')
                 $('#__tiket_terminados_').css('display','none')

            } else  {
                 jefatura_producividad(data)
            }

        })
    }

    function productividad(json) {
       
        $('#productividad_mes_actual').remove()
        $('#__productividad_mes_actual').append('<canvas id="productividad_mes_actual"></canvas>')

        var barChartData

        barChartData = {
                labels: json.data_productividad_grafico.fecha,
                datasets: [{
                    backgroundColor: window.chartColors.red, 
                    label: 'Pendientes',
                    data: json.data_productividad_grafico.PENDIENTE,
                },{
                    backgroundColor: window.chartColors.blue, 
                    label: 'En Proceso',
                    data: json.data_productividad_grafico.ENPROCESO
                },{
                    backgroundColor: window.chartColors.green, 
                    label: 'Terminado',
                    data: json.data_productividad_grafico.TERMINADOS
                },{
                    backgroundColor: window.chartColors.orange, 
                    label: 'Pausa',
                    data: json.data_productividad_grafico.ENPAUSA
                }]
          }

          var ctx = document.getElementById('productividad_mes_actual').getContext('2d');
          window.myBar = new Chart(ctx, {
            type: 'bar',
            data: barChartData,
            options: {
                title: {
                    display: true,
                    text: 'Incidencia - Reporte de Productividad '
                },
                tooltips: {
                    mode: 'index',
                    intersect: false
                },
                responsive: true,
                scales: {
                    xAxes: [{
                        stacked: true,
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                }
            }
       })

    }


    function calificacion(json) {
        
        $('#calificacion_mes_actual').remove()
        $('#__calificacion_mes_actual').append('<canvas id="calificacion_mes_actual"></canvas>') 

        barChartData = {
                labels: json.data_calificacion_grafico.FECHA,
                datasets: [{
                    backgroundColor: window.chartColors.red, 
                    label: 'MUY MALO',
                    data: json.data_calificacion_grafico.MUYMALO,
                },{
                    backgroundColor: window.chartColors.blue, 
                    label: 'POBRE',
                    data: json.data_calificacion_grafico.POBRE
                },{
                    backgroundColor: window.chartColors.orange, 
                    label: 'NI BIEN NI MAL',
                    data: json.data_calificacion_grafico.NIBIENNIMAL
                },{
                    backgroundColor: window.chartColors.grey, 
                    label: 'BUENO',
                    data: json.data_calificacion_grafico.BUENO
                },{
                    backgroundColor: window.chartColors.green, 
                    label: 'MUY BUENO',
                    data: json.data_calificacion_grafico.MUYBUENO
                }]
          }

          var ctx = document.getElementById('calificacion_mes_actual').getContext('2d');
          window.myBar = new Chart(ctx, {
            type: 'bar',
            data: barChartData,
            options: {
                title: {
                    display: true,
                    text: 'Incidencia - Reporte de Productividad '
                },
                tooltips: {
                    mode: 'index',
                    intersect: false
                },
                responsive: true,
                scales: {
                    xAxes: [{
                        stacked: true,
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                }
            }
        })

    }

    function SLA_TA(json) {   


        $('#SLA_TA_mes_actual').remove()
        $('#__SLA_TA_mes_actual').append('<canvas id="SLA_TA_mes_actual"></canvas>')
        
         barChartData = {
                labels: json.data_sla_ta.FECHA,
                datasets: [
                      {
                          backgroundColor: window.chartColors.red, 
                          label: 'NO CUMPLIO',
                          data: json.data_sla_ta.NOCUMPLIO,
                      }, 
                      {
                          backgroundColor: window.chartColors.green,
                          label: 'SI CUMPLIO',
                          data : json.data_sla_ta.SICUMPLIO
                      }
                ]
          }

          var ctx = document.getElementById('SLA_TA_mes_actual').getContext('2d');
          window.myBar = new Chart(ctx, {
            type: 'bar',
            data: barChartData,
            options: {
                title: {
                    display: true,
                    text: 'SLA - Tiempo de Atencion'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false
                },
                responsive: true,
                scales: {
                    xAxes: [{
                        stacked: true,
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                }
            }
        })
    }

    function SLA_TS(json) 
    {

        $('#SLA_TS_mes_actual').remove()
        $('#__SLA_TS_mes_actual').append('<canvas id="SLA_TS_mes_actual"></canvas>')

        barChartData = {
                labels: json.data_sla_ts.FECHA,
                datasets: [
                      {
                          backgroundColor: window.chartColors.red, 
                          label: 'NO CUMPLIO',
                          data: json.data_sla_ts.NOCUMPLIO,
                      }, 
                      {
                          backgroundColor: window.chartColors.green,
                          label: 'SI CUMPLIO',
                          data : json.data_sla_ts.SICUMPLIO
                      }
                ]
          }

          var ctx = document.getElementById('SLA_TS_mes_actual').getContext('2d');
          window.myBar = new Chart(ctx, {
            type: 'bar',
            data: barChartData,
            options: {
                title: {
                    display: true,
                    text: 'SLA - Tiempo de Solución'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false
                },
                responsive: true,
                scales: {
                    xAxes: [{
                        stacked: true,
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                }
            }
          })
    }


    function productividad_anio(json) {

        $('#productividad_anio').remove()
        $('#__productividad_anio').append('<canvas id="productividad_anio"></canvas>')

        var config = {
            type: 'line',
            data: {
                labels: json.PRODUCTIVIDAD_ANIO.meses,
                datasets: [{
                    label: 'Pendientes',
                    backgroundColor: window.chartColors.red,
                    borderColor: window.chartColors.red,
                    data: json.PRODUCTIVIDAD_ANIO.pendiente.valor,
                    fill: false,
                }, {
                    label: 'Terminados',
                    fill: false,
                    backgroundColor: window.chartColors.green,
                    borderColor: window.chartColors.green,
                    data: json.PRODUCTIVIDAD_ANIO.terminado.valor,
                }, {
                    label: 'Total',
                    fill: false,
                    backgroundColor: window.chartColors.blue,
                    borderColor: window.chartColors.blue,
                    data: json.PRODUCTIVIDAD_ANIO.terminado.total,
                }]
            },
            options: {
                responsive: true,
                title: {
                    display: false,
                    text: 'Productividad'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Meses'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Valores'
                        }
                    }]
                }
            }
        };


        var ctx = document.getElementById('productividad_anio').getContext('2d');
        window.myLine = new Chart(ctx, config);
    }

    function str_graf_jefatura_productividad() {





    }

    

</script>