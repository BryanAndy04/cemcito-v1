<div class="row">
        <div class="col-lg-12 col-ml-12">
            <div class="row">
                <div class="col-12 top_div">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="des_usuario" class="col-form-label">Mensaje :<span style="color: red"> (*) </span></label>
                                <textarea id='des_mensaje' style="height: 650px"></textarea>
                            </div>
                            <input type="hidden" id='id_area'>
                            <center><button type="button" class="btn btn-outline-primary mb-3" id="guardar_mensaje" tip="insert">Guardar</button></center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <script type="text/javascript">
        

        
        
    
        $('#des_mensaje').kendoEditor({
                tools: [
                    "bold",
                    "italic",
                    "underline",
                    "strikethrough",
                    "justifyLeft",
                    "justifyCenter",
                    "justifyRight",
                    "justifyFull",
                    "insertUnorderedList",
                    "insertOrderedList",
                    "indent",
                    "outdent",
                    "createLink",
                    "unlink",
                    "insertImage",
                    "insertFile",
                    "tableWizard",
                    "createTable",
                    "addColumnLeft",
                    "addColumnRight",
                    "deleteRow",
                    "deleteColumn",
                    "cleanFormatting",
                    "fontName",
                    "fontSize",
                    "foreColor",
                    "backColor"
                ]
            });


           $.ajax({
                    url: url + 'read-mensaje',
                    type: 'GET',
                    success : function(resp) {
                        $("#des_mensaje").data("kendoEditor").value(resp.des_mensaje)
                    }
            })


           $('#guardar_mensaje').click(function(event) {
                $.ajax({
                    url: url + 'mensaje',
                    type: 'POST',
                    data: { des_mensaje : $("#des_mensaje").data("kendoEditor").value()},
                    success : function(argument) {
                        alert('Editado con exito')
                    }
                })
            });



    </script>