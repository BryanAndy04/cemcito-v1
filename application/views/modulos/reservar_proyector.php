    <div class="row">
        <div class="col-lg-3 col-ml-3">
            <div class="row">
                <div class="col-12 top_div">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="des_usuario" class="col-form-label">Reserva :<span style="color: red"> (*) </span></label>
                                <select class="custom-select" id='cbo_equipo'>
                                    <option value=''>Seleccione Equipo</option>
                                    <option value='1'>Proyector</option>
                                    <option value='2'>Sala</option>
                                    <option value='3'>Proyector y sala</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="des_usuario" class="col-form-label">Fecha :<span style="color: red"> (*) </span></label>
                                <input class="form-control" id="fecha" value="<?php echo date("Y-m-d") ?>">
                            </div>
                            <input type="hidden" id='id_usuario'>
                            <div class="form-group">
                                <label for="email_usuario" class="col-form-label">Hora de Inicio :<span style="color: red"> (*) </span></label>
                                <input class="form-control time_fec" type="text" id="fec_desde">
                            </div>

                            <div class="form-group">
                                <label for="email_usuario" class="col-form-label">Hora de fin :<span style="color: red"> (*) </span></label>
                                <input class="form-control time_fec" type="text" id="fec_hasta">
                            </div>

                            <div class="form-group">
                                <label for="email_usuario" class="col-form-label">Motivo :<span style="color: red"> (*) </span></label>
                                <textarea class="form-control" id="res_motivo" style="height: 250px"> </textarea>
                            </div>
                           
                            <center><button type="button" class="btn btn-outline-primary mb-3" id="guardar_reserva" tip="insert">Guardar</button></center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-9 col-ml-9">
            <div class="row">
                 <div class="col-12 top_div">
                    <div class="card">
                        <div class="card-body">
                            <div>
                                <h5 style='color: #e57373'><b>REGLAMENTO DE USO DEL PROYECTOR</b></h5> 
                                <p> 1 - "El usuario es responsable de recoger y devolver el proyector en buenas condiciones al área de sistemas". </p>
                                <p> 2 - "El usuario tiene <b>5 minutos de tolerancia</b> para devolver el equipo y <b> 3 oportunidades </b> para devolver a tiempo el proyector, caso contrario, su usuario no podrá reservar el equipo ".</p>
                                <p> 3 - "El usuario tiene <b>5 minutos de tolerancia</b> para recoger el equipo, caso contrario, su reserva se anulara automaticame ".</p>
                                <p> 4 - "Las reserva que figuran de color plomo, son los que no recogieron el proyector y est� disponible ".</p>
                            </div>
                            <hr>
                            <div id='calendar'></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
      
      var html = ''

      $("#fecha").kendoDatePicker({ format: "yyyy-MM-dd" });
      $("#fecha").click(function(){
          $(this).data("kendoDatePicker").open()
      }) ;

      $('.time_fec').timepicker({
         timeFormat: 'hh:mm p',
         interval: 30,
         minTime: '07:00',
         maxTime: '21:00',
         defaultTime: '07:00',
         startTime: '07:00',
         dynamic: false,
         dropdown: true,
         scrollbar: true
      });

		  var calendarEl = document.getElementById('calendar'),
              fecha = $('#fecha'),
              fec_desde = $('#fec_desde'),
              fec_hasta = $('#fec_hasta'),
              valida = new Array()

		  var calendar = new FullCalendar.Calendar(calendarEl, {
			    plugins: [ 'timeGrid' ],
          locale: 'es',
			    timeZone: 'UTC',
			    defaultView: 'timeGridWeek',
			    header: {
			      left: 'prev,next',
			      center: 'title',
			      right: 'timeGridDay,timeGridWeek'
			    },
			    views: {
			      timeGridWeek: {
			        type: 'timeGrid'
			      }
			    },
			    events: url + 'list-reservas',
          eventClick: function(info) {
              
              html = ""
              $.getJSON( url + 'read-det-reserva', { id_reser_equipo : info.event.id}, function(json, textStatus) {

                 html+= "<span><b>Reserva de : </b> "+json.des_equipo+"</span><br>" 
                 html+= "<span><b>Usuario : </b> "+json.des_usuario+"</span><br>"
                 html+= "<span><b>Fecha : </b> "+json.fecha+"</span><br>"
                 html+= "<span><b>Desde : </b> "+json.fec_desde + "</span><br><span> <b> Hasta : </b> "+json.fec_hasta+" </span><br>"
                 html+= "<span><b>Motivo : </b></span><br>"
                 html+= "<span>"+json.res_motivo+"</span><br><br><br>"
                 
                 if ( json.est == '1' ){ 
                      if( json.id_usuario == $('#id_usuario').val() ){ 

                           if ( $('#des_tip_usu').val() == 'sistemas' )

                                if ( json.est_alert == '1' )
                                     html+= "<center><button type='button' class='btn btn-primary' data-dismiss='modal' id='close' onclick='terminar_reserva("+info.event.id+")' >Entregar</button> <button type='button' class='btn btn-danger' data-dismiss='modal' id='close' onclick='delete_reserva("+info.event.id+")' >Eliminar</button></center>"
                                else if ( json.est_devolucion == '1' ) 
                                    html+= "<center><button type='button' class='btn btn-primary' data-dismiss='modal' id='close' onclick='devolucion_equipo("+info.event.id+")' >Devolver</button><button type='button' class='btn btn-danger' data-dismiss='modal' id='close' onclick='delete_reserva("+info.event.id+")' >Eliminar</button></center>"
                                else 
                                    html+= "<center><button type='button' class='btn btn-danger' data-dismiss='modal' id='close' onclick='delete_reserva("+info.event.id+")' >Eliminar</button></center>"   

                           else 
                               html+= "<center><button type='button' class='btn btn-danger' data-dismiss='modal' id='close' onclick='delete_reserva("+info.event.id+")' >Eliminar</button></center>"    

                       } else {
                            
                           if ( $('#des_tip_usu').val() == 'sistemas' ) {
                                
                                if ( json.est_alert == '1' )
                                     html+= "<center><button type='button' class='btn btn-primary' data-dismiss='modal' id='close' onclick='terminar_reserva("+info.event.id+")' >Entregar</button> <button type='button' class='btn btn-danger' data-dismiss='modal' id='close' onclick='delete_reserva("+info.event.id+")' >Eliminar</button></center>"
                                else if ( json.est_devolucion == '1' ) 
                                    html+= "<center><button type='button' class='btn btn-primary' data-dismiss='modal' id='close' onclick='devolucion_equipo("+info.event.id+")' >Devolver</button><button type='button' class='btn btn-danger' data-dismiss='modal' id='close' onclick='delete_reserva("+info.event.id+")' >Eliminar</button></center>"
                                else 
                                    html+= "<center><button type='button' class='btn btn-danger' data-dismiss='modal' id='close' onclick='delete_reserva("+info.event.id+")' >Eliminar</button></center>"
                      
                           } else {
                                html+= "<center><button type='button' class='btn btn-secondary' data-dismiss='modal' onclick='cerrar_window()'>Cerrar</button></center>"
                           }
                              
                       }  
                 }


                 var opt_ = ( json.est == '1') ? 'Reserva ' : 'Reserva eliminada automaticame' 
                   

                 $('#ventana_').html(html)
                 $('#ventana_').kendoWindow({
                      width: "350px",
                      height: "220px",
                      title: opt_,
                      visible: false,
                      actions: [
                          "Close"
                      ]
                 }).data("kendoWindow").center().open()

              });
           }
			  });

 			  calendar.render();

        $('#guardar_reserva').click(function(event) {
               
               valida.length = 0

               if ( $('#cbo_equipo').val().length == 0 )
                    valida.push("Ingrese el Equipo")
               if ( $('#res_motivo').val().length < 2 )
                    valida.push("Ingrese el motivo")
               if ( valida.length == 0 ) {
                    $.ajax({
                         url: url + 'insert-reserva',
                         type: 'POST',
                         data: { id_equipo : $('#cbo_equipo').val(), id_usuario : $('#id_usuario').val(), fecha: fecha.val(), fec_desde : fec_desde.val(), fec_hasta : fec_hasta.val(), res_motivo : $('#res_motivo').val() },
                         success : function(resp){
                            var resp_t = resp.split('&')
                            if (  resp_t[0].trim() == '1' && resp_t[1].trim() == '1' ) {
                                 $('li[class="active"]').click()
                            } else if ( resp_t[1].trim() == '1' ) {
                                 kendo.confirm("<span style='color:red;font-size:14px'>"+resp_t[0]+"</span></br>").then(function () {
                                    $('li[class="active"]').click()
                                 }, function () {});     
                                $('div[class="k-dialog-buttongroup k-dialog-button-layout-stretched"]').find('button').eq(1).remove()
                                $('.k-dialog-titlebar').remove()
                            } else {

                                    
                                 kendo.alert("<span style='color:red;font-size:14px'> " + resp_t[0].trim() + " </span></br>");
                                 $('.k-dialog-titlebar').remove();   

                            }

                        }
                    })
               } else {
                   html = '' 
                   for (var i = 1; i <= valida.length; i++)
                        html+= "<span style='color:red;font-size:14px'>"+i+"- "+valida[i-1]+"</span></br>"
                   
                   kendo.alert(html);
                   $('.k-dialog-titlebar').remove();
               } 

        });

        function cerrar_window(id_reser_equipo) {
           $('#ventana_').data("kendoWindow").close()
        }

        function terminar_reserva(id_reser_equipo){
           $.post( url + 'terminar-reserva' , { id_reser_equipo : id_reser_equipo }, function(data, textStatus, xhr) {
               $('#ventana_').data("kendoWindow").close()  
               $('li[class="active"]').click();
           });
        }

        function delete_reserva(id_reser_equipo) {
           $.post( url + 'delete-reserva', { id_reser_equipo : id_reser_equipo }, function(data, textStatus, xhr) {
              $('#ventana_').data("kendoWindow").close()  
              $('li[class="active"]').click();
           })
        }

        function devolucion_equipo(id_reser_equipo) {
           $.post( url + 'devolucion-reserva', { id_reser_equipo : id_reser_equipo }, function(data, textStatus, xhr) {
              $('#ventana_').data("kendoWindow").close()  
              $('li[class="active"]').click();
           })
        }

    </script>