
<div class="row">
    <div class="col-lg-12 col-ml-12 top_div">
    	<div class="card" id='reportes'>
    		<div class="card-body">
	            <div class="row">
                    <div class="col-2">

                        <div class="form-group">
                            <label class="col-form-label">Función<span style="color: red"> (*) </span></label>
                            <select class="custom-select" id='rep_funcion'>
                                <option value='nada'>Seleccione</option>
                                <option value='grafico'> Grafico </option>
                                <option value='exportar'> Excel </option>
                            </select>
                        </div>

                        <div class="form-group" id='r_de_d'>
                            <label class="col-form-label">Reporte de <span style="color: red"> (*) </span></label>
                            <select class="custom-select" id='r_de'>
                                <option value=''>Seleccione Tipo</option>
                                <option value='productividad' func='r_productividad'>Productividad</option>
                                <option value='sub_tipo' func='r_subtipo'>Subtipo</option>
                                <option value='solucion' func='r_solucion'>Solución</option>
                                <option value='solucion' func='r_calificacion'>Calificación</option>
                                <option value='solucion' func='r_creacion_tic'>Creación de Ticket</option>
                                <option value='solucion' func='r_solucion_tic'>Solución de Ticket</option>
                            </select>
                        </div>

                        <div class="form-group" id='tip_reporte_d'>
                            <label class="col-form-label">Tipo de reporte <span style="color: red"> (*) </span></label>
                            <select class="custom-select" id='tip_reporte'>
                                <option value=''>Seleccione Tipo</option>
                                <option value='gerencial'> Gerencial </option>
                                <option value='jefatura'> Jefatura </option>
                            </select>
                        </div>

                    	<div class="form-group" id='fecha_desde_d'>
                            <label for="des_usuario" class="col-form-label">Fecha desde :<span style="color: red"> (*) </span></label>
                            <input class="form-control fec_reporte" id="fecha_desde" value="<?php echo date("Y-m-d") ?>">
                        </div>
                        <div class="form-group" id='fecha_hasta_d'>
                            <label for="des_usuario" class="col-form-label">Fecha Hasta :<span style="color: red"> (*) </span></label>
                            <input class="form-control fec_reporte" id="fecha_hasta" value="<?php echo date("Y-m-d") ?>">
                        </div>
                        <center>
                            <button type="button" class="btn btn-outline-primary mb-3" onclick="r()" id='r_d'>Gráfico</button>
                        </center>
                    </div>
                    <div class="col-10">
                        <div id='cesta_reporte_____'></div>
                    </div>
	            </div>
	        </div>    
        </div>
    </div>
</div>

<script type="text/javascript">

	var tpl = '',
	    thead = '',
	    tbody = ''

    $('#r_de_d').hide()
    $('#tip_reporte_d').hide()
    $('#fecha_desde_d').hide()
    $('#fecha_hasta_d').hide()
    $('#r_d').hide()
    
    $('#rep_funcion').change(function(){
        if ( $(this).val() == 'grafico' ) {
             $('#r_de').find('option').eq(3).hide()
             $('#r_de').find('option').eq(5).hide()
             $('#r_de').find('option').eq(6).hide()
             $('#r_d').text('Gráfico')
             $('#r_de_d').show();
        } else if ( $(this).val() == 'exportar' ){
             $('#tip_reporte').find('option').eq(1).hide()
             $('#r_de').find('option').eq(3).show()
             $('#r_de').find('option').eq(5).show()
             $('#r_de').find('option').eq(6).show()
             $('#r_d').text('Exportar excel')
             $('#r_de_d').show()
        } else if ( $(this).val() == 'nada' ) {
             $('#r_de_d').hide()
             $('#tip_reporte_d').hide()
             $('#fecha_desde_d').hide()
             $('#fecha_hasta_d').hide()
             $('#r_d').hide()
        }

    });

    $('#r_de').change(function(){

        if ( $(this).val() == 'solucion' || $(this).val() == 'sub_tipo'  )
             $('#tip_reporte').find('option').eq(1).hide()
        else
             $('#tip_reporte').find('option').eq(1).show()
        
        $('#tip_reporte_d').show()
        $('#fecha_desde_d').show()
        $('#fecha_hasta_d').show()
        $('#r_d').show()
    });    

	$('#reportes').height($(window).height() - 100) 
	$('#reportes').css('overflow-y', 'scroll');
	$(".fec_reporte").kendoDatePicker({ format: "yyyy-MM-dd" });
	$(".fec_reporte").click(function(){
	    $(this).data("kendoDatePicker").open()
	})

    function r(){
        eval($('#r_de option:selected').attr('func')+'()')
    }

    function r_solucion(){
        window.open(url + 'report-rsolucion?op=exportar&fec_ini_cierre='+$('#fecha_desde').val()+'&fec_hasta_cierre='+$('#fecha_hasta').val())
    }

    function r_creacion_tic() {
        window.open(url + 'report-creacion-ticket?op=exportar&fec_ini_cierre='+$('#fecha_desde').val()+'&fec_hasta_cierre='+$('#fecha_hasta').val())
    }

    function r_solucion_tic(){
        window.open(url + 'report-termino-ticket?op=exportar&fec_ini_cierre='+$('#fecha_desde').val()+'&fec_hasta_cierre='+$('#fecha_hasta').val())
    }



    // GRAFICO PRODUCTIVIDAD 
    function r_productividad() {
        
        $.ajax({
            url: url + 'report-productividad',
            type: 'POST',
            dataType: 'json',
            data: {   
                      fec_ini_cierre : $('#fecha_desde').val(),
                      fec_hasta_cierre : $('#fecha_hasta').val(),
                      tip_reporte : $('#tip_reporte option:selected').val()
            },
            success : function(data) {
                $('#cesta_reporte_____').html('<canvas id="canvas"></canvas><hr><canvas id="canvas_2"></canvas>')
                var barChartData = '', barChartData_2 = ''

                if (  $('#tip_reporte option:selected').val() == 'gerencial' ) {
                      barChartData = {
                            labels: data.G_labels,
                            datasets: [{
                                backgroundColor: window.chartColors.red, 
                                label: 'Pendientes',
                                data: data.G_PENDIENTE
                            },{
                                backgroundColor: window.chartColors.blue, 
                                label: 'En Proceso',
                                data: data.G_ENPROCESO
                            },{
                                backgroundColor: window.chartColors.green, 
                                label: 'Terminado',
                                data: data.G_TERMINADOS
                            },{
                                backgroundColor: window.chartColors.orange, 
                                label: 'Pausa',
                                data: data.G_ENPAUSA
                            }]
                      }

                      var ctx = document.getElementById('canvas').getContext('2d');
                      window.myBar = new Chart(ctx, {
                            type: 'bar',
                            data: barChartData,
                            options: {
                                title: {
                                    display: true,
                                    text: 'Reporte de Productividad'
                                },
                                tooltips: {
                                    mode: 'index',
                                    intersect: false
                                },
                                responsive: true,
                                scales: {
                                    xAxes: [{
                                        stacked: true,
                                    }],
                                    yAxes: [{
                                        stacked: true
                                    }]
                                }
                            }
                       })



                } else {

                  barChartData = {
                        labels: data.S_labels,
                        datasets: [{
                            backgroundColor: window.chartColors.red, 
                            label: 'Pendientes',
                            data: data.I_PENDIENTE,
                        },{
                            backgroundColor: window.chartColors.blue, 
                            label: 'En Proceso',
                            data: data.I_ENPROCESO
                        },{
                            backgroundColor: window.chartColors.green, 
                            label: 'Terminado',
                            data: data.I_TERMINADOS
                        },{
                            backgroundColor: window.chartColors.orange, 
                            label: 'Pausa',
                            data: data.I_ENPAUSA
                        }]
                  }

                  var ctx = document.getElementById('canvas').getContext('2d');
                  window.myBar = new Chart(ctx, {
                        type: 'bar',
                        data: barChartData,
                        options: {
                            title: {
                                display: true,
                                text: 'Incidencia - Reporte de Productividad '
                            },
                            tooltips: {
                                mode: 'index',
                                intersect: false
                            },
                            responsive: true,
                            scales: {
                                xAxes: [{
                                    stacked: true,
                                }],
                                yAxes: [{
                                    stacked: true
                                }]
                            }
                        }
                   })

                   barChartData_2 = {
                        labels: data.S_labels,
                        datasets: [{
                            backgroundColor: window.chartColors.red, 
                            label: 'Pendientes',
                            data: data.S_PENDIENTE,
                        },{
                            backgroundColor: window.chartColors.blue, 
                            label: 'En Proceso',
                            data: data.S_ENPROCESO
                        },{
                            backgroundColor: window.chartColors.green, 
                            label: 'Terminado',
                            data: data.S_TERMINADOS
                        },{
                            backgroundColor: window.chartColors.orange, 
                            label: 'Pausa',
                            data: data.S_ENPAUSA
                        }]
                  }

                  var ctx = document.getElementById('canvas_2').getContext('2d');
                  window.myBar = new Chart(ctx, {
                        type: 'bar',
                        data: barChartData_2,
                        options: {
                            title: {
                                display: true,
                                text: 'Solicitud - Reporte de Productividad '
                            },
                            tooltips: {
                                mode: 'index',
                                intersect: false
                            },
                            responsive: true,
                            scales: {
                                xAxes: [{
                                    stacked: true,
                                }],
                                yAxes: [{
                                    stacked: true
                                }]
                            }
                        }
                   }) 
                }   
            } 
        })
    }

    function r_calificacion() {
        window.open(url + 'Requerimiento_C/report_calificacion?fec_ini_cierre='+$('#fecha_desde').val()+'&fec_hasta_cierre='+$('#fecha_hasta').val())
    }

    function r_subtipo(){


        if ( $('#rep_funcion option:selected').val() == 'grafico' ) { 

            $.ajax({
                url: url + 'report-subtipo?op=grafico',
                type: 'POST',
                dataType: 'json',
                data: {   
                          fec_ini_cierre : $('#fecha_desde').val(),
                          fec_hasta_cierre : $('#fecha_hasta').val(),
                          tip_reporte : $('#tip_reporte option:selected').val()
                },
                success : function(data) {
                    var color = [],config,
                        color_ = function() {
                            return "rgb(" + Math.floor(Math.random() * 255) + "," + Math.floor(Math.random() * 255) + "," + Math.floor(Math.random() * 255) + ")";
                        }

                        color.length = 0
                        $('#cesta_reporte_____').html('<canvas id="canvas"></canvas><hr><canvas id="canvas_2"></canvas>')
                        for (var i = data.label_incidencia.length - 1; i >= 0; i--)
                            color.push(color_())

                        config = {
                                    type: 'pie',
                                    data: {
                                        datasets: [{
                                            data: data.data_incidencia,
                                            backgroundColor:color,
                                            label: 'Incidencia'
                                        }],
                                        labels: data.label_incidencia
                                    },options: {
                                        responsive: true,
                                        title: {
                                            display: true,
                                            text: 'Incidencia - Total por Subtipo'
                                        },
                                    }
                                };
                        var ctx = document.getElementById('canvas').getContext('2d');
                        window.myPie = new Chart(ctx, config);

                        color.length = 0;

                        for (var i = data.label_incidencia.length - 1; i >= 0; i--)
                            color.push(color_())

                        config= {

                                    type: 'pie',
                                    data: {
                                        datasets: [{
                                            data: data.data_solicitudes,
                                            backgroundColor:color,
                                            label: 'Solicitud'
                                        }],
                                        labels: data.label_solicitud
                                    },options: {
                                        responsive: true,
                                        title: {
                                            display: true,
                                            text: 'Solicitud - Total por Subtipo'
                                        },
                                    }
                                }
                        var ctx2 = document.getElementById('canvas_2').getContext('2d')
                        window.myPie2 = new Chart(ctx2, config)
                } 
            })
        } else if ( $('#rep_funcion option:selected').val() == 'exportar' ) {
            window.open(url + 'report-subtipo?op=exportar&fec_ini_cierre='+$('#fecha_desde').val()+'&fec_hasta_cierre='+$('#fecha_hasta').val())
        }

    }

</script>



