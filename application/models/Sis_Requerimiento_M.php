<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sis_Requerimiento_M extends CI_Model {

	public $usuario;
	public $password;
	public $id_detalle_requerimiento;
	public $id_tip_requerimiento;
	public $name_table;
    public $id_usuario;
    public $id_area;
    public $hora;
    public $op;
    public $mensaje;
    public $est;
    public $req_prioridad;
    public $muestra_tikets;
    public $id_usuario_asig;
    public $id_sub_tip_requerimiento;
    public $fec_ini_cierre;
    public $fec_hasta_cierre;
    public $id_reser_equipo;
    public $fil_fec;


	public function __construct()
	{
		parent::__construct();
	}
    
    function load_tikets_com(){
    	$sql = "SELECT id_detalle_requerimiento AS id , CONCAT(LPAD(id_detalle_requerimiento,6,'0'),'-',titulo_requerimiento) AS des
                FROM detalle_requerimiento WHERE est IN ('1','2','3') AND id_usuario_asig = '".$this->id_usuario."'";
    	return $this->db->query($sql)->result();
    }

	function info_usuario()
	{
		$sql = "SELECT usu.id_usuario,f_insertar_mayusculaMinuscula(usu.des_usuario) AS des_usuario,usu.id_area,des_tip_usu,usu.pass_vis,usu.est_pass FROM usuario usu
                INNER JOIN tip_usuario tip ON usu.id_tip_usu = tip.id_tip_usu
                WHERE usu.est = '1' AND tip.est = '1' AND usu.usuario = '".$this->usuario."' AND usu.pass_inc = MD5('".$this->password."')";
		return $this->db->query($sql)->row_array();	
	}

	function read_mensaje()
	{ 
		$sql = "SELECT des_mensaje FROM mensaje LIMIT 1";
		return $this->db->query($sql)->row();
	}

	function tip_requerimiento()
	{
		$sql = "SELECT id_tip_requerimiento AS id ,des_tip_requerimiento AS des FROM tipo_requerimiento WHERE est = '1'";
		return $this->db->query($sql)->result();
	}

	function sub_tip_requerimiento()
	{
		$sql = "SELECT id_sub_tip_requerimiento AS id, des_sub_tip_requerimiento AS des FROM sub_tipo_requerimiento WHERE est = '1' AND id_tip_requerimiento = '".$this->id_tip_requerimiento."' ORDER BY des_sub_tip_requerimiento ASC";
		return $this->db->query($sql)->result();
	}

	function insert_requerimiento($param)
	{
		$this->db->insert($this->name_table, $param);
		return $this->db->insert_id();
	}

	function load_requerimientos_m()
	{   
		$where = "";
		if ( $this->op == 'normal' ){
			if ( $this->id_tip_requerimiento == 'todos' ) 
	             $where = "";
	        else
	        	 $where = " AND sub.id_tip_requerimiento = '" .$this->id_tip_requerimiento."'";

	        if ( $this->est == 'todos' )
	             $where.= "";
	        else
	             $where.= " AND req.est = '" . $this->est."'";

	        if ( $this->muestra_tikets == 'todos' ) 
	        	 $where.= " AND req.id_usuario IN ( SELECT id_usuario FROM usuario WHERE id_area = '". $this->session->userdata('id_area') ."' )";
	        else
	        	 $where.= " AND req.id_usuario = '" . $this->id_usuario."'";

			$sql = "SELECT LPAD(req.id_detalle_requerimiento,6,'0') AS id_detalle_requerimiento,req.titulo_requerimiento,
					CASE req.req_prioridad WHEN '1' THEN 'Alta' WHEN '2' THEN 'Media' WHEN '3' THEN 'Baja' END AS prioridad,
					CASE  sub.id_tip_requerimiento WHEN '1' THEN 'Solicitud' WHEN '2' THEN 'Incidencia' END AS tipo_requerimiento,
					CASE req.est WHEN '0' THEN 'Terminado' WHEN '1' THEN 'En Proceso' WHEN '2' THEN 'Pendiente' WHEN '3' THEN 'Pausa' END AS est_des,
					DATE_FORMAT(req.date_requerimiento_apertura, '%d/%m/%Y %H:%i') AS fec_apertura,
                                        DATE_FORMAT(req.date_requerimiento_cierre, '%d/%m/%Y %H:%i') AS fec_cierre,
					IFNULL(f_insertar_mayusculaMinuscula(usu.des_usuario),' ') AS responsable,req.req_prioridad,
					( SELECT f_insertar_mayusculaMinuscula(usu.des_usuario) FROM usuario usu JOIN area_of are ON usu.id_area = are.id_area WHERE usu.id_usuario = req.id_usuario) AS usuario,
					( SELECT f_insertar_mayusculaMinuscula(are.des_area) FROM usuario usu JOIN area_of are ON usu.id_area = are.id_area WHERE usu.id_usuario = req.id_usuario) AS area_usu
					FROM detalle_requerimiento req 
					INNER JOIN sub_tipo_requerimiento sub ON req.id_sub_tip_requerimiento = sub.id_sub_tip_requerimiento
					LEFT JOIN usuario usu ON req.id_usuario_asig = usu.id_usuario WHERE 1=1
					 " .$where . " GROUP BY req.titulo_requerimiento,req.desc_requerimiento ORDER BY req.id_detalle_requerimiento DESC";					 

		} else {

			if ( $this->est == 'todos' )
	             $where.= "";
	        else
	             $where.= " AND req.est IN(".$this->est.")";


	        if ( $this->fil_fec == '1' )
	        	 $where.= " AND DATE_FORMAT(req.date_requerimiento_apertura, '%Y-%m-%d') BETWEEN  '".$this->fec_ini_cierre."' AND '".$this->fec_hasta_cierre."'";
	        else
                 $where.= "";
	       
			$sql = "SELECT LPAD(req.id_detalle_requerimiento,6,'0') AS id_detalle_requerimiento,req.titulo_requerimiento,
					CASE req.req_prioridad WHEN '1' THEN 'Alta' WHEN '2' THEN 'Media' WHEN '3' THEN 'Baja' END AS prioridad,
                                        req.path_o_cod_video,
					CASE  sub.id_tip_requerimiento WHEN '1' THEN 'Solicitud' WHEN '2' THEN 'Incidencia' END AS tipo_requerimiento,
                                        CASE req.TA WHEN 'Incumplimiento de SLA' THEN 'Incumplimiento de SLA' WHEN '0' THEN ' ' END AS 'tiempo_atencion',
                                        CASE req.TS WHEN 'Incumplimiento de SLA' THEN 'Incumplimiento de SLA' WHEN '0' THEN ' ' END AS 'tiempo_solucion',
					CASE req.est WHEN '0' THEN 'Terminado' WHEN '1' THEN 'En Proceso' WHEN '2' THEN 'Pendiente' WHEN '3' THEN 'Pausa' END AS est_des,
					DATE_FORMAT(req.date_requerimiento_apertura, '%d/%m/%Y %H:%i') AS fec_apertura, 
					IFNULL(f_insertar_mayusculaMinuscula(usu.des_usuario),' ') AS responsable,req.req_prioridad,
					( SELECT f_insertar_mayusculaMinuscula(usu.des_usuario) FROM usuario usu JOIN area_of are ON usu.id_area = are.id_area WHERE usu.id_usuario = req.id_usuario) AS usuario,
					( SELECT f_insertar_mayusculaMinuscula(are.des_area) FROM usuario usu JOIN area_of are ON usu.id_area = are.id_area WHERE usu.id_usuario = req.id_usuario) AS area_usu,
                                        IFNULL(cal.des_tip_calificacion,'-') AS des_tip_calificacion
					FROM detalle_requerimiento req 
					INNER JOIN sub_tipo_requerimiento sub ON req.id_sub_tip_requerimiento = sub.id_sub_tip_requerimiento
                                        LEFT JOIN tip_calificacion_requerimiento cal ON req.id_tip_calificacion = cal.id_tip_calificacion
					LEFT JOIN usuario usu ON req.id_usuario_asig = usu.id_usuario WHERE 1=1 " . $where . " GROUP BY req.titulo_requerimiento,req.desc_requerimiento ORDER BY req.id_detalle_requerimiento DESC";

		}
		
		return $this->db->query($sql)->result();

	}

        function load_requerimientos_m_sistemas()
	{   
		$where = "";
		if ( $this->op == 'normal' ){
			if ( $this->id_tip_requerimiento == 'todos' ) 
	             $where = "";
	        else
	        	 $where = " AND sub.id_tip_requerimiento = '" .$this->id_tip_requerimiento."'";

	        if ( $this->est == 'todos' )
	             $where.= "";
	        else
	             $where.= " AND req.est = '" . $this->est."'";

	        if ( $this->muestra_tikets == 'todos' ) 
	        	 $where.= " AND req.id_usuario_reg IN ( SELECT id_usuario FROM usuario WHERE id_area = '". $this->session->userdata('id_area') ."' )";
	        else
	        	 $where.= " AND req.id_usuario_reg = '" . $this->id_usuario."'";

			$sql = "SELECT LPAD(req.id_detalle_requerimiento,6,'0') AS id_detalle_requerimiento,req.titulo_requerimiento,
					CASE req.req_prioridad WHEN '1' THEN 'Alta' WHEN '2' THEN 'Media' WHEN '3' THEN 'Baja' END AS prioridad,
					CASE  sub.id_tip_requerimiento WHEN '1' THEN 'Solicitud' WHEN '2' THEN 'Incidencia' END AS tipo_requerimiento,
					CASE req.est WHEN '0' THEN 'Terminado' WHEN '1' THEN 'En Proceso' WHEN '2' THEN 'Pendiente' WHEN '3' THEN 'Pausa' END AS est_des,
					DATE_FORMAT(req.date_requerimiento_apertura, '%d/%m/%Y %H:%i') AS fec_apertura,
                                        DATE_FORMAT(req.date_requerimiento_cierre, '%d/%m/%Y %H:%i') AS fec_cierre,
					IFNULL(f_insertar_mayusculaMinuscula(usu.des_usuario),' ') AS responsable,req.req_prioridad,
					( SELECT f_insertar_mayusculaMinuscula(usu.des_usuario) FROM usuario usu JOIN area_of are ON usu.id_area = are.id_area WHERE usu.id_usuario = req.id_usuario) AS usuario,
					( SELECT f_insertar_mayusculaMinuscula(are.des_area) FROM usuario usu JOIN area_of are ON usu.id_area = are.id_area WHERE usu.id_usuario = req.id_usuario) AS area_usu
					FROM detalle_requerimiento req 
					INNER JOIN sub_tipo_requerimiento sub ON req.id_sub_tip_requerimiento = sub.id_sub_tip_requerimiento
					LEFT JOIN usuario usu ON req.id_usuario_asig = usu.id_usuario WHERE 1=1
					 " .$where . " GROUP BY req.desc_requerimiento ORDER BY req.id_detalle_requerimiento DESC";					 

		} else {

			if ( $this->est == 'todos' )
	             $where.= "";
	        else
	             $where.= " AND req.est IN(".$this->est.")";


	        if ( $this->fil_fec == '1' )
	        	 $where.= " AND DATE_FORMAT(req.date_requerimiento_apertura, '%Y-%m-%d') BETWEEN  '".$this->fec_ini_cierre."' AND '".$this->fec_hasta_cierre."'";
	        else
                 $where.= "";
	       
			$sql = "SELECT LPAD(req.id_detalle_requerimiento,6,'0') AS id_detalle_requerimiento,req.titulo_requerimiento,
					CASE req.req_prioridad WHEN '1' THEN 'Alta' WHEN '2' THEN 'Media' WHEN '3' THEN 'Baja' END AS prioridad,
                                        req.path_o_cod_video,
					CASE  sub.id_tip_requerimiento WHEN '1' THEN 'Solicitud' WHEN '2' THEN 'Incidencia' END AS tipo_requerimiento,
                                        CASE req.TA WHEN 'Incumplimiento de SLA' THEN 'Incumplimiento de SLA' WHEN '0' THEN ' ' END AS 'tiempo_atencion',
                                        CASE req.TS WHEN 'Incumplimiento de SLA' THEN 'Incumplimiento de SLA' WHEN '0' THEN ' ' END AS 'tiempo_solucion',
					CASE req.est WHEN '0' THEN 'Terminado' WHEN '1' THEN 'En Proceso' WHEN '2' THEN 'Pendiente' WHEN '3' THEN 'Pausa' END AS est_des,
					DATE_FORMAT(req.date_requerimiento_apertura, '%d/%m/%Y %H:%i') AS fec_apertura, 
					IFNULL(f_insertar_mayusculaMinuscula(usu.des_usuario),' ') AS responsable,req.req_prioridad,
					( SELECT f_insertar_mayusculaMinuscula(usu.des_usuario) FROM usuario usu JOIN area_of are ON usu.id_area = are.id_area WHERE usu.id_usuario = req.id_usuario) AS usuario,
					( SELECT f_insertar_mayusculaMinuscula(are.des_area) FROM usuario usu JOIN area_of are ON usu.id_area = are.id_area WHERE usu.id_usuario = req.id_usuario) AS area_usu,
                                        IFNULL(cal.des_tip_calificacion,'-') AS des_tip_calificacion
					FROM detalle_requerimiento req 
					INNER JOIN sub_tipo_requerimiento sub ON req.id_sub_tip_requerimiento = sub.id_sub_tip_requerimiento
                                        LEFT JOIN tip_calificacion_requerimiento cal ON req.id_tip_calificacion = cal.id_tip_calificacion
					LEFT JOIN usuario usu ON req.id_usuario_asig = usu.id_usuario WHERE 1=1 " . $where . " GROUP BY req.desc_requerimiento ORDER BY req.id_detalle_requerimiento DESC";

		}
		
		return $this->db->query($sql)->result();

	}

	function detalle_requerimiento()
	{
		$sql = "SELECT LPAD(req.id_detalle_requerimiento,6,'0') AS id_detalle_requerimiento,
				req.titulo_requerimiento,req.obj_requerimiento,
				CASE req.req_prioridad WHEN '1' THEN 'Alta' WHEN '2' THEN 'Media' WHEN '3' THEN 'Baja' END AS prioridad,
				req.desc_requerimiento,
				CASE  sub.id_tip_requerimiento WHEN '1' THEN 'Solicitud' WHEN '2' THEN 'Incidencia' END AS tipo_requerimiento,
				CASE req.est WHEN '0' THEN 'Terminado' WHEN '1' THEN 'En Proceso' WHEN '2' THEN 'Pendiente' WHEN '3' THEN 'Pausa' END AS est_des,
				DATE_FORMAT(req.date_requerimiento_apertura, '%d/%m/%Y %H:%i') AS fec_apertura,
				DATE_FORMAT(req.fec_est_termino, '%d/%m/%Y %H:%i') AS fec_est_termino,
				DATE_FORMAT(req.fec_est_sistmas, '%d/%m/%Y %H:%i') AS fec_est_sistmas,
				req.req_prioridad,
				IFNULL(req.id_usuario_asig,'0') AS id_usuario_asig,
				req.path_adjunto,sub.id_tip_requerimiento,req.est,req.req_prioridad,
				IFNULL(f_insertar_mayusculaMinuscula(usu.des_usuario),' ') AS responsable,
                                ( SELECT f_insertar_mayusculaMinuscula(des_usuario) FROM usuario WHERE id_usuario = req.id_usuario) AS usuario,
                                ( SELECT f_insertar_mayusculaMinuscula(are.des_area) FROM usuario usu JOIN area_of are ON usu.id_area = are.id_area WHERE usu.id_usuario = req.id_usuario) AS area_usu,
				req.id_usuario
				FROM detalle_requerimiento req 
				INNER JOIN sub_tipo_requerimiento sub ON req.id_sub_tip_requerimiento = sub.id_sub_tip_requerimiento
				LEFT JOIN usuario usu ON req.id_usuario_asig = usu.id_usuario
				WHERE req.id_detalle_requerimiento = '".$this->id_detalle_requerimiento."'";
		return $this->db->query($sql)->row();
	}

	function load_historial_requerimiento()
	{
		$sql = "SELECT his.id_hist_req,his.comet_hist_req,DATE_FORMAT(his.fec_hist_req, '%d/%m/%Y %H:%i') AS fec_reg,his.id_usuario,f_insertar_mayusculaMinuscula(usu.des_usuario) AS des_usuario FROM 
		        hist_requerimiento his INNER JOIN usuario usu ON his.id_usuario = usu.id_usuario
                WHERE his.est = '1' AND his.id_detalle_requerimiento = '".$this->id_detalle_requerimiento."'";
        return $this->db->query($sql)->result();
	}


	function load_area()
	{
		$sql = "SELECT id_area AS id, des_area AS des FROM area_of WHERE est = '1'";
		return $this->db->query($sql)->result();
	}

	function load_tipo_usuario()
	{
		$sql = "SELECT id_tip_usu as id, des_tip_usu as des FROM tip_usuario";
		return $this->db->query($sql)->result();
	}

	function load_user()
	{
		$sql = "SELECT usu.id_usuario,f_insertar_mayusculaMinuscula(usu.des_usuario) AS des_usuario,usu.email_usuario,usu.usuario,usu.pass_vis,f_insertar_mayusculaMinuscula(are.des_area) as des_area,tip.des_tip_usu from usuario usu
				INNER JOIN tip_usuario tip ON usu.id_tip_usu = tip.id_tip_usu
				INNER JOIN area_of are ON usu.id_area = are.id_area WHERE  usu.est = '1' AND are.est = '1'";
		return $this->db->query($sql)->result();		
	}

	function delete_usuario()
	{
		$sql = "UPDATE usuario SET est = '0' WHERE id_usuario = '".$this->id_usuario."'";
		$this->db->query($sql);
	}

    function detalle_usuario()
    {
    	$sql = "SELECT usu.id_usuario,usu.des_usuario AS des_usuario,usu.email_usuario,usu.usuario,usu.pass_vis,are.des_area,are.id_area,tip.id_tip_usu,tip.des_tip_usu,usu.est_pass from usuario usu
				INNER JOIN tip_usuario tip ON usu.id_tip_usu = tip.id_tip_usu
				INNER JOIN area_of are ON usu.id_area = are.id_area WHERE usu.id_usuario = '".$this->id_usuario."'";
		return $this->db->query($sql)->row();
    }

    function detalle_area()
    {
    	$sql = "SELECT id_area AS id, des_area AS des FROM area_of WHERE id_area = '".$this->id_area."'";
    	return $this->db->query($sql)->row();
    }
    
    function update($param,$where)
    {
		$this->db->update($this->name_table, $param, $where);
    }

    function list_sist()
    { 
    	$sql = "SELECT id_usuario AS id, des_usuario AS des FROM usuario WHERE id_tip_usu IN('2','3') AND id_area = '4' AND est = '1'";
    	return $this->db->query($sql)->result();
    }


    function list_reservas()
    {
        $sql = "SELECT CONCAT(f_insertar_mayusculaMinuscula(usu.des_usuario),'-',eq.des_equipo,'-',res.res_motivo ) AS title,CONCAT(res.fecha,'T',res.fec_desde) AS 'start', CONCAT(res.fecha,'T',res.fec_hasta) AS 'end', 
                CASE eq.id_equipo WHEN 1 THEN '#fff59d' WHEN 2 THEN '#2196f3' WHEN 3 THEN '#ff9800' END  as backgroundColor,res.id_reser_equipo  as id FROM  reserva_equipo res
				INNER JOIN equipo eq ON res.id_equipo = eq.id_equipo
				INNER JOIN usuario usu ON res.id_usuario = usu.id_usuario
				WHERE res.est = '1'
				UNION
				SELECT CONCAT(f_insertar_mayusculaMinuscula(usu.des_usuario),'-',eq.des_equipo,'-',res.res_motivo ) AS title,CONCAT(res.fecha,'T',res.fec_desde) AS 'start', CONCAT(res.fecha,'T',res.fec_hasta) AS 'end', 
                '#9e9e9e' as backgroundColor,res.id_reser_equipo  as id FROM  reserva_equipo res
				INNER JOIN equipo eq ON res.id_equipo = eq.id_equipo
				INNER JOIN usuario usu ON res.id_usuario = usu.id_usuario
				WHERE res.est = '2'
				";
        return $this->db->query($sql)->result();
    }

    function validar_reserva($id_equipo,$hora,$fecha,$ini_o_fin)
    {
    	$sql = ( $ini_o_fin == 'inicio' ) ? "SELECT * FROM reserva_equipo WHERE id_usuario != '0' AND  est = '1' AND id_equipo = '".$id_equipo."' AND 
    	 fecha = '".$fecha."' AND '".$hora."' BETWEEN fec_desde AND SUBTIME(fec_hasta,'00:01:00')" : "SELECT * FROM reserva_equipo WHERE est = '1' AND id_equipo = '".$id_equipo."' AND 
    	 fecha = '".$fecha."' AND '".$hora."' BETWEEN ADDTIME(fec_desde,'00:01:00') AND fec_hasta";
    	return $this->db->query($sql)->row();
    }

    function delete_area()
	{
		$sql = "UPDATE area_of SET est = '0' WHERE id_area = '".$this->id_area."'";
		$this->db->query($sql);
	}
    
    function list_data_reporte($id_sub_tip_requerimiento,$date_requerimiento_cierre)
    {
        $sql = "SELECT IF(COUNT(*) > 0, COUNT(*), 0) AS total  FROM detalle_requerimiento WHERE  
				id_sub_tip_requerimiento = '".$id_sub_tip_requerimiento."' AND  id_usuario_asig = '".$this->id_usuario_asig."' 
				AND est='0' AND DATE_FORMAT(date_requerimiento_cierre,'%Y-%m-%d') = '".$date_requerimiento_cierre."'";
		$return = $this->db->query($sql)->row();
		return $return->total;
    }

    function select_tipo()
    {
    	$sql = "SELECT * FROM tipo_requerimiento WHERE est = '1'";
    	return $this->db->query($sql)->result();
    }

    function select_sub_tipo_()
    {
    	$sql = "SELECT * FROM sub_tipo_requerimiento WHERE id_tip_requerimiento = '".$this->id_tip_requerimiento."'";
    	return $this->db->query($sql)->result();
    }

    /**** reportes *****/
       
    function r_read_usuario()
    {
        $sql = "SELECT id_usuario AS id , f_insertar_mayusculaMinuscula(des_usuario) AS des FROM usuario WHERE est = '1' AND id_tip_usu = '2'";
        return $this->db->query($sql)->result();
    }

    /******************/
    
    /*****************/
    
    function read_det_reserva()
    {
    	$sql =  "SELECT res.est,eq.des_equipo,res.fec_desde,res.fec_hasta,res.id_reser_equipo,res.est_alert,f_insertar_mayusculaMinuscula(usu.des_usuario) AS des_usuario,res.res_motivo,res.fecha,res.id_usuario,res.est_devolucion FROM  reserva_equipo res
				INNER JOIN equipo eq ON res.id_equipo = eq.id_equipo
				INNER JOIN usuario usu ON res.id_usuario = usu.id_usuario
				WHERE res.est IN ('1','2') AND res.id_reser_equipo = '".$this->id_reser_equipo."'";
		return $this->db->query($sql)->row();		
    }

    /****************/

    /****************/
    function load_sub_requerimientos()
    {
        $sql = "SELECT * FROM sub_tipo_requerimiento sub
				 JOIN tipo_requerimiento tip ON tip.id_tip_requerimiento = sub.id_tip_requerimiento
				 WHERE sub.est = '1'";
        return $this->db->query($sql)->result();
    }
    /***************/

 /****************** SLA ******************/

    function read_sla()
    {
        if ( $this->tip_sla_des == '1' )
             $sql = "SELECT * FROM detalle_sla WHERE est='1' AND tip_sla_des = '1' LIMIT 1";
        else
        	 $sql = "SELECT * FROM detalle_sla WHERE est='1' AND tip_sla_des = '2' AND id_tip_requerimiento = '".$this->id_tip_requerimiento."' AND id_sub_tip_requerimiento = '".$this->id_sub_tip_requerimiento."' LIMIT 1";
    	return $this->db->query($sql)->row();
    }
  
    /*****************************************/

    /********* alerta proyector **************/


    function alert_reserva()
    {
    	$sql = "SELECT CASE res.id_equipo WHEN 1 THEN 'Proyector' WHEN 2 THEN 'Carpa azul' WHEN 3 THEN 'Proyector y Carpa azul' END AS equipo ,res.fec_desde,
res.fec_hasta, res.res_motivo, f_insertar_mayusculaMinuscula(usu.des_usuario) AS usuario,
CONCAT('RESERVA',' ',CASE res.id_equipo WHEN 1 THEN 'PROYECTOR' WHEN 2 THEN 'CARPA AZUL' WHEN 3 THEN 'PROYECTOR Y CARPA AZUL' END ) AS titulo
FROM reserva_equipo res JOIN usuario usu ON res.id_usuario = usu.id_usuario
WHERE res.fecha = CURDATE() AND res.est_alert = '1'AND SUBTIME(res.fec_desde, '00:05:00') <= TIME (NOW())
AND res.fec_desde >= TIME (NOW()) AND res.est = '1' AND res.id_equipo != 2
UNION
SELECT CASE ac_spt_m WHEN 1 THEN 'Servidor' WHEN 2 THEN 'Desktop' END AS equipo, hor_ini_spt_m AS fec_desde,hor_fin_spt_m AS fec_hasta,
motivo_spt_m AS res_motivo,responsable_spt_m AS usuario,CONCAT('RESERVA',' ',CASE ac_spt_m WHEN 1 THEN 'SERVIDOR' WHEN 2 THEN 'DESKTOP' END ) AS titulo
FROM tbl_spt_m
WHERE est_aten_spt_m = '0' AND est_spt_m = '1' AND fec_spt_m = CURDATE()AND SUBTIME(hor_ini_spt_m, '00:05:00') <= TIME (NOW())
AND hor_fin_spt_m >= TIME (NOW())";
    	return $this->db->query($sql)->row();
    }

 
    /********************************** REPORTES **********************************************/
 
    /********* REPORTE GRAFICO ***************/

    function report_productividad()
    {   
        $sql = "";

        if ( $this->tip_reporte == 'gerencial' ) {
            $sql = "SELECT 
					COUNT(CASE rq.est WHEN '0' THEN 1 END) AS 'TERMINADOS', 
					COUNT(CASE rq.est WHEN '1' THEN 1 END) AS 'ENPROCESO',
					COUNT(CASE rq.est WHEN '2' THEN 1 END) AS 'PENDIENTE',
					COUNT(CASE rq.est WHEN '3' THEN 1 END) AS 'ENPAUSA',
					'gerencia'AS tipo_requerimiento,
					CASE sub.id_tip_requerimiento WHEN '1' THEN 'SOLICITUD' WHEN '2' THEN 'INCIDENCIA' END AS USUARIO
					FROM detalle_requerimiento rq
					JOIN sub_tipo_requerimiento sub ON rq.id_sub_tip_requerimiento = sub.id_sub_tip_requerimiento
					WHERE DATE_FORMAT(rq.date_requerimiento_apertura, '%Y-%m-%d') BETWEEN '". $this->fec_ini_cierre ."' AND '" . $this->fec_hasta_cierre."' GROUP BY sub.id_tip_requerimiento";
        } else {
            $sql = "SELECT  
					COUNT(CASE rq.est WHEN '0' THEN 1 END) AS 'TERMINADOS',
					COUNT(CASE rq.est WHEN '1' THEN 1 END) AS 'ENPROCESO',
					COUNT(CASE rq.est WHEN '2' THEN 1 END) AS 'PENDIENTE',
					COUNT(CASE rq.est WHEN '3' THEN 1 END) AS 'ENPAUSA',
					IFNULL(usu.des_usuario,'Vacio') AS 'USUARIO',
					CASE  sub.id_tip_requerimiento WHEN '1' THEN 'Solicitud' WHEN '2' THEN 'Insidencia' END AS tipo_requerimiento,
					rq.id_usuario_asig
					FROM detalle_requerimiento rq
					JOIN sub_tipo_requerimiento sub ON rq.id_sub_tip_requerimiento = sub.id_sub_tip_requerimiento
					LEFT JOIN usuario usu ON rq.id_usuario_asig = usu.id_usuario
					WHERE id_tip_usu = '2' AND DATE_FORMAT(date_requerimiento_apertura, '%Y-%m-%d') BETWEEN '". $this->fec_ini_cierre ."' AND '" . $this->fec_hasta_cierre."' GROUP BY sub.id_tip_requerimiento,USUARIO";
        }

    	return $this->db->query($sql)->result();

    }

    function report_subtipo_exportar()
    {
    	$sql = "SELECT *,
				CASE det.est WHEN '0' THEN 'Terminado' WHEN '1' THEN 'En Proceso' WHEN '2' THEN 'Pendiente' WHEN '3' THEN 'Pausa' END AS est_des,
				CASE sub.id_tip_requerimiento WHEN '1' THEN 'SOLICITUD' WHEN '2' THEN 'INCIDENCIA' END AS tipo_requerimiento,
				CASE det.req_prioridad WHEN '1' THEN 'Alta' WHEN '2' THEN 'Media' WHEN '3' THEN 'Baja' END AS prioridad,
				(SELECT des_usuario FROM usuario WHERE id_usuario = det.id_usuario ) AS 'usuario',
                (SELECT are.des_area FROM usuario usu JOIN area_of are ON usu.id_area = are.id_area WHERE usu.id_usuario = det.id_usuario ) AS 'area'
				FROM detalle_requerimiento det
				JOIN sub_tipo_requerimiento sub ON det.id_sub_tip_requerimiento = sub.id_sub_tip_requerimiento
				JOIN usuario usu ON usu.id_usuario = det.id_usuario_asig
				WHERE DATE_FORMAT(det.date_requerimiento_apertura, '%Y-%m-%d') BETWEEN '". $this->fec_ini_cierre ."' AND '" . $this->fec_hasta_cierre."'";
        return $this->db->query($sql)->result_array();  
    }

    function report_subtipo()
    {
        $sql = "SELECT COUNT(sub.id_sub_tip_requerimiento) AS TOTAL,sub.des_sub_tip_requerimiento,
				CASE  sub.id_tip_requerimiento WHEN '1' THEN 'Solicitud' WHEN '2' THEN 'Incidencia' END AS tipo_requerimiento
				FROM detalle_requerimiento det
				JOIN sub_tipo_requerimiento sub ON det.id_sub_tip_requerimiento = sub.id_sub_tip_requerimiento
				WHERE DATE_FORMAT(det.date_requerimiento_apertura, '%Y-%m-%d') BETWEEN '". $this->fec_ini_cierre ."' AND '" . $this->fec_hasta_cierre."'
				GROUP BY sub.id_sub_tip_requerimiento,sub.id_tip_requerimiento";
        return $this->db->query($sql)->result();
    }
/************ reporte de solucion *********/

    function usuarios_sistemas () {
        $sql = "SELECT f_insertar_mayusculaMinuscula(des_usuario) AS des_usuario, id_usuario FROM usuario WHERE est = '1' AND id_tip_usu = '2'";
        return $this->db->query($sql)->result();
    }

    function requerimientos_terminados()
    {
    	$sql = "SELECT LPAD(det.id_detalle_requerimiento,6,'0') AS id_detalle_requerimiento,det.titulo_requerimiento,det.TTR,sub.des_sub_tip_requerimiento,
				CASE sub.id_tip_requerimiento WHEN '1' THEN 'SOLICITUD' WHEN '2' THEN 'INCIDENCIA' END AS tipo_requerimiento,
				CASE det.req_prioridad WHEN '1' THEN 'Alta' WHEN '2' THEN 'Media' WHEN '3' THEN 'Baja' END AS prioridad,
				DATE_FORMAT(det.date_requerimiento_apertura, '%d/%m/%Y %H:%i') AS fec_apertura,
                DATE_FORMAT(det.date_requerimiento_cierre, '%d/%m/%Y %H:%i') AS fec_cierre,
                f_insertar_mayusculaMinuscula(usu.des_usuario) as des_usuario,
                f_insertar_mayusculaMinuscula(are.des_area) as des_area,
                IFNULL(cal.des_tip_calificacion,'-') as calificacion
				FROM detalle_requerimiento det
				JOIN sub_tipo_requerimiento sub ON det.id_sub_tip_requerimiento = sub.id_sub_tip_requerimiento
				JOIN usuario usu ON usu.id_usuario = det.id_usuario
				JOIN area_of are ON usu.id_area = are.id_area
				LEFT JOIN tip_calificacion_requerimiento cal ON det.id_tip_calificacion = cal.id_tip_calificacion
				WHERE det.id_usuario_asig = '".$this->id_usuario_asig."' AND DATE_FORMAT(det.fec_est_termino,'%Y-%m-%d') BETWEEN '". $this->fec_ini_cierre."' AND '".$this->fec_hasta_cierre."' AND det.est = '0'";
    	return $this->db->query($sql)->result();
    }

    /*****************************************/

    /*************** HORARIOS ****************/
    function horario_usuario()
    {
    	$sql = "SELECT * FROM horario_usuario WHERE id_usuario = '".$this->id_usuario."'";
    	return $this->db->query($sql)->result();
    }
    /*****************************************/   

    /*---------------------------------
    -----------------------------------
    ------CONFIGURACION DE ------------
    -----------RESERVA-----------------
    -----------------------------------
    ---------------------------------*/

    function read_data_conf_equipo()
    {
    	$sql = "SELECT * FROM conf_equipo WHERE id_equipo = '" . $this->id_equipo . "'";
    	return $this->db->query($sql)->row();
    }

    function auto_eliminar_reserva()
    { 
        $sql = "UPDATE reserva_equipo SET est = '2' WHERE fecha = CURDATE() AND ADDTIME(fec_desde, '00:10:00') >= TIME(NOW()) 
                AND ADDTIME(fec_desde, '00:10:00') <= ADDTIME(TIME(NOW()), '00:05:00') AND id_equipo != 2 AND est = '1' AND est_alert = '1'";		
        $this->db->query($sql);
    }

    function list_conf_equipo()
    {
    	$sql = "SELECT * FROM conf_equipo WHERE id_equipo = '1' AND est = '1'";
    	return $this->db->query($sql)->row();
    }

    function Count_x_bloqueo_ususario($cons)
    {  
    	 if ( $cons == 1 ) {
             $sql = "SELECT  usu.des_usuario ,COUNT(*) AS total FROM reserva_cont_devoluciones_no_cumplidas cum
					  JOIN usuario usu ON usu.id_usuario = cum.id_usuario
					  WHERE cum.id_usuario = '".$this->id_usuario."'
					  AND cum.est = '1'
					  GROUP BY cum.id_usuario";
			 $return = $this->db->query($sql)->row();
    	 } else {
             $sql = "SELECT usu.id_usuario,usu.des_usuario,are.des_area,COUNT(*) AS total FROM reserva_cont_devoluciones_no_cumplidas cum
					 JOIN usuario usu ON usu.id_usuario = cum.id_usuario
					 JOIN area_of are ON usu.id_area = are.id_area
					 WHERE cum.est = '1'
					 GROUP BY cum.id_usuario";
			 $return = $this->db->query($sql)->result();
    	 }
    	 return $return;
    }

 

    /*---------------------------------
    -----------------------------------
    ------FIN CONFIGURACION DE --------
    -----------RESERVA-----------------
    -----------------------------------
    ---------------------------------*/


   /*---------------------------------
    -----------------------------------
    -------------   CMI    ------------
    -----------------------------------
    -----------------------------------
    ---------------------------------*/


    function graf_productividad_cmi($fec,$tipo,$usuario)
    {   
        $where = ( $usuario == 0 ) ? '' : ' AND rq.id_usuario_asig = '. $usuario;
        $sql = "SELECT COUNT(CASE rq.est WHEN '0' THEN 1 END) AS 'TERMINADOS', COUNT(CASE rq.est WHEN '1' THEN 1 END) AS 'ENPROCESO',COUNT(CASE rq.est WHEN '2' THEN 1 END) AS 'PENDIENTE',COUNT(CASE rq.est WHEN '3' THEN 1 END) AS 'ENPAUSA',DATE_FORMAT(rq.date_requerimiento_apertura, '%Y-%m') AS 'FECHA' FROM detalle_requerimiento rq JOIN sub_tipo_requerimiento sub ON rq.id_sub_tip_requerimiento = sub.id_sub_tip_requerimiento WHERE DATE_FORMAT(rq.date_requerimiento_apertura, '%Y-%m') IN (".$fec.") AND sub.id_tip_requerimiento = '".$tipo."' ".$where." GROUP BY DATE_FORMAT(rq.date_requerimiento_apertura, '%Y-%m')";
        return 	$this->db->query($sql)->result(); 
    }

    function graf_calificacion_cmi($fec,$tipo,$usuario)
    {
    	$where = ( $usuario == 0 ) ? '' : ' AND rq.id_usuario_asig = '. $usuario;
		$sql = "SELECT COUNT(CASE rq.id_tip_calificacion WHEN '1' THEN 1 END) AS 'MUYMALO',COUNT(CASE rq.id_tip_calificacion WHEN '2' THEN 1 END) AS 'POBRE',COUNT(CASE rq.id_tip_calificacion WHEN '3' THEN 1 END) AS 'NIBIENNIMAL',COUNT(CASE rq.id_tip_calificacion WHEN '4' THEN 1 END) AS 'BUENO',COUNT(CASE rq.id_tip_calificacion WHEN '5' THEN 1 END) AS 'MUYBUENO',DATE_FORMAT(rq.date_requerimiento_apertura, '%Y-%m') AS 'FECHA' FROM detalle_requerimiento rq JOIN sub_tipo_requerimiento sub ON rq.id_sub_tip_requerimiento = sub.id_sub_tip_requerimiento WHERE DATE_FORMAT(rq.date_requerimiento_apertura, '%Y-%m') IN (".$fec.") AND sub.id_tip_requerimiento = '".$tipo."' ".$where." GROUP BY DATE_FORMAT(rq.date_requerimiento_apertura, '%Y-%m')";
        return 	$this->db->query($sql)->result();
    }

    function graf_sla_TA_cmi($fec,$tipo,$usuario)
    {
    	$where = ( $usuario == 0 ) ? '' : ' AND rq.id_usuario_asig = '. $usuario;
        $sql = "SELECT
				COUNT(CASE rq.TA WHEN '0' THEN 1 END) AS 'SICUMPLIO',
				COUNT(CASE rq.TA WHEN 'Incumplimiento de SLA' THEN 1 END) AS 'NOCUMPLIO',
				DATE_FORMAT(rq.date_requerimiento_apertura, '%Y-%m') AS 'FECHA' FROM detalle_requerimiento rq JOIN sub_tipo_requerimiento sub ON rq.id_sub_tip_requerimiento = sub.id_sub_tip_requerimiento WHERE DATE_FORMAT(rq.date_requerimiento_apertura, '%Y-%m') IN (".$fec.") AND sub.id_tip_requerimiento = '".$tipo."' ".$where." GROUP BY DATE_FORMAT(rq.date_requerimiento_apertura, '%Y-%m')";	
        return 	$this->db->query($sql)->result();
    }

    function graf_sla_TS_cmi($fec,$tipo,$usuario)
    {
    	$where = ( $usuario == 0 ) ? '' : ' AND rq.id_usuario_asig = '. $usuario;
        $sql = "SELECT
				COUNT(CASE rq.TS WHEN '0' THEN 1 END) AS 'SICUMPLIO',
				COUNT(CASE rq.TS WHEN 'Incumplimiento de SLA' THEN 1 END) AS 'NOCUMPLIO',
				DATE_FORMAT(rq.date_requerimiento_apertura, '%Y-%m') AS 'FECHA' FROM detalle_requerimiento rq JOIN sub_tipo_requerimiento sub ON rq.id_sub_tip_requerimiento = sub.id_sub_tip_requerimiento WHERE DATE_FORMAT(rq.date_requerimiento_apertura, '%Y-%m') IN (".$fec.") AND sub.id_tip_requerimiento = '".$tipo."' ".$where." GROUP BY DATE_FORMAT(rq.date_requerimiento_apertura, '%Y-%m')";
        return 	$this->db->query($sql)->result();
    }

    function graf_productividad_anio($mes,$anio,$tipo,$usuario,$cond)
    {   
        if ( $cond == 'pendientes' )
        	 $where = ( $usuario == 0 ) ? " AND det.est != '0'" : " AND det.est != '0' AND det.id_usuario_asig = '". $usuario . "'";
        else
             $where = ( $usuario == 0 ) ? " AND det.est = '0'" : " AND det.est = '0' AND det.id_usuario_asig = '". $usuario . "'";
        $sql   = "SELECT COUNT(det.id_detalle_requerimiento) AS total FROM detalle_requerimiento det
				  JOIN sub_tipo_requerimiento sub ON det.id_sub_tip_requerimiento = sub.id_sub_tip_requerimiento
				  WHERE MONTH(det.date_requerimiento_apertura) = '".$mes."' 
				  AND YEAR(det.date_requerimiento_apertura) = '".$anio."' AND sub.id_tip_requerimiento = '".$tipo."'" . $where;  
	    return 	$this->db->query($sql)->row();
    }

    function graf_productividad_jefatura($mes,$anio,$tipo)
    {
    	$sql = "SELECT  
				COUNT(CASE rq.est WHEN '0' THEN 1 END) AS 'TERMINADOS',
				COUNT(CASE rq.est WHEN '1' THEN 1 END) AS 'ENPROCESO',
				COUNT(CASE rq.est WHEN '2' THEN 1 END) AS 'PENDIENTE',
				COUNT(CASE rq.est WHEN '3' THEN 1 END) AS 'ENPAUSA',
				IFNULL(usu.des_usuario,'Vacio') AS 'USUARIO',
				CASE  sub.id_tip_requerimiento WHEN '1' THEN 'Solicitud' WHEN '2' THEN 'Incidencia' END AS tipo_requerimiento,
				rq.id_usuario_asig
				FROM detalle_requerimiento rq
				JOIN sub_tipo_requerimiento sub ON rq.id_sub_tip_requerimiento = sub.id_sub_tip_requerimiento
				LEFT JOIN usuario usu ON rq.id_usuario_asig = usu.id_usuario
				WHERE id_tip_usu IN ('2','3') AND MONTH(rq.date_requerimiento_apertura) = '".$mes."' AND YEAR(rq.date_requerimiento_apertura) = '".$anio."'
				AND sub.id_tip_requerimiento = '".$tipo."'
				GROUP BY USUARIO";
	    return $this->db->query($sql)->result();
    }


    function graf_calificacion_jefatura($mes,$anio,$tipo)
    {
    	$sql = "SELECT  
				COUNT(CASE rq.id_tip_calificacion WHEN '1' THEN 1 END) AS 'MUYMALO',
				COUNT(CASE rq.id_tip_calificacion WHEN '2' THEN 1 END) AS 'POBRE',
				COUNT(CASE rq.id_tip_calificacion WHEN '3' THEN 1 END) AS 'NIBIENNIMAL',
				COUNT(CASE rq.id_tip_calificacion WHEN '4' THEN 1 END) AS 'BUENO',
				COUNT(CASE rq.id_tip_calificacion WHEN '5' THEN 1 END) AS 'MUYBUENO',
				IFNULL(usu.des_usuario,'Vacio') AS 'USUARIO',
				CASE  sub.id_tip_requerimiento WHEN '1' THEN 'Solicitud' WHEN '2' THEN 'Incidencia' END AS tipo_requerimiento,
				rq.id_usuario_asig
				FROM detalle_requerimiento rq
				JOIN sub_tipo_requerimiento sub ON rq.id_sub_tip_requerimiento = sub.id_sub_tip_requerimiento
				LEFT JOIN usuario usu ON rq.id_usuario_asig = usu.id_usuario
				WHERE id_tip_usu IN ('2','3') AND MONTH(rq.date_requerimiento_apertura) = '".$mes."' AND YEAR(rq.date_requerimiento_apertura) = '".$anio."'
				AND sub.id_tip_requerimiento = '".$tipo."' AND rq.est = '0'
				GROUP BY USUARIO";
	    return $this->db->query($sql)->result();
    }

    function graf_TA_jefatura($mes,$anio,$tipo)
    {
    	$sql = "SELECT  
				COUNT(CASE rq.TA WHEN '0' THEN 1 END) AS 'SICUMPLIO',
				COUNT(CASE rq.TA WHEN 'Incumplimiento de SLA' THEN 1 END) AS 'NOCUMPLIO',
				IFNULL(usu.des_usuario,'Vacio') AS 'USUARIO',
				CASE  sub.id_tip_requerimiento WHEN '1' THEN 'Solicitud' WHEN '2' THEN 'Incidencia' END AS tipo_requerimiento,
				rq.id_usuario_asig
				FROM detalle_requerimiento rq
				JOIN sub_tipo_requerimiento sub ON rq.id_sub_tip_requerimiento = sub.id_sub_tip_requerimiento
				LEFT JOIN usuario usu ON rq.id_usuario_asig = usu.id_usuario
				WHERE id_tip_usu IN ('2','3') AND MONTH(rq.date_requerimiento_apertura) = '".$mes."' AND YEAR(rq.date_requerimiento_apertura) = '".$anio."'
				AND sub.id_tip_requerimiento = '".$tipo."'
				GROUP BY USUARIO";
	    return $this->db->query($sql)->result();
	}

    function graf_TS_jefatura($mes,$anio,$tipo)
    {
    	$sql = "SELECT  
				COUNT(CASE rq.TS WHEN '0' THEN 1 END) AS 'SICUMPLIO',
				COUNT(CASE rq.TS WHEN 'Incumplimiento de SLA' THEN 1 END) AS 'NOCUMPLIO',
				IFNULL(usu.des_usuario,'Vacio') AS 'USUARIO',
				CASE  sub.id_tip_requerimiento WHEN '1' THEN 'Solicitud' WHEN '2' THEN 'Incidencia' END AS tipo_requerimiento,
				rq.id_usuario_asig
				FROM detalle_requerimiento rq
				JOIN sub_tipo_requerimiento sub ON rq.id_sub_tip_requerimiento = sub.id_sub_tip_requerimiento
				LEFT JOIN usuario usu ON rq.id_usuario_asig = usu.id_usuario
				WHERE id_tip_usu IN ('2','3') AND MONTH(rq.date_requerimiento_apertura) = '".$mes."' AND YEAR(rq.date_requerimiento_apertura) = '".$anio."'
				AND sub.id_tip_requerimiento = '".$tipo."'
				GROUP BY USUARIO";
	    return $this->db->query($sql)->result();
	}


  


    /**********************************
     **********************************
     **********************************
     **********************************
     **********************************/



    /****AUTOCOMPLETADOR**************/
        

      function autocomplete()
    {
    	 $sql = "SELECT des_usuario,id_usuario FROM usuario WHERE est='1' AND des_usuario LIKE '%".$this->des_usuario."%' LIMIT 10";
    	 return $this->db->query($sql)->result();
    }





}

/* End of file Sis_Requerimiento_M.php */
/* Location: ./application/models/Sis_Requerimiento_M.php */