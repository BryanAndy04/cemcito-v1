function md5(d){return rstr2hex(binl2rstr(binl_md5(rstr2binl(d),8*d.length)))}function rstr2hex(d){for(var _,m="0123456789ABCDEF",f="",r=0;r<d.length;r++)_=d.charCodeAt(r),f+=m.charAt(_>>>4&15)+m.charAt(15&_);return f}function rstr2binl(d){for(var _=Array(d.length>>2),m=0;m<_.length;m++)_[m]=0;for(m=0;m<8*d.length;m+=8)_[m>>5]|=(255&d.charCodeAt(m/8))<<m%32;return _}function binl2rstr(d){for(var _="",m=0;m<32*d.length;m+=8)_+=String.fromCharCode(d[m>>5]>>>m%32&255);return _}function binl_md5(d,_){d[_>>5]|=128<<_%32,d[14+(_+64>>>9<<4)]=_;for(var m=1732584193,f=-271733879,r=-1732584194,i=271733878,n=0;n<d.length;n+=16){var h=m,t=f,g=r,e=i;f=md5_ii(f=md5_ii(f=md5_ii(f=md5_ii(f=md5_hh(f=md5_hh(f=md5_hh(f=md5_hh(f=md5_gg(f=md5_gg(f=md5_gg(f=md5_gg(f=md5_ff(f=md5_ff(f=md5_ff(f=md5_ff(f,r=md5_ff(r,i=md5_ff(i,m=md5_ff(m,f,r,i,d[n+0],7,-680876936),f,r,d[n+1],12,-389564586),m,f,d[n+2],17,606105819),i,m,d[n+3],22,-1044525330),r=md5_ff(r,i=md5_ff(i,m=md5_ff(m,f,r,i,d[n+4],7,-176418897),f,r,d[n+5],12,1200080426),m,f,d[n+6],17,-1473231341),i,m,d[n+7],22,-45705983),r=md5_ff(r,i=md5_ff(i,m=md5_ff(m,f,r,i,d[n+8],7,1770035416),f,r,d[n+9],12,-1958414417),m,f,d[n+10],17,-42063),i,m,d[n+11],22,-1990404162),r=md5_ff(r,i=md5_ff(i,m=md5_ff(m,f,r,i,d[n+12],7,1804603682),f,r,d[n+13],12,-40341101),m,f,d[n+14],17,-1502002290),i,m,d[n+15],22,1236535329),r=md5_gg(r,i=md5_gg(i,m=md5_gg(m,f,r,i,d[n+1],5,-165796510),f,r,d[n+6],9,-1069501632),m,f,d[n+11],14,643717713),i,m,d[n+0],20,-373897302),r=md5_gg(r,i=md5_gg(i,m=md5_gg(m,f,r,i,d[n+5],5,-701558691),f,r,d[n+10],9,38016083),m,f,d[n+15],14,-660478335),i,m,d[n+4],20,-405537848),r=md5_gg(r,i=md5_gg(i,m=md5_gg(m,f,r,i,d[n+9],5,568446438),f,r,d[n+14],9,-1019803690),m,f,d[n+3],14,-187363961),i,m,d[n+8],20,1163531501),r=md5_gg(r,i=md5_gg(i,m=md5_gg(m,f,r,i,d[n+13],5,-1444681467),f,r,d[n+2],9,-51403784),m,f,d[n+7],14,1735328473),i,m,d[n+12],20,-1926607734),r=md5_hh(r,i=md5_hh(i,m=md5_hh(m,f,r,i,d[n+5],4,-378558),f,r,d[n+8],11,-2022574463),m,f,d[n+11],16,1839030562),i,m,d[n+14],23,-35309556),r=md5_hh(r,i=md5_hh(i,m=md5_hh(m,f,r,i,d[n+1],4,-1530992060),f,r,d[n+4],11,1272893353),m,f,d[n+7],16,-155497632),i,m,d[n+10],23,-1094730640),r=md5_hh(r,i=md5_hh(i,m=md5_hh(m,f,r,i,d[n+13],4,681279174),f,r,d[n+0],11,-358537222),m,f,d[n+3],16,-722521979),i,m,d[n+6],23,76029189),r=md5_hh(r,i=md5_hh(i,m=md5_hh(m,f,r,i,d[n+9],4,-640364487),f,r,d[n+12],11,-421815835),m,f,d[n+15],16,530742520),i,m,d[n+2],23,-995338651),r=md5_ii(r,i=md5_ii(i,m=md5_ii(m,f,r,i,d[n+0],6,-198630844),f,r,d[n+7],10,1126891415),m,f,d[n+14],15,-1416354905),i,m,d[n+5],21,-57434055),r=md5_ii(r,i=md5_ii(i,m=md5_ii(m,f,r,i,d[n+12],6,1700485571),f,r,d[n+3],10,-1894986606),m,f,d[n+10],15,-1051523),i,m,d[n+1],21,-2054922799),r=md5_ii(r,i=md5_ii(i,m=md5_ii(m,f,r,i,d[n+8],6,1873313359),f,r,d[n+15],10,-30611744),m,f,d[n+6],15,-1560198380),i,m,d[n+13],21,1309151649),r=md5_ii(r,i=md5_ii(i,m=md5_ii(m,f,r,i,d[n+4],6,-145523070),f,r,d[n+11],10,-1120210379),m,f,d[n+2],15,718787259),i,m,d[n+9],21,-343485551),m=safe_add(m,h),f=safe_add(f,t),r=safe_add(r,g),i=safe_add(i,e)}return Array(m,f,r,i)}function md5_cmn(d,_,m,f,r,i){return safe_add(bit_rol(safe_add(safe_add(_,d),safe_add(f,i)),r),m)}function md5_ff(d,_,m,f,r,i,n){return md5_cmn(_&m|~_&f,d,_,r,i,n)}function md5_gg(d,_,m,f,r,i,n){return md5_cmn(_&f|m&~f,d,_,r,i,n)}function md5_hh(d,_,m,f,r,i,n){return md5_cmn(_^m^f,d,_,r,i,n)}function md5_ii(d,_,m,f,r,i,n){return md5_cmn(m^(_|~f),d,_,r,i,n)}function safe_add(d,_){var m=(65535&d)+(65535&_);return(d>>16)+(_>>16)+(m>>16)<<16|65535&m}function bit_rol(d,_){return d<<_|d>>>32-_}

function auch_log(id){
    var user = $("#txt_usuario"),msg = ''
    var password = $("#txt_contrasena")
    var valida = new Array();
    if ( user.val().length ==  0 )
         valida.push("Ingresar Usuario")
    if ( password.val().length ==  0 ) 
         valida.push("Ingresar Contraseña") 
    if(  valida.length == 0 ) {
         $.ajax({
         	url: url  + 'login_json',
         	type: 'POST',
         	data: {  
         	       	   usuario : user.val(),
         	       	   password : password.val()
         	      },
         	success :function(json){
                    if ( json  == 1 ) 
                        window.location = url + 'panel';
                    else if  ( json == 0 ) 
                        alert('Ingresar Registros Correctos')
                    else if ( json == 2 ) 
                        window.location = url + 'restablecer-contrasena';            
         	}
         })
    } else {
    	msg = '' 
        for (var i = 1; i <= valida.length; i++)
            msg+= "<span style='color:red;font-size:14px'>"+i+"- "+valida[i-1]+"</span></br>"
       
        kendo.alert(msg);
        $('.k-dialog-titlebar').remove();
    }
}


function restablecer_contrasena(){
    var valida = new Array(),msg =''
    
    if ( $('#nueva_contrasena').val().length == 0 )
         valida.push("Ingresar nueva contraseña.")
    
    if(  valida.length == 0 ) {
        $.post( url + 'restablecer-contrasena-update', { nueva_contrasena : $('#nueva_contrasena').val() }, function(data, textStatus, xhr) {
            window.location = url + 'panel';
        });
    } else {
        msg = '' 
        for (var i = 1; i <= valida.length; i++)
            msg+= "<span style='color:red;font-size:14px'>"+i+"- "+valida[i-1]+"</span></br>"
       
        kendo.alert(msg);
        $('.k-dialog-titlebar').remove();
    }    

}

function cerrar_session(){
	$.ajax({
         	url: url  + 'cerrar-session',
         	type: 'GET',
         	success :function(){
         		window.location = url;
         	}
    })
}

function carga_tipos_requerimiento(id,route,param) {
     $.ajax({
     	url: url + route,
     	type: 'POST',
     	data: param,
        dataType : 'json',
     	success : function(resp){
     		var html = "<option value=''>Seleccione opción</option>"
            $.each(resp, function(index, val) {
                 html+="<option value='"+val.id+"'>"+val.des+"</option>"
            });
            id.html(html)   	
     	}
     })
}



function view_detalle_requerimiento(id_detalle_requerimiento){
	  $.ajax({
	   	 url: url + 'datalle-requerimiento',
	   	 type: 'POST',
	   	 dataType: 'json',
	   	 data: { id_detalle_requerimiento: id_detalle_requerimiento },
	   	 success : function(resp) {
              load_historial(id_detalle_requerimiento)
	   	 	  $('#mdl_detalle_requerimiento_tittle').html( resp.tipo_requerimiento + ' - ' + resp.titulo_requerimiento )
	   	 	  $('#descripcion_req').html(resp.desc_requerimiento)
	   	 	  $('#codigo_req').html(resp.id_detalle_requerimiento)
	   	 	  $('#input_id_detalle_requerimiento').val(id_detalle_requerimiento)
	   	 	  $('#capertura_req').html(resp.fec_apertura)
	   	 	  $('#estado_req').html(resp.est_des)
	   	 	  $('#prioridad_req').html(resp.prioridad)
              $('#__prioridad_req').val(resp.req_prioridad)
              $('#__estado_req').val(resp.est_des)
	   	 	  $('.btn_tip_requerimiento').html(resp.tipo_requerimiento)
              $('#descargar_evi__').html(resp.path_adjunto)
              $('#descargar_evi__').attr('arch',resp.path_adjunto)
              if ( resp.est == 'Abierto' ) {
                  $('#btn_abrir_requerimiento').css('display','none')
                  $('#com_solicitud').css('display','block');
                  $('#btn_asignar_rq').css('display','block')
              } else if (resp.est == 'Cerrado' ) {
                  $('#btn_asignar_rq').css('display','none')
              	  $('#btn_abrir_requerimiento').css('display','block')
              	  $('#com_solicitud').css('display','none');
              }

	   	      $('#mdl_detalle_requerimiento').modal('show')
	   	 }
	   })
}

function load_historial(id_detalle_requerimiento) {
	  
	  var html = "";

	  $.ajax({
	  	url: url + 'load-historial-requerimiento',
	  	type: 'POST',
	  	dataType: 'json',
	  	data: { id_detalle_requerimiento: id_detalle_requerimiento },
        success : function(resp){
        	$.each(resp, function(index, val) {

                html+= '<div class="timeline-task">'
                html+= '<div class="icon bg3">'
                html+= '<i class="fa fa-comment"></i>'
                html+= '</div>'
                html+= '<div class="tm-title">'
                html+= '<h4>'+val.des_usuario+'</h4>'
                html+= '<span class="time"><i class="ti-time"></i>'+ val.fec_reg +'</span>'
                html+= '</div>'
                html+= '<p>'+val.comet_hist_req+'</p>'
                html+= '</div>'
        	});
        	$('#cest_historial').html(html);
        }
	  }) 
}


function cerrar_session_pausar() {
    $.ajax({
        url: url  + 'cerrar-session-pausar',
        type: 'GET',
        success :function(){
            window.location = url;
        }
    })
}

function options(id) {
	$('li[op="click"]').removeClass('active')
	$('#tittle').text($(id).attr('title'))
	$(id).addClass('active')
	$.get( url + 'modules', { op : $(id).attr('mdl') } ,function(resp) {
        	$('#__content_panel').html(resp)
    })
}